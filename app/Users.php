<?php

namespace App;

use App\City;
use App\Category;
use App\Models\Role;
use App\Notification;
use Spatie\Sluggable\HasSlug;
use App\Models\ApprovalThreads;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class Users extends Model
{
    use SoftDeletes,HasSlug,LogsActivity;
    protected static $logFillable = true;
    protected $table = 'users';
    protected $fillable = ['id','user_type','user_slug','user_code', 'first_name','last_name','email','password','remember_token','mobile','mentorship_status','role_id','created_at','updated_at','deleted_at','display_name','activation_date','deactivation_date','approval_status'];


    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom(['first_name','last_name'])
            ->saveSlugsTo('user_slug');
    }

    public function userProfile(){
        return $this->hasOne('App\UsersProfile','user_id','id');
         // return $this->hasOne('App\UsersProfile');
    }
    public function companyTeamMember(){
        return $this->hasMany('App\CompanyDirectors','user_id','id');
    }
    public function companyFiles(){
        return $this->hasMany('App\CompanyDocuments','user_id','id');
    }
    public function entrepreneur(){
        return $this->hasOne('App\Enterpreneur','user_id','id');
    }
    public function qualification(){
        return $this->hasMany('App\UserQualification','user_id','id')->select('id', 'qualification_name');;
    }
    public function companyAwards(){
        return $this->hasMany('App\CompanyAwards','user_id','id');
    }
    public function skills(){
        return $this->hasMany('App\MentorSkill','user_id','id');
    }
    public function services(){
        return $this->hasMany('App\PartnerServices','user_id','id');
    }

    public function categories(){
        return $this->belongsToMany(Category::class,'entrepreneur_interest_area','user_id','interest_area_id');
    }

     //Permission.php
     public function roles() {
        return $this->belongsToMany(Role::class,'users_roles','user_id','role_id');
    }

    public function notifications() {
        return $this->belongsToMany(Notification::class,'user_notification_mapper','user_id','notification_id');
    }

    public function giveRolesTo(... $roles) {
      
        $roles = $this->getAllRoles($roles);
        
		if($roles === null) {
			return $this;
		}
		$this->roles()->sync($roles);
		return $this;
    }
    
    protected function getAllRoles(array $roles) {
		return Role::whereIn('id',$roles[0])->get();
    }
    
	public function withdrawlRolesTo( ... $roles) {
      
        $roles = $this->getAllRoles($roles);
        
		$this->roles()->detach($roles);
		return $this;
    }
    
    public function hasAccess(array $permissions) {
        foreach($this->roles as $role) {
            if($role->hasAccess($permissions)) {
                return true;
            } 
        }
        return false;
    }

    public function threads() {
        return $this->hasOne(ApprovalThreads::class,'user_id','id');
    }

    public function city() {
        return $this->belongsTo(City::class);
    }
 
    protected $hidden = ['id'];
}
