<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class SaveFormData extends Model
{
   use SoftDeletes,LogsActivity;
   protected static $logFillable = true;
    protected $table = 'dynamic_form_data';
    protected $fillable = ['form_id','form_details','created_at','updated_at','deleted_at'];
}
