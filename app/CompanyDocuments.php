<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyDocuments extends Model
{
   use SoftDeletes;
    protected $table = 'company_documents';
    protected $fillable = ['user_id','documents_type_id','document_file','created_at','updated_at','deleted_at'];

}
