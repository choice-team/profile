<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class PartnerProfile extends Authenticatable
{
    use Notifiable,LogsActivity,SoftDeletes;
    protected static $logFillable = true;
    protected $table = 'partner_profile';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'slug',
        'office_address_1',
        'office_address_2',
        'city',
        'state',
        'country',
        'postal_code',
        'logo',
        'website_url',
        'linkedin_url',
        'facebook_url',
        'area_of_business',
        'type_of_establishment',
        'company_description',
        'cxo_name',
        'cxo_qualification',
        'cxo_designation',
        'cxo_contact',
        'cxo_linkedin_url',
        'contact_person_name',
        'collaboration_model_description',
        'proposed_deliverables_description',
        'proposed_deliverables_benefit',
        'expectations',
        'about_wep_description',
        'created_by',
        'updated_by',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'deleted_at',
    ];
}
