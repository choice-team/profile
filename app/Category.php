<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes,LogsActivity;
    protected static $logFillable = true;
    protected $table = 'categories';
    protected $fillable = ['category_name','category_type','updated_by','created_at','updated_at','deleted_at'];


    public function services() {
        return $this->belongsToMany(PartnerServices::class,'service_category_mapper','service_id','category_id');
    }

    public function users(){
        return $this->belongsToMany(Users::class,'entrepreneur_interest_area','user_id','interest_area_id');
    }
    
    public function notifications()
    {
        return $this->morphMany('App\Notification', 'notification');
    }
}
