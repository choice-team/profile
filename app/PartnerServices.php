<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class PartnerServices extends Model
{
    use SoftDeletes,LogsActivity;
    protected static $logFillable = true;
    protected $table = 'partner_services';
    protected $fillable = ['user_id','service_title','service_desc','created_at','updated_at','deleted_at'];

    public function user() {
        $this->belongsTo('App\Users');
    }

    public function category() {
        return $this->belongsToMany(Category::class,'service_category_mapper','service_id','category_id');
    }

    protected $hidden = ['user_id'];
}
