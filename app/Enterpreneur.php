<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Enterpreneur extends Model
{
    use SoftDeletes;
    protected $table = 'entrepreneur';
    protected $fillable = ['user_id','entrepreneur_name','url','sector','incorporation_structure_id','created_at','updated_at','deleted_at'];

    // public function city(){
    //     return $this->hasMany('App\City', 'state_id','id');
    // }
}
