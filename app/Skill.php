<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Skill extends Model
{
   use SoftDeletes;
    protected $table = 'skills';
    protected $fillable = ['skill_name','created_at','updated_at','deleted_at'];

    //  public function expertise(){
    //     // echo "DFdfd"; exit;
    //     return $this->belongsToMany('App\MentorSkill','skill_id','id');
    // }
}
