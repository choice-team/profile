<?php

namespace App\Services;

use App\Models\Module;
use Illuminate\Support\Facades\DB;

class PermissionMapper
{
    /**
     * @param $title
     * @param int $id
     * @return string
     * @throws \Exception
     */
    public static function createPermission($name, $slug)
    {
        // Normalize the title
        $shortform = (new self)->generateShortform($name);
        if(Module::where('name',$name)->count() == 0) {
            $module = Module::create(['name'=>$name,'slug'=>$slug,'shortform'=>$shortform,'parent_id'=>12]);
            $operations = DB::table('operations')->get()->pluck('name','id');
            foreach($operations as $k=>$v) {
                $a = strtolower($module->name.' '.$v);
                $a = str_replace(' ', '-', $a);
                DB::table('permissions')->insert(['slug'=>($module->name.'-'.$v),'module_id'=>$module->id,'operation_id'=>$k]);
            }
        }
        return true;
    }

    protected function generateShortform($name) {
        if(preg_match_all('/\b(\w)/',strtolower($name),$m)) {
            $v = implode('',$m[1]); // $v is now SOQTU
        }
        return $v;
    }

    public static function deletePermission($slug) {
        $moduleQuery = DB::table('modules')->where('slug',$slug);
        $module = $moduleQuery->first();
        if($module) {
            $permissionQuery = DB::table('permissions')->where('module_id',$module->id);
            $permission = $permissionQuery->pluck('id')->toArray();
            DB::table('roles_permissions')->whereIn('permission_id',$permission)->delete();
            $permissionQuery->delete();
            $moduleQuery->delete();
        }
        return true;
    }

    public static function updatePermission($oldName, $newName,$slug) {
        $moduleQuery = DB::table('modules')->where('name',$oldName)->first();

        if(empty($moduleQuery))
            return false;
        DB::table('modules')->where('name',$oldName)->update(['name'=>$newName,'slug'=>$slug]);
        $operations = DB::table('operations')->get()->pluck('name','id');
        $moduleData = DB::table('modules')->where('id',$moduleQuery->id)->first();
        foreach($operations as $k=>$v) {
            $a = strtolower($moduleData->name.' '.$v);
            $a = str_replace(' ', '-', $a);
            DB::table('permissions')->where(['module_id'=>$moduleData->id,'operation_id'=>$k])
            ->update(['slug'=>($slug.'-'.$v)]);
        }
        return true;
    }

}
