<?php

namespace App\Services;

use App\Models\Module;
use Illuminate\Support\Facades\DB;

class LanguageService
{
    /**
     * @param $title
     * @param int $id
     * @return string
     * @throws \Exception
     */
    public static function translateLanguage($string, $language)
    {
        $apiKey = env('GOOGLE_API_KEY');
        $client = new \GuzzleHttp\Client();
        try {
            $url = 'https://www.googleapis.com/language/translate/v2?key=' . $apiKey . '&q=' . rawurlencode($string) . '&source=en&target='.$language;
            $request = $client->get($url);
        }
        catch (\Exception $e) {
           return $e->getCode();
        }
        $response = $request->getBody()->getContents();
           
        return $response;

    }


}
