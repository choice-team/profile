<?php

namespace App\Services;

use DB;

class Slug
{
    /**
     * @param $title
     * @param int $id
     * @return string
     * @throws \Exception
     */
    public static function createSlug($title, $table_name, $column_name, $id = 0)
    {
        // Normalize the title
        $title = substr($title,0,50);
        $slug = str_slug($title);

        // Get any that could possibly be related.
        $allSlugs = (new self)->getRelatedSlugs($slug, $table_name, trim($column_name), $id);

        // If we haven't used it before then we are all good.
        if (! $allSlugs->contains($column_name, $slug)){
            return $slug;
        }

        for ($i = 1; $i <= 100; $i++) {
            $newSlug = $slug.'-'.$i;
            if (! $allSlugs->contains($column_name, $newSlug)) {
                return $newSlug;
            }
        }

        throw new \Exception('Can not create a unique slug');
    }

    protected function getRelatedSlugs($slug, $table_name, $column_name, $id = 0)
    {
        return DB::table($table_name)->select($column_name)->where($column_name, 'like', $slug.'%')
            ->where('id', '<>', $id)
            ->get();
    }
}
