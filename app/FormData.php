<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class FormData extends Model
{
   use SoftDeletes,LogsActivity;
   protected static $logFillable = true;
    protected $table = 'form_fields';
    protected $fillable = ['form_id','form_data','created_at','updated_at','deleted_at'];
}
