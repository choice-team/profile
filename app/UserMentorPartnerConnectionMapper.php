<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserMentorPartnerConnectionMapper extends Model
{
   use SoftDeletes,LogsActivity;
   protected static $logFillable = true;
    protected $table = 'user_mentor_partner_connection_mapper';
    protected $fillable = ['user_id', 'connected_to', 'connection_status', 'created_at', 'updated_at', 'deleted_at'];

    //  public function expertise(){
    //     // echo "DFdfd"; exit;
    //     return $this->belongsToMany('App\MentorSkill','skill_id','id');
    // }
}
