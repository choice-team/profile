<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class UsersProfile extends Model
{

    use SoftDeletes,LogsActivity;
    protected static $logFillable = true;
    protected $table = 'user_profile';
    protected $fillable = ['user_id','gender','birth_year','country','state','city','other_city','about','profile_pic','created_at','updated_at','deleted_at'];
    public function userProfile(){
        // echo "dfdfd"; exit;
        return $this->belongsTo('App\Users','user_id','id');

    }

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['created_at','updated_at','deleted_at'];


}
