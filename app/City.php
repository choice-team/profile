<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class City extends Model
{
    //
    use SoftDeletes,LogsActivity;
    protected static $logFillable = true;
    protected $table = 'city';
    protected $fillable = ['id','city_name','state_id','created_at','updated_at','updated_by','deleted_at'];

    
    protected $hidden= [];

    public function users() {
        return $this->hasMany(Users::class);
    }
}
