<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyAwards extends Model
{
    use SoftDeletes;
    protected $table = 'company_awards';
    protected $fillable = ['user_id','award_name','award_certificate','award_desc','created_at','updated_at','deleted_at'];

}
