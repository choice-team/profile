<?php
namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class DeactivateDate implements Rule
{
    protected  $min_rent;

    /**
     * Create a new rule instance.
     *
     * @param $activation_date
     */
    public function __construct($activation_date)
    {
        // Here we are passing the activation_date value to use it in the validation.
        $this->activation_date = $activation_date;         
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // This is where you define the condition to be checked.
        return $value >= $this->activation_date;         
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        // Customize the error message
        return 'The Deactivation date should always be greater then activation date.'; 
    }
}

?>