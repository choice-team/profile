<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Captchas extends Model {

    public $timestamps = false;
    protected $table = 'captcha';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'ans'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

}
