<?php

namespace App\Models;

use App\Models\City;
use App\Models\Partner;
use Spatie\Sluggable\HasSlug;
use App\Models\AssociateSpeaker;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;


class Events extends Model implements AuthenticatableContract, AuthorizableContract {

    use Authenticatable,
        Authorizable, SoftDeletes, HasSlug,LogsActivity;
    
    protected static $logFillable = true;

    public $timestamps = true;
    protected $table = 'events';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'event_shares',
        'event_name',
        'event_slug',
        'event_type',
        'start_date',
        'start_time',
        'end_date',
        'status',
        'end_time',
        'address',
        'landmark',
        'city_id',
        'other_city',        
        'state_id',
        'event_desc',
        'event_agenda',
        'feature_image',
        'partner_id',        
        'event_url',
        'created_by',
        'updated_by',
        'created_at', 
        'updated_at',
        'organizer_type',
        'associate_partner',
        'hosted_by'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [        
        'deleted_at','created_by'
    ];
    public function getSlugOptions() : SlugOptions
    {
       
        return SlugOptions::create()
            ->generateSlugsFrom('event_name')
            ->saveSlugsTo('event_slug');
        
    }
    public function categories()
    {
        return $this->belongsToMany(Category::class,'event_category_mapper','event_id','category_id');
    }
    public function state()
    {
        return $this->hasOne(State::class,'id','state_id');
    }

    public function city()
    {
        return $this->hasOne(City::class,'id','city_id');
    }

    public function agendas() {
        return $this->hasMany(EventAgenda::class,'event_id');
    }

    public function speakers() {
        return $this->hasMany(EventSpeakerMapper::class,'event_id');
    }

    public function associatePartners() {
        return $this->hasMany(AssociatePartner::class,'event_id');
    }

    public function Partner() {
        return $this->belongsTo(Partner::class);
    }

    public function users()
    {
        return $this->belongsToMany(Users::class,'event_attendees','event_id','user_id');
    }

    public function galleries() {
        return $this->hasMany(EventGallery::class,'event_id');
    }

    public function feedbacks() {
        return $this->hasMany(EventFeedback::class,'event_id');
    }
 }
