<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class Blog extends Model
{
    use SoftDeletes;
    protected static $logFillable = true;
    protected $table = 'blog_posts';

    public $fillable = ['blog_title','blog_slug','blog_meta_title','blog_meta_description','blog_excerpt','blog_content','blog_status','blog_user_id', 'blog_likes', 'blog_shares','blog_dislikes','blog_published_date','created_at','updated_at', 'deleted_at','blog_iframe_link','language_id'];

   
}
