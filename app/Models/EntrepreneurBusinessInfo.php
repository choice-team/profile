<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EntrepreneurBusinessInfo extends Model {

    use SoftDeletes;

    protected $table = 'entrepreneur_business_info';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'existing_business_status',
        'registered_entity_status',
        'name_of_enterprise',
        'country',
        'state',
        'city',
        'other_city',
        'address_line_1',
        'address_line_2',
        'sector_of_entreprise',
        'type_of_enterprise',
        'registration_year',
        'stage_of_business',
        'number_of_employee',
        'financial_turn_over',
        'website_link',
        'enterprise_fb_link',
        'enterprise_twitter_link',
        'registered_with_pdp_status',
        'supported_by_gov_status',
        'future_plan_for_business_status',
        'student_type_status',
        'business_plan_ready_status',
        'setting_up',
        'created_at', 
        'updated_at',
        'deleted_at',
        'business_start',
        'cofounder',
        'other_sector_of_entreprise',
        'financing',
        'stage_of_funding',
        'investments',
        'enterprise_linkedin_link'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['created_at','updated_at'];
}
