<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class AreaOfSpecialization extends Model
{   
    use SoftDeletes,LogsActivity;
    protected static $logFillable = true;
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   protected $fillable = [
       'area_of_specialization',
       'updated_by',
       'created_at', 
       'updated_at',
       'deleted_at'
   ];

   /**
    * The attributes excluded from the model's JSON form.
    *
    * @var array
    */
   protected $hidden = ['created_at'];
}