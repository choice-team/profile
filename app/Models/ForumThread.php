<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ForumThread extends Model
{
    protected static $logFillable = true;
    protected $table = 'forum_thread';

    public $fillable = ['question_id','thread_answer','parent_id','user_id','likes','created_at','updated_at', 'deleted_at'];

   
}
