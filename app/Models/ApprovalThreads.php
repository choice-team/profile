<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class ApprovalThreads extends Model
{
    use SoftDeletes,LogsActivity;
    protected static $logFillable = true;
    protected $table = 'approval_thread';


    public $fillable = ['type','blog_id','description','user_id','parent_id','created_at','updated_at', 'deleted_at'];

    public function users() {
        return $this->belongsTo('App\Users');
    }
    protected $hidden = ['user_id'];
}
