<?php

namespace App\Models;

use App\Models\Role;
use App\Models\Permissions;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class Module extends Model  {

  use SoftDeletes,LogsActivity;
  protected static $logFillable = true;
  /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'slug','shortform','parent_id','created_at','updated_at','deleted_at'];

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function children() {
      return $this->hasMany('App\Models\Module', 'parent_id', 'id' )->with('children');
    }
      
    public function parent() {
      return $this->hasOne('App\Models\Module', 'id', 'parent_id' )->where('parent_id',0)->with('parent');;
    }
      //Permission.php
    public function permissions() {
        return $this->hasMany(Permission::class,'module_id','role_id');
    }
}
?>
