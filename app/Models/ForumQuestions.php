<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class ForumQuestions extends Model
{
    use SoftDeletes;
    protected static $logFillable = true;
    protected $table = 'forum_questions';

    public $fillable = ['forum_id','topic_id','user_id','forum_question','description','forum_question_slug','likes','dislikes','posted_date','publish_date','status'];

}
