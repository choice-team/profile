<?php

namespace App\Models;

use App\Models\Roles;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RoleModule extends Model  {

    public $timestamp = false;

    protected $table = 'role_module';

}
