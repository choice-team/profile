<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Forms extends Model
{
   use SoftDeletes, HasSlug, LogsActivity;
   protected static $logFillable = true;
    protected $table = 'dynamic_forms';
    protected $fillable = ['form_name','form_slug','created_at','updated_at','deleted_at'];

     public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('form_name')
            ->saveSlugsTo('form_slug');
    }
}
