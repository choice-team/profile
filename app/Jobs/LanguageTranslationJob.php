<?php

namespace App\Jobs;

use App\Notification;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Services\LanguageService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\BlogPostTranslate;
use App\Language;
use Response;


class LanguageTranslationJob extends Job
{
  use  InteractsWithQueue, Queueable, SerializesModels;
  protected $string;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($string,$translate_lang)
    {
      $this->string = $string;
      $this->translate_lang = $translate_lang;
      
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        DB::table('translated_data')->truncate();
       if(isset($this->translate_lang)&& $this->translate_lang) {
        $string = LanguageService::translateLanguage($this->string,$this->translate_lang);

        $string = json_decode($string,true);
        $char = $string['data']['translations'][0]['translatedText'];
         $data[$this->translate_lang] = $char;
         return $data;
      }
      $language =  Language::pluck('short_name');
      foreach($language as $value) {
        if($value=='en') {
          $char = $this->string;
        } else {
          $translated_string = LanguageService::translateLanguage($this->string,$value);
          // dd($translated_string['string']);
           $translated_string = json_decode($translated_string,true);
           $char = $translated_string['data']['translations'][0]['translatedText'];
        }
        $data[$value] = $char;
           DB::table('translated_data')->insert([
            'short_name' => $value,
            'data' => $char
        ]);
      }

    }

  }
