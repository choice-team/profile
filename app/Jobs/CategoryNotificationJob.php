<?php

namespace App\Jobs;

use App\Users;
use App\Notification;
use Illuminate\Bus\Queueable;
use App\Events\NotificationEvent;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;


class CategoryNotificationJob extends Job
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $notification;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Notification $notification)
    {
        $this->notification = $notification;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = Users::all();
        $this->notification->users()->saveMany($user);
        event(new NotificationEvent($this->notification));
    }
}
