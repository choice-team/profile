<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserQualification extends Model
{
    use SoftDeletes;
    protected $table = 'users_qualification';
    protected $fillable = ['user_id','qualification_id','area_of_specialisation','linked_in_link','created_at','updated_at','deleted_at'];
    public function qualification(){
        return $this->belongsToMany('App\UserQualification','user_id','id');
    }

}
