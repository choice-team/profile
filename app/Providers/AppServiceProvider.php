<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;
use App\Users;

class AppServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Validator::extend('unique_mobile', function($attribute, $value, $parameters, $validator) {

            $mobile = array_get($validator->getData(), $parameters[0], null);

            if($mobile != null){

                $count = Users::where([
                    'mobile' => $mobile,
                    'approval_status' => 'approved',
                ])->count();

                if($count > 0){

                    return false;
                }    
            }

            return true;

        });

        Validator::extend('unique_email', function($attribute, $value, $parameters, $validator) {

            $email = array_get($validator->getData(), $parameters[0], null);

            if($email != null){

                $count = Users::where([
                    'email' => $email,
                    'approval_status' => 'approved',
                ])->count();

                if($count > 0){

                    return false;
                }    
            }

            return true;
        });
    }
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        require_once __DIR__ . '/../Helpers/helpers.php';

         $this->app->singleton('Illuminate\Contracts\Routing\ResponseFactory', function ($app) {
        return new \Illuminate\Routing\ResponseFactory(
            $app['Illuminate\Contracts\View\Factory'],
            $app['Illuminate\Routing\Redirector']
        );
    });

        require_once __DIR__ . '/../helpers.php';
    }
}
