<?php

namespace App\Providers;

use App\Users;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use App\Models\Permission;
use function GuzzleHttp\json_encode;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.

        $this->app['auth']->viaRequest('api', function ($request) {
          if ($request->header('Authorization')) {
                $authorization = explode(' ',$request->header('Authorization'));
		if(isset($authorization[1])){
                $token =explode('~~', base64_decode($authorization[1]));
                if(count($token)) {
                    $user = Users::where('api_token', $token[0])->first();
                    if(!empty($user)){
                        $request->request->add(['user_id' => $user->id]);
                    }
                    return $user;
                }
               }
            }
        });

        $this->registerPermissionPolicies();
    }

    public function registerPermissionPolicies() {
        // Gate::define('area-of-interest-single',function($user) {
        //     return $user->hasAccess(['area-of-interest-single']);
        //  });
        Permission::get()->map(function($permission){
            Gate::define($permission->slug, function($user) use ($permission){  
                if($user->roles->count()){
                    if ($user->roles[0]->slug == 'super-admin') {
                        return true;
                    }
                    return $user->hasAccess([$permission->slug]);
                }
            });
        });
        
    }
}
