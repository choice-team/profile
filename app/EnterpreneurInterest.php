<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EnterpreneurInterest extends Model
{
    use SoftDeletes;
    protected $table = 'entrepreneur_interest_area';
    protected $fillable = ['user_id','interest_area_id','created_at','updated_at','deleted_at'];

    // public function city(){
    //     return $this->hasMany('App\City', 'state_id','id');
    // }
}
