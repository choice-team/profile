<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notification extends Model
{
    use SoftDeletes,LogsActivity;
    protected static $logFillable = true;
    protected $table = 'notifications';
    protected $fillable = ['id','notification_type','notification_id','title','url','created_by','created_at','updated_at','deleted_at','status'];

     /**
     * Get all of the owning notification models.
     */
    public function notification()
    {
        return $this->morphTo();
    }

    public function users() {
        return $this->belongsToMany(Users::class,'user_notification_mapper','notification_id','user_id');
    }

}
