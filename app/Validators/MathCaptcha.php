<?php 
namespace App\Validators;

use App\Models\Captchas;
/**
    * Name          : Math Captcha
    * Description   : Mc is very simple and lightweight captcha function. You can easily configure it in your php project
    * Make date     : 12/03/2019
    * Author        : Choicetechlab
*/
trait MathCaptcha
{
    private $num1;
    private $num2;
    private $actionNumber;
    private $arr;

    private function generate()
    {
        $this->num1 = random_int(2, 5);
        $this->num2 = random_int(6, 9);
        $this->actionNumber = random_int(1, 3);
        $this->ar = [];
      
        switch ($this->actionNumber)
        {
            case 1:
                $this->arr['question'] = $this->num1 . " + " . $this->num2 . " = ? ";
                $ans = $this->addition($this->num1, $this->num2);
                $this->arr['id'] = Captchas::create(['ans'=>$ans])->id;
                return $this->arr;
                break;

            case 2:
                $this->arr['question'] = $this->num2 . " - " . $this->num1 . " = ? ";
                $ans = $this->substruction($this->num1, $this->num2);
                $this->arr['id'] = Captchas::create(['ans'=>$ans])->id;
                return $this->arr;
                break;

            case 3:
                $this->arr['question'] = $this->num1 . " * " . $this->num2 . " = ? ";
                $ans = $this->multiplication($this->num1, $this->num2);
                $this->arr['id'] = Captchas::create(['ans'=>$ans])->id;
                return $this->arr;
                break;
        }
    }

    private function addition($num1, $num2)
    {
        return $num1 + $num2;
    }

    private function substruction($num1, $num2)
    {
        return $num2 - $num1;
    }

    private function multiplication($num1, $num2)
    {
        return $num1 * $num2;
    }

    public function postMc($response)
   {
        $ans = Captchas::find($response['id']);
        if(empty($ans)) {
            return false;
        } else {
            $answer = $ans->ans;
            $ans->delete();
            return ($response['ans']==$answer)?true:false;
        }
   }
}
