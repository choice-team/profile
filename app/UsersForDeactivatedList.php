<?php

namespace App;

use App\Blog;
use App\Models\Role;
use App\Notification;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class UsersForDeactivatedList extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, LogsActivity;
    protected static $logFillable = true;
    protected $table = 'users';
    protected $fillable = ['user_type','user_code', 'first_name','last_name','email','password','remember_token','mobile','mentorship_status','role_id','created_at','updated_at','deleted_at','activation_date','deactivation_date','display_name'];

    public $timestamps = false;
    public function userProfile(){
        return $this->hasOne('App\UsersProfile','user_id','id');
         // return $this->hasOne('App\UsersProfile');
    }
    public function companyTeamMember(){
        return $this->hasMany('App\CompanyDirectors','user_id','id');
    }
    public function companyFiles(){
        return $this->hasMany('App\CompanyDocuments','user_id','id');
    }
    public function entrepreneur(){
        return $this->hasOne('App\Enterpreneur','user_id','id');
    }
    public function qualification(){
        return $this->hasMany('App\UserQualification','user_id','id')->select('id', 'qualification_name');;
    }
    public function companyAwards(){
        return $this->hasMany('App\CompanyAwards','user_id','id');
    }
    public function skills(){
        return $this->hasMany('App\MentorSkill','user_id','id');
    }

     //Permission.php
     public function roles() {
        return $this->belongsToMany(Role::class,'users_roles','user_id','role_id');
    }

    public function blogs() {
        return $this->hasMany(Blog::class);
    }

    public function notifications() {
        return $this->belongsToMany(Notification::class,'user_notification_mapper','user_id','notification_id');
    }
    
    public function giveRolesTo(... $roles) {
      
        $roles = $this->getAllRoles($roles);
        
		if($roles === null) {
			return $this;
		}
		$this->roles()->sync($roles);
		return $this;
    }
    
    public function blogPosts() {
        return $this->belongsToMany(Blog::class,'user_blog_like_mapper','user_id','blog_id');  
    }
    
    protected function getAllRoles(array $roles) {
		return Role::whereIn('id',$roles[0])->get();
    }
    
	public function withdrawlRolesTo( ... $roles) {
      
        $roles = $this->getAllRoles($roles);
        
		$this->roles()->detach($roles);
		return $this;
    }
    
    public function hasAccess(array $permissions) {
        foreach($this->roles as $role) {
            if($role->hasAccess($permissions)) {
                return true;
            } 
        }
        return false;
    }

    protected $hidden= ['id','deleted_at'];
}
