<?php

namespace App\Events;

use Illuminate\Support\Facades\Log;

class NotificationEvent extends Event
{

    public $notification;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($notification)
    {
        $this->notification = $notification;
    }

}
