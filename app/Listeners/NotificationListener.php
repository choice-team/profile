<?php

namespace App\Listeners;

use App\Users;
use App\Events\NotificationEvent;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use function GuzzleHttp\json_encode;

class NotificationListener implements ShouldQueue 
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NotificationEvent  $event
     * @return void
     */
    public function handle(NotificationEvent $event)
    {
 
        $user  = Users::select('fcm_token.token')->join('fcm_token','users.id','=','fcm_token.user_id')->where(['users.approval_status'=>'approved'])->get()->pluck('token')->toArray();
        Log::info(print_r($user));
        $data = [];

      
        $data['data']['notification']['title'] = "New Category ".$event->notification->title." Added";
        $data['data']['notification']['body'] = $event->notification->title.' added';
        $data['data']['notification']['icon'] = 'https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwjsgtW4_NPhAhUlg-YKHV0pAf8QjRx6BAgBEAU&url=https%3A%2F%2Fwww.facebook.com%2FTESnews%2F&psig=AOvVaw3K_KiNmHtPoEzRWkk04S5E&ust=1555482076956416';
        $data['data']['notification']['data']= 'Category';
        $data['registration_ids'] = $user;
              
        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', 'https://fcm.googleapis.com/fcm/send', [
            'headers' => [
                'Authorization'=>'key=AAAAvt7x7fI:APA91bF5kzXbe0Vj6EEwfZe-yVoWT_jHIgqgELA7epYf-XPq6jxZ-NZQbf_jIFy3576OhkKoYOOoPaJJHKrgwCSWvxFrscIgx6xuwGZmLm8Lx3X3m9KctsThMP3n-NVUVzmH1xFRfLD9',
                'Accept' => 'application/json',
                'Content-type' => 'application/json'
            ],
           \GuzzleHttp\RequestOptions::JSON => $data,
            'debug' => TRUE,
        ]);
       
        $response = $response->getBody()->getContents();
    } 
}
