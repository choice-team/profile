<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class State extends Model
{
    //
    use SoftDeletes,LogsActivity;
    protected static $logFillable = true;
    protected $table = 'state';
    protected $fillable = ['id','state_name','updated_by','countries_id','created_at','updated_at','deleted_at'];

    public function city(){
        return $this->hasMany('App\City', 'state_id','id');
    }

     /**
     * Get the country that owns the state.
     */
    public function country()
    {
        return $this->belongsTo('App\Countries');
    }

    protected $hidden= [];

}
