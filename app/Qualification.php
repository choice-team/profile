<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class Qualification extends Model
{
   use SoftDeletes,LogsActivity;
   protected static $logFillable = true;
    protected $table = 'qualification';
    protected $fillable = ['qualification_name','area_of_specialisation','updated_by','created_at','updated_at','deleted_at'];
    public function qualification(){
        // echo "DFdfd"; exit;
        return $this->belongsToMany('App\UserQualification');
    }

}
