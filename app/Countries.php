<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

class Countries extends Model
{
   use SoftDeletes,LogsActivity;
   protected static $logFillable = true;
    protected $table = 'countries';
    protected $fillable = ['name','created_at','updated_by','updated_at','deleted_at'];

    //  public function expertise(){
    //     // echo "DFdfd"; exit;
    //     return $this->belongsToMany('App\MentorSkill','skill_id','id');
    // }

    public function state(){
        return $this->hasMany('App\State', 'countries_id','id');
    }
}
