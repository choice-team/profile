<?php
namespace App\Http\Controllers;

use App\Users;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Permissions\HasPermissionsTrait;
use App\Models\Module;
use App\Services\Slug;
use App\Models\Permission;

class RoleController extends Controller
{
    use HasPermissionsTrait;

    // protected $slugName;

    // public function __construct(Slug $slug)
    // {
    //    $this->slugName = $slug;
    // }

   /* @api {GET} api/profile/roles Get Roles List
  * @apiName index
  * @apiGroup Admin
  * @apiDescription This api is used to get all list of roles in the WEP Website.
  * @apiHeader {String} Authorization api token key.
  * @apiSuccessExample Success-Response-2000:
   HTTP/1.1 2000 OK
     {
       "status_code": 2000,
       "message": "Success",
        "body": {
        "current_page": 1,
        "data": [
            {
                "id": 31,
                "slug": "state-admin-1",
                "name": "Administer roles & permissions",
                "updated_by": "rupesh p",
                "created_at": "2019-01-21 17:22:15",
                "updated_at": "2019-01-30 13:52:17",
                "permissions": [
                    {
                        "id": 13,
                        "category": "permissions",
                        "sub_category": "",
                        "module_name": "user management",
                        "slug": "permission-update",
                        "name": "Prmission Update",
                        "updated_by": 3,
                        "created_at": "2019-01-22 15:53:46",
                        "updated_at": "2019-01-22 15:53:46",
                        "pivot": {
                            "roles_id": 31,
                            "permissions_id": 13
                        }
                    }
                ]
            }
         ]
       }
    }

  * @apiErrorExample Error-Response-4001:
  *   HTTP/1.1 4001 Unauthorized
  *   {
   "status_code": 4001,
   "message": "Unauthorized Accecss",
   "body": []
   }
  *
  * @apiErrorExample Error-Response-5000:
  *   HTTP/1.1 5000 Internal Server Error
  *      {
   "status_code": 5000,
   "message": "Internal Error, Try again later",
   "body": []
   }
  */
    public function index(Request $request) {
            $limit = $request->limit? $request->limit:10;
            $roleData = Role::with('permissions')->where('id','!=',1);
            

            if($request->sort == 'roleName') {
                $data = ($request->order == 1)? $roleData->orderBy('name') : $roleData->orderBy('name','DESC');
            }
            if($request->sort == 'roleType') {
                $data = ($request->order == 1)? $roleData->orderBy('type') : $roleData->orderBy('type','DESC');
            } else if($request->sort == 'updatedAt') {
                $data = ($request->order == 1) ? $roleData->orderBy('updated_at') : $roleData->orderBy('updated_at','DESC');
            } else {
                $data = $roleData->orderBy('created_at','DESC');
            }
            if(isset($request->keyword) && !empty($request->keyword)) {
                $data = $roleData->where('name','like','%'.$request->keyword.'%');
            }

            if(isset($request->flag)) {
                $data = $data->get();
            } else {
                $data = $data->paginate($limit); 
            }
            if($data->count()==0) {
                return new JsonResponse(['message' => 'No Data found'], 200);
            }
            foreach($data as $value) {
                $userName = Users::find($value['updated_by']);
                $value['updated_by'] = ($userName) ? trim($userName['first_name'].' '.$userName['last_name']):'';
            }
            return new JsonResponse($data);
    }

     /* @api {GET} api/profile/role/{id} Get Role
  * @apiName show
  * @apiGroup Admin
  * @apiDescription This api is used to get  roles in the WEP Website.
  * @apiHeader {String} Authorization api token key.
  * @apiParam {id} Role id
  * @apiSuccessExample Success-Response-2000:
   HTTP/1.1 2000 OK
     {
       "status_code": 2000,
       "message": "Success",
        "body": {
        "data": [
            {
                "id": 31,
                "slug": "state-admin-1",
                "name": "Administer roles & permissions",
                "updated_by": "rupesh p",
                "created_at": "2019-01-21 17:22:15",
                "updated_at": "2019-01-30 13:52:17",
                "permissions": [
                    {
                        "id": 13,
                        "category": "permissions",
                        "sub_category": "",
                        "module_name": "user management",
                        "slug": "permission-update",
                        "name": "Prmission Update",
                        "updated_by": 3,
                        "created_at": "2019-01-22 15:53:46",
                        "updated_at": "2019-01-22 15:53:46",
                        "pivot": {
                            "roles_id": 31,
                            "permissions_id": 13
                        }
                    }
                ]
            }
         ]
       }
    }

  * @apiErrorExample Error-Response-4001:
  *   HTTP/1.1 4001 Unauthorized
  *   {
   "status_code": 4001,
   "message": "Unauthorized Accecss",
   "body": []
   }
  *
  * @apiErrorExample Error-Response-5000:
  *   HTTP/1.1 5000 Internal Server Error
  *      {
   "status_code": 5000,
   "message": "Internal Error, Try again later",
   "body": []
   }
  */
    public function show($id) {
        try{
            $roleData = Role::with(['permissions'=> function($q){
                $q->Distinct();
            },'modules'=> function($q){
                $q->select('modules.id');
            }])->find($id);
            if($roleData == null)
                return new JsonResponse(['message' => 'Not found'], 200);
            
            $userName = Users::find($roleData->updated_by);
            $roleData['updated_by'] = ($userName) ? trim($userName['first_name'].' '.$userName['last_name']):'';
            $roleData['module_id'] = $roleData['modules']->map(function($item,$key){
                return $item->id;
            });
            unset($roleData['modules']);
            return new JsonResponse($roleData);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

    public function store(Request $request) {
       
       // try {
            $this->validate($request,[
                'name'   => 'required|unique:roles,name,NULL,id,deleted_at,NULL'
            ]);
            $data = $request->toArray();
           
            $data['updated_by'] = $request->user_id;
            $arrayModule = $request->module_id;

          
            //$module = Module::whereIn('id',$arrayModule)->pluck('slug');
            $query = Module::find($arrayModule);
            $module = $query->pluck('slug');
          //  dd($module);
            $slugName = $request->type.'-';
            foreach ($module as $item) {
                $slugName.=$item.'-';
            }
            
            $data['slug'] = strtolower(trim($slugName,'-'));
            $data['slug'] =  Slug::createSlug($data['slug'],'roles','slug');
            $roleData = new Role($data);
            // HERE: additional, non-fillable fields

            $roleData->save();
            if($roleData)
                $roleData->modules()->attach($query);
                return new JsonResponse([$roleData], 200);

            return new JsonResponse(['message' => 'Server Error'], 500);
        // } catch (\Illuminate\Validation\ValidationException $e) {
        //     return validation_exception($e);
        // } catch (\Exception $e) {
        //     return general_expection($e);
        // }
    }

    public function destroy(Request $request, $id) {
        try {
            $roleData = Role::with('permissions')->find($id);
            if($roleData === null)
                return new JsonResponse(['message' => 'Not found'], 200);
            if($roleData->permissions)    
                $roleData->withdrawPermissionsTo( $roleData->permissions->pluck('id'));
                $roleData->modules()->detach();
                $roleData->users()->detach();
            $roleData->delete();

            return new JsonResponse(['message'=>'Deleted Sucessfully'], 200);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

    public function update($id, Request $request) {
    
       try {
        $this->validate($request,[
          'name' => 'unique:roles,name,'.$id.',id,deleted_at,NULL'
        ]);
            $roleData = Role::find($id);
           
            $permission = $request->permissions;

            if($roleData === null)
                return new JsonResponse(['message' => 'Not found'], 200);

            $data = $request->toArray();
            $data['updated_by'] = $request->user_id;
            $data['updated_at'] =  date('Y-m-d H:i:s');

           
            if(isset($request->module_id)) {
                $module = Module::find($request->module_id);
                $roleData->modules()->sync($module);
            }
            $roleData->update($data);
            $accessData = array();
            if($permission) {
                foreach($permission as $val) {
                    if(count($val['access'])) {
                        $access_id =  Permission::where('module_id',$val['module_id'])->whereIn('operation_id',$val['access'])->select('id')->get()->toArray();
                        if(count($access_id)) {
                            $accessData[] =$access_id;
                        }
                    }
                }
               
                $accessData = $this->array_flatten($accessData);
                $roleData->givePermissionsTo($accessData);
            }
            return new JsonResponse([]);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

    function array_flatten($array) { 
        if (!is_array($array)) { 
          return FALSE; 
        } 
        $result = array(); 
        foreach ($array as $key => $value) { 
          if (is_array($value)) { 
            $result = array_merge($result, array_flatten($value)); 
          } 
          else { 
            $result[$key] = $value; 
          } 
        } 
        return $result; 
    } 

    public function search(Request $request, $role) {
        try {
            $search = str_replace("%20"," ",$role);
            $limit = $request->limit?$request->limit:10;
            $data = Role::with('permissions')
            ->where('name','like','%'.$search.'%')->paginate($limit);
            
            if($data->count() == 0)
                return new JsonResponse(['message' => 'Not found'], 200);

            foreach($data as $value) {
                $userName = Users::find($value->updated_by);
                $value['updated_by'] = ($userName) ? trim($userName['first_name'].' '.$userName['last_name']):'';
            }
            return new JsonResponse($data);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

    public function rolesPermissionDashboard(Request $request) {
        $role_id = DB::table('users_roles')->where('user_id',$request->user_id)->get()->pluck('role_id');
        $roleData = Role::with(['permissions'=> function($q){
            $q->DISTINCT();
        }])->whereIn('id',$role_id)->get();
        $dev= [];
        foreach($roleData as $value) {
            foreach($value['permissions'] as $val) {
                array_push($dev,$val);
            }
        }
        return $dev;
    }

    //to map module and operation in permissions table
    public function mapper() {
       $module = DB::table('modules')->whereNull('deleted_at')->get()->pluck('name','id');
       $operation = DB::table('operations')->whereNull('deleted_at')->get()->pluck('name','id');
       foreach($module as $key=>$value) {
            foreach($operation as $k=>$v) {
                $a = strtolower($value.' '.$v);
                $a = str_replace(' ', '-', $a);
                DB::table('permissions')->insert(['slug'=>$a,'module_id'=>$key,'operation_id'=>$k]);
            }
       }
       return new JsonResponse();
    }

    public function restore($id) {
        Role::withTrashed()->find($id)->restore();
        return new JsonResponse([],200);
    }
}
