<?php

namespace App\Http\Controllers;

use App\Language;
use App\HomePageContent;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use App\Services\LanguageService;
use function GuzzleHttp\json_decode;
use PHPUnit\Runner\Exception;
use Log;
use App\Jobs\LanguageTranslationJob;
use Illuminate\Support\Facades\Queue;


class HomePageContentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      
    }

    public function getMenuItem(Request $request) {
      $limit = $request->limit?$request->limit:10;
      // $array = ["%20","/",'(',')','?'];
      // $search = str_replace($array," ",$request->search);
      $search = $request->search;
      if(isset($search) && $search) {

      // $data = DB::raw("select * from `home_page_content` where (`language` = 'en' and `menu_name` 
      //      like '%".$search."%' or `main_title` like '%".$search."%' or `sub_title` like '%".$search."%') and `home_page_content`.`deleted_at` is null");

       // dd($data);

       $data = HomePageContent::where('language','en')->where('menu_name','like','%'.$search.'%')
        ->orWhere('main_title','like','%'.$search.'%')
        ->orWhere('sub_title','like','%'.$search.'%')
        ->paginate($limit);

        //dd($data);
        return $data;
      }
      $data = HomePageContent::where('language','en')->paginate($limit);
      return $data;
       // return DB::table('home_page_content')->select(DB::Raw('DISTINCT(menu_name) as menu_name'),'id')->where('language','en')->groupBy('menu_name')->get();
    }

    // public function getMainTitleId() {
    //     $menu =  DB::table('home_page_content')->select(DB::Raw('DISTINCT(main_title) as main_title'))->where('language','en')->groupBy('main_title')->get();
    //     $data=[];
    //     $count=0;
    //     foreach($menu as $key => $value) {
    //         $data[$count]['name']= $value->main_title;
    //         $data[$count]['id'] = DB::table('home_page_content')->where(['language'=>'en','main_title'=>$value->main_title])->get()->pluck('id');
    //         $count++;
    //     }
    //     return $data;
    // }
    // public function getMainTitle(Request $request) {
    //     return DB::table('home_page_content')->select(DB::Raw('DISTINCT(main_title) as main_title'),'id')->where('language','en')->where('menu_name',$request->name)->groupBy('main_title')->get();
    // }

    // public function getSubTitle(Request $request) {
    //     return DB::table('home_page_content')->select('sub_title','id','description')->where(['language'=>'en','main_title'=>$request->name])->get();
    // }

    public function getDetailLanguage(Request $request) {
    
      $data = HomePageContent::select(DB::Raw('DISTINCT(main_title) as main_title'),'sub_title','description','id','language')->where(['id'=>$request->id])->groupBy('main_title')->first();
      
      $languageData = HomePageContent::select(DB::Raw('DISTINCT(main_title) as main_title'),'sub_title','description','id','language')->where(['parent_id'=>$request->id])->groupBy('main_title')->get()->toArray();
    
      array_push($languageData,$data);
      return $languageData;
    }

/* @api {POST} api/profile/content-update Update the content
  * @apiName updateContent
  * @apiGroup Admin
  * @apiDescription This api is used to update content side based on language.
  * @apiHeader {String} Authorization api token key.
  * @apiParam
   "content":[
    {	
      "main_title":"about weps",
      "language":"en"},{
              "main_title": "रोने के बारे में",
              "language": "hi"
              },
              {"main_title": "વીપ્સ વિશે", "language": "gj"}
			]
    }
  * @apiSuccessExample Success-Response-2000:
   HTTP/1.1 2000 OK
     {
       "status_code": 2000,
       "message": "Success",
        "body": {
        "current_page": 1,
        "data": [
            {
                "parent_id": 31,
                "translate": [
                    {
                        "language":"en",
                        "main_title":"title",
                        "sub_title":"sub_title"
                    }
                ]
            }
         ]
       }
    }

  * @apiErrorExample Error-Response-4001:
  *   HTTP/1.1 4001 Unauthorized
  *   {
   "status_code": 4001,
   "message": "Unauthorized Accecss",
   "body": []
   }
  *
  * @apiErrorExample Error-Response-5000:
  *   HTTP/1.1 5000 Internal Server Error
  *      {
   "status_code": 5000,
   "message": "Internal Error, Try again later",
   "body": []
   }
  */
   public function updateContent(Request $request) {
    $content  = $request->content;
    foreach($content as $value) {
      $data = HomePageContent::find($value['id']);
      HomePageContent::where(['main_title'=>$data['main_title'],'language'=>$value['lang']])->update(['main_title'=>$value['data']]);
    }
    return new JsonResponse([],200);
  }

    /* @api {POST} api/profile/subtitle-update Update the sub-title content
  * @apiName updateSubTitle
  * @apiGroup Admin
  * @apiDescription This api is used to update content side based on language.
  * @apiHeader {String} Authorization api token key.
  * @apiParam
   "content":[
    {	
      "main_title":"about weps",
      "language":"en"},{
              "main_title": "रोने के बारे में",
              "language": "hi"
              },
              {"sub_title": "વીપ્સ વિશે", "language": "gj"}
			]
    }
  * @apiSuccessExample Success-Response-2000:
   HTTP/1.1 2000 OK
     {
       "status_code": 2000,
       "message": "Success",
        "body": {
        "current_page": 1,
        "data": [
            {
                "parent_id": 31,
                "translate": [
                    {
                        "language":"en",
                        "main_title":"title",
                        "sub_title":"sub_title"
                    }
                ]
            }
         ]
       }
    }

  * @apiErrorExample Error-Response-4001:
  *   HTTP/1.1 4001 Unauthorized
  *   {
   "status_code": 4001,
   "message": "Unauthorized Accecss",
   "body": []
   }
  *
  * @apiErrorExample Error-Response-5000:
  *   HTTP/1.1 5000 Internal Server Error
  *      {
   "status_code": 5000,
   "message": "Internal Error, Try again later",
   "body": []
   }
  */

   public function updateSubTitle(Request $request) {
    $content  = $request->content;
    foreach($content as $value) {
      HomePageContent::where(['id'=>$value['id'],'language'=>$value['lang']])->update(['sub_title'=>$value['data']]);
    }
    return new JsonResponse([],200);
    
  }


/* @api {POST} api/profile/description-update Update the sub-title content
  * @apiName updateDescription
  * @apiGroup Admin
  * @apiDescription This api is used to update content side based on language.
  * @apiHeader {String} Authorization api token key.
  * @apiParam
   "content":[
    {	
      "main_title":"about weps",
      "language":"en"},{
              "main_title": "रोने के बारे में",
              "language": "hi"
              },
              {"description": "વીપ્સ વિશે", "language": "gj"}
			]
    }
  * @apiSuccessExample Success-Response-2000:
   HTTP/1.1 2000 OK
     {
       "status_code": 2000,
       "message": "Success",
        "body": {
        "current_page": 1,
        "data": [
            {
                "parent_id": 31,
                "translate": [
                    {
                        "language":"en",
                        "main_title":"title",
                        "sub_title":"sub_title"
                    }
                ]
            }
         ]
       }
    }

  * @apiErrorExample Error-Response-4001:
  *   HTTP/1.1 4001 Unauthorized
  *   {
   "status_code": 4001,
   "message": "Unauthorized Accecss",
   "body": []
   }
  *
  * @apiErrorExample Error-Response-5000:
  *   HTTP/1.1 5000 Internal Server Error
  *      {
   "status_code": 5000,
   "message": "Internal Error, Try again later",
   "body": []
   }
  */

   public function updatedescription(Request $request) {
    $content  = $request->content;
    foreach($content as $value) {
      HomePageContent::where(['id'=>$value['id'],'language'=>$value['lang']])->update(['description'=>$value['data']]);
    }
    return new JsonResponse([],200);
  }

  // public function showLanguageData(Request $request) {

  //   Queue::push(new LanguageTranslationJob($request->string));

  //    $data = DB::table('translated_data')->select('short_name','data')->get()->toArray();
  //    $translation=[];
  //    foreach ($data as $key => $value) {
  //    	$translation[$value->short_name] = $value->data;
  //    }
  //    return $translation;
  // }

    public function showLanguageData(Request $request) {
      $language =  Language::pluck('short_name');

      $data=[];

      if(isset($request->translate_lang)&& $request->translate_lang) {
        $string = LanguageService::translateLanguage($request->string,$request->translate_lang);

        $string = json_decode($string,true);
        $char = $string['data']['translations'][0]['translatedText'];
         $data[$request->translate_lang] = $char;
         return $data;
      }

      foreach($language as $value) {
        if($value=='en') {
            $char = $request->string;
        } else {
            $string = LanguageService::translateLanguage($request->string,$value);
            $string = json_decode($string,true);
            $char = $string['data']['translations'][0]['translatedText'];
        }
        $data[$value] = $char;
      }
      return $data;
    }
}
