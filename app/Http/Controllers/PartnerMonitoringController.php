<?php

namespace App\Http\Controllers;

use DB;
use App\Users;
use App\Models\Events;
use App\Models\ForumQuestions;
use App\Models\ForumThread;

use App\Models\Blog;
use Illuminate\Http\Request;
use Illuminate\Http\Response;


class PartnerMonitoringController extends Controller
{

  /**
   * @api {GET} api/profile/dashboard/partner-wise-blog-count Blog Count Partner wise
   * @apiName TotalCount of Partner Wise Blog 
   * @apiGroup Blog
   * @apiDescription Blog Count
   * @apiSuccessExample Success-Response-2000:
    HTTP/1.1 2000 OK
{
    "status_code": "2000",
    "message": "success",
    "body": [
        {
            "total_count": 3
        }
    ]
}

   * @apiErrorExample Error-Response-4001:
   *   HTTP/1.1 4001 Unauthorized
   *   
    {
    "status_code": 4001,
    "message": "Unauthorized Accecss",
    "body": []
    }
   *
   * @apiErrorExample Error-Response-5000:
   *   HTTP/1.1 5000 Internal Server Error
   *      {
    "status_code": 5000,
    "message": "Internal Error, Try again later",
    "body": []
    }
   */
    public function partnerWiseBlogCount() {

      $total =  DB::select(DB::raw("SELECT count(blog_posts.blog_user_id) as total_count FROM `users`
        left join blog_posts on blog_posts.blog_user_id = users.id
        WHERE users.user_type='partner' and users.deleted_at is null and blog_posts.deleted_at is null 
        and blog_status ='Published'"));

      return $total; 
    }
    /**
   * @api {post} api/profile/dashboard/partner-wise-blog-list Blog List Partner Wise
   * @apiName Blog List Partner Wise  
   * @apiGroup Blog
   * @apiDescription Blog List
   * @apiSuccessExample Success-Response-2000:
    HTTP/1.1 2000 OK
{
    "status_code": "2000",
    "message": "success",
    "body": {
        "current_page": 1,
        "data": [
            {
                "blog_count": 2,
                "first_name": "Bharat",
                "user_slug": "us-india"
            },
            {
                "blog_count": 4,
                "first_name": "US India",
                "user_slug": "us-india"
            }
        ],
        "first_page_url": "http://wepprofile.choicetechlab.com/api/profile/dashboard/partner-wise-blog-list?page=1",
        "from": 1,
        "last_page": 1,
        "last_page_url": "http://wepprofile.choicetechlab.com/api/profile/dashboard/partner-wise-blog-list?page=1",
        "next_page_url": null,
        "path": "http://wepprofile.choicetechlab.com/api/profile/dashboard/partner-wise-blog-list",
        "per_page": 15,
        "prev_page_url": null,
        "to": 2,
        "total": 2
    }
}

   * @apiErrorExample Error-Response-4001:
   *   HTTP/1.1 4001 Unauthorized
   *   
    {
    "status_code": 4001,
    "message": "Unauthorized Accecss",
    "body": []
    }
   *
   * @apiErrorExample Error-Response-5000:
   *   HTTP/1.1 5000 Internal Server Error
   *      {
    "status_code": 5000,
    "message": "Internal Error, Try again later",
    "body": []
    }
   */


    public function partnerWiseBlogList(Request $request) {
     $limit = $request->limit ? $request->limit : 10;
     $offset = $request->offset ? $request->offset : 0;

     $data = Blog::select('users.first_name', 'users.user_slug',DB::raw("count(blog_posts.blog_user_id) as blog_count"),DB::raw("( select count(user_blog_like_mapper.blog_id) from user_blog_like_mapper where user_blog_like_mapper.blog_id = blog_posts.id and user_blog_like_mapper.like_status =1 and user_blog_like_mapper.deleted_at is null )as likes"),DB::raw("(select count(user_blog_share_mapper.blog_id) from user_blog_share_mapper where user_blog_share_mapper.blog_id = blog_posts.id  and user_blog_share_mapper.deleted_at is null )as share"))
     ->leftjoin('users','blog_posts.blog_user_id' ,'=', 'users.id')
     ->where('users.user_type','=', 'partner')
     ->where(['blog_status' => 'Published','users.deleted_at' => null, 'blog_posts.deleted_at' => null])
     ->groupBy('blog_posts.id');

     if($request->sort == 'first_name') {
      $data = ($request->order == 1)? $data->orderBy('first_name') : $data->orderBy('first_name','DESC');
    }

    if(isset($request->search) && !empty($request->search)) {
     $data = $data->where('users.first_name','like','%'.$request->search.'%');
   }

   $data = $data->get()->toArray();

   $result = array_reduce($data, function($carry, $item) { 
    if(!isset($carry[$item['user_slug']])) {
      $carry[$item['user_slug']] = $item;
    } else {
      $carry[$item['user_slug']]['blog_count'] += $item['blog_count'];
      $carry[$item['user_slug']]['likes'] += $item['likes'];
      $carry[$item['user_slug']]['share'] += $item['share'];
    }
    return $carry;
  });

   $result = array_values($result);
   
   return $result;

 }

/**
   * @api {GET} api/profile/dashboard/partner-wise-event-count Event Count Partner Wise
   * @apiName TotalCount of Partner Wise Event 
   * @apiGroup Event
   * @apiDescription Event Count Partner Wise
   * @apiSuccessExample Success-Response-2000:
    HTTP/1.1 2000 OK
{
    "status_code": "2000",
    "message": "success",
    "body": [
        {
            "total_count": 3
        }
    ]
}

   * @apiErrorExample Error-Response-4001:
   *   HTTP/1.1 4001 Unauthorized
   *   
    {
    "status_code": 4001,
    "message": "Unauthorized Accecss",
    "body": []
    }
   *
   * @apiErrorExample Error-Response-5000:
   *   HTTP/1.1 5000 Internal Server Error
   *      {
    "status_code": 5000,
    "message": "Internal Error, Try again later",
    "body": []
    }
   */
    public function partnerWiseEventCount() {

      $data =  DB::select(DB::raw("SELECT events.id, count(events.partner_id) as total_count FROM `events` 
        join users on events.partner_id = users.id WHERE users.user_type='partner' and users.deleted_at is null and events.deleted_at is null and events.status='approved'"));
      return $data;
    }
   /**
   * @api {post} api/profile/dashboard/partner-wise-event-list Event List Partner Wise
   * @apiName Event List Partner Wise  
   * @apiGroup Event
   * @apiDescription Event List Partner Wise
   * @apiSuccessExample Success-Response-2000:
    HTTP/1.1 2000 OK
{
    "status_code": "2000",
    "message": "success",
    "body": {
        "current_page": 1,
        "data": [
            {
                "event_count": 2,
                "first_name": "Bharat",
                "user_slug": "us-india"
            },
            {
                "event_count": 2,
                "first_name": "deAsra Foundation",
                "user_slug": "deasra-foundation"
            },
            {
                "event_count": 2,
                "first_name": "Choice Techlab",
                "user_slug": "choice-techlab"
            }
        ],
        "first_page_url": "http://wepprofile.choicetechlab.com/api/profile/dashboard/partner-wise-event-list?page=1",
        "from": 1,
        "last_page": 1,
        "last_page_url": "http://wepprofile.choicetechlab.com/api/profile/dashboard/partner-wise-event-list?page=1",
        "next_page_url": null,
        "path": "http://wepprofile.choicetechlab.com/api/profile/dashboard/partner-wise-event-list",
        "per_page": 15,
        "prev_page_url": null,
        "to": 3,
        "total": 3
    }
}

   * @apiErrorExample Error-Response-4001:
   *   HTTP/1.1 4001 Unauthorized
   *   
    {
    "status_code": 4001,
    "message": "Unauthorized Accecss",
    "body": []
    }
   *
   * @apiErrorExample Error-Response-5000:
   *   HTTP/1.1 5000 Internal Server Error
   *      {
    "status_code": 5000,
    "message": "Internal Error, Try again later",
    "body": []
    }
   */

    public function partnerWiseEventList(Request $request) {
      $limit = $request->limit ? $request->limit : 10;
      $offset = $request->offset ? $request->offset : 0;

      $data = Events::select('users.first_name', 'users.user_slug',DB::raw("count(events.partner_id) as event_count"),DB::raw("(select count(event_attendees.event_id) from event_attendees where `event_attendees`.`event_id` = `events`.`id`and event_attendees.deleted_at is null) AS interested_people"),DB::raw("(select count(user_event_share_mapper.event_id) from user_event_share_mapper where user_event_share_mapper.event_id = events.id) as event_share"))
      ->leftjoin('users','users.id' ,'=', 'events.partner_id')
      ->where('users.user_type','=', 'partner')
      ->where(['events.status'=> 'approved','users.deleted_at'=>null])
      ->groupBy('events.id');


      if(isset($request->search) && !empty($request->search)) {
        $data = $data->where('users.first_name','like','%'.$request->search.'%');
      }

      $data = $data->get()->toArray();
      
      
      $result = array_reduce($data, function($carry, $item) { 
        if(!isset($carry[$item['user_slug']])) {
          $carry[$item['user_slug']] = $item;
        } else {
          $carry[$item['user_slug']]['event_count'] += $item['event_count'];
          $carry[$item['user_slug']]['interested_people'] += $item['interested_people'];
          $carry[$item['user_slug']]['event_share'] += $item['event_share'];
        }
        return $carry;
      });

      $result = array_values($result);

      return $result;

    }
    /**
   * @api {get} api/profile/dashboard/partner-wise-community-count Community Partner Count
   * @apiName TotalCount of Partner Wise Community 
   * @apiGroup Community
   * @apiDescription Community Partner Count
   * @apiSuccessExample Success-Response-2000:
    HTTP/1.1 2000 OK
{
    "status_code": "2000",
    "message": "success",
    "body": [
        {
            "total_count": 3
        }
    ]
}

   * @apiErrorExample Error-Response-4001:
   *   HTTP/1.1 4001 Unauthorized
   *   
    {
    "status_code": 4001,
    "message": "Unauthorized Accecss",
    "body": []
    }
   *
   * @apiErrorExample Error-Response-5000:
   *   HTTP/1.1 5000 Internal Server Error
   *      {
    "status_code": 5000,
    "message": "Internal Error, Try again later",
    "body": []
    }
   */
    public function partnerWiseCommunityCount() {
      $data =  DB::select(DB::raw("SELECT count(forum_questions.user_id) as total_count FROM `forum_questions` 
        join users on forum_questions.user_id = users.id WHERE users.user_type='partner' 
        and users.deleted_at is null and forum_questions.status ='Approved' and forum_questions.deleted_at is null"));
      return $data;
    }

      /**
   * @api {post} api/profile/dashboard/partner-wise-community-list Community List Partner Wise
   * @apiName Community List Partner Wise  
   * @apiGroup Community 
   * @apiDescription Community List
   * @apiSuccessExample Success-Response-2000:
    HTTP/1.1 2000 OK
{
    "status_code": "2000",
    "message": "success",
    "body": {
        "current_page": 1,
        "data": [
            {
                "community_count": 2,
                "first_name": "Bharat",
                "user_slug": "us-india"
            },
            {
                "community_count": 1,
                "first_name": "US India",
                "user_slug": "us-india"
            }
        ],
        "first_page_url": "http://wepprofile.choicetechlab.com/api/profile/dashboard/partner-wise-community-list?page=1",
        "from": 1,
        "last_page": 1,
        "last_page_url": "http://wepprofile.choicetechlab.com/api/profile/dashboard/partner-wise-community-list?page=1",
        "next_page_url": null,
        "path": "http://wepprofile.choicetechlab.com/api/profile/dashboard/partner-wise-community-list",
        "per_page": 15,
        "prev_page_url": null,
        "to": 2,
        "total": 2
    }
}

   * @apiErrorExample Error-Response-4001:
   *   HTTP/1.1 4001 Unauthorized
   *   
    {
    "status_code": 4001,
    "message": "Unauthorized Accecss",
    "body": []
    }
   *
   * @apiErrorExample Error-Response-5000:
   *   HTTP/1.1 5000 Internal Server Error
   *      {
    "status_code": 5000,
    "message": "Internal Error, Try again later",
    "body": []
    }
   */
    public function partnerWiseCommunityList(Request $request) {
       $limit = $request->limit ? $request->limit : 10;
     $offset = $request->offset ? $request->offset : 0;
   
     $data = ForumQuestions::select('users.first_name','users.user_slug',DB::raw("(select count(user_forum_like_mapper.question_id) from user_forum_like_mapper where user_forum_like_mapper.question_id = forum_questions.id and user_forum_like_mapper.deleted_at is null and like_status = 1) as likes"),DB::raw("count(forum_questions.user_id) as community_count"),DB::raw("(select count(forum_thread.question_id) from forum_thread where `forum_questions`.`id` =`forum_thread`.`question_id`) as answers "))
     ->leftjoin('users','forum_questions.user_id', '=', 'users.id')
     ->where(['users.user_type' =>'partner','forum_questions.status'=>'Approved', 'forum_questions.deleted_at' =>null,'users.deleted_at'=>null])
     ->groupBy("forum_questions.id");
 

     if(isset($request->search) && !empty($request->search)) {
       $data = $data->where('users.first_name','like','%'.$request->search.'%');
     }

    $data = $data->get()->toArray();

      $result = array_reduce($data, function($carry, $item) { 
        if(!isset($carry[$item['user_slug']])) {
          $carry[$item['user_slug']] = $item;
        } else {
          $carry[$item['user_slug']]['likes'] += $item['likes'];
          //$carry[$item['user_slug']]['answers_likes'] += $item['answers_likes'];
          $carry[$item['user_slug']]['answers'] += $item['answers'];
          $carry[$item['user_slug']]['community_count'] += $item['community_count'];
        }
        return $carry;
      });

      $result = array_values($result);


      return $result;
   }

 }