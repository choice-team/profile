<?php

namespace App\Http\Controllers;

use DB;
use App\Users;
use App\parnter;
use App\Category;
use App\Language;
use App\Enterpreneur;
use App\UsersProfile;
use App\PartnerServices;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use App\Models\EntrepreneurBusinessInfo;
use App\PartnerEntrepreneurRelationship;
use App\UserMentorPartnerConnectionMapper;


class UsersDashboardController extends Controller
{
    private $user_id;

    public function __construct()
    {
        $this->user_id = Auth::user()->id;
    }

    public function getCount(Request $request){
        
        try {
          
            $connectedPartnerCount = UserMentorPartnerConnectionMapper::join('users','user_mentor_partner_connection_mapper.connected_to','=','users.id')
            ->where('users.user_type', 'partner');
            if(isset($request->user_slug) && !empty($request->user_slug)){
               $connected_partners_id =  Users::select('id')->where('user_slug','=',$request->user_slug)->first();

               $connectedPartnerCount =   $connectedPartnerCount->where('user_mentor_partner_connection_mapper.user_id', $connected_partners_id->id); 
           } else {
              $connectedPartnerCount =  $connectedPartnerCount->where('user_mentor_partner_connection_mapper.user_id', $this->user_id); 
          }

          $connectedPartnerCount = $connectedPartnerCount->where('user_mentor_partner_connection_mapper.connection_status', 1)->count();


            //echo $conncetedPartner;exit;

            $recentConnectedPartner = UserMentorPartnerConnectionMapper::select('users.user_slug as slug', 'users.first_name as name', 'partner_profile.logo', 'users.user_type', 'user_mentor_partner_connection_mapper.created_at','user_mentor_partner_connection_mapper.updated_at','partner_profile.slug')
                ->join('users','user_mentor_partner_connection_mapper.connected_to','=','users.id')
                ->leftjoin('partner_profile','user_mentor_partner_connection_mapper.connected_to','=','partner_profile.user_id')
                ->where('users.user_type', 'partner')
                ->where('user_mentor_partner_connection_mapper.user_id', $this->user_id)
                ->where('user_mentor_partner_connection_mapper.connection_status', 1)
                ->orderBy('user_mentor_partner_connection_mapper.updated_at','DESC')
                ->limit(5)
                ->get()
                ->transform(function($item,$key) {
                    $item->created_at = $item->updated_at?$item->updated_at:$item->created_at;
                    unset($item->updated_at);
                    return $item;
                });

            //print_r($recentConnectedPartner);exit;

            $connectedEntrepreneurCount = UserMentorPartnerConnectionMapper::join('users','user_mentor_partner_connection_mapper.connected_to','=','users.id')
            ->where('users.user_type', 'entrepreneur');
            if(isset($request->user_slug) && !empty($request->user_slug)){
             
                 $connected_entrepreneur_id =  Users::select('id')->where('user_slug','=',$request->user_slug)->first();
  
                 $connectedEntrepreneurCount =   $connectedEntrepreneurCount->where('user_mentor_partner_connection_mapper.user_id','=', $connected_entrepreneur_id->id);
             } else {
             
                $connectedEntrepreneurCount =  $connectedEntrepreneurCount->where('user_mentor_partner_connection_mapper.user_id','=', $this->user_id); 
            }
          $connectedEntrepreneurCount =  $connectedEntrepreneurCount->where('user_mentor_partner_connection_mapper.connection_status', 1)->count();

            //echo $connectedEntrepreneurCount;exit;

            $recentConnectedEntrepreneur = UserMentorPartnerConnectionMapper::select('users.user_slug as slug', 'users.first_name as name','users.last_name','user_profile.entreprise_name', 'user_profile.profile_pic', 'users.user_type', 'user_mentor_partner_connection_mapper.created_at','user_mentor_partner_connection_mapper.updated_at')
                ->join('users','user_mentor_partner_connection_mapper.connected_to','=','users.id')
                ->leftjoin('user_profile','user_mentor_partner_connection_mapper.connected_to','=','user_profile.user_id')
                ->where('user_mentor_partner_connection_mapper.user_id', $this->user_id)
                ->where('users.user_type', 'entrepreneur')
                ->where('user_mentor_partner_connection_mapper.connection_status', 1)
                ->orderBy('user_mentor_partner_connection_mapper.updated_at','DESC')
                ->limit(5)
                ->get()
                ->transform(function($item,$key) {
                    $item->created_at = $item->updated_at?$item->updated_at:$item->created_at;
                    unset($item->updated_at);
                    return $item;
                });

            //print_r($recentConnectedEntrepreneur);exit;
            
            return response()->json([
                    'connected_partner_count' => $connectedPartnerCount,
                    'recent_connected_partner' => $recentConnectedPartner,
                    'connected_entrepreneur_count' => $connectedEntrepreneurCount,
                    'recent_connected_entrepreneur' => $recentConnectedEntrepreneur,
                ],Response::HTTP_OK
            );

        } catch (\Exception $e) {

            return general_expection($e);
        }
    }
}
