<?php
namespace App\Http\Controllers;

use App\Users;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Models\SectorOfEnterprise;
use Illuminate\Support\Facades\DB;


class SectorOfEnterprisesController extends Controller
{
    public function index(Request $request) {
            $limit = $request->limit? $request->limit:10;

            $sector = SectorOfEnterprise::select('sector_of_enterprises.sector_of_enterprise','sector_of_enterprises.id','sector_of_enterprises.updated_at',DB::raw("TRIM(CONCAT_WS(' ',u.first_name,u.last_name)) as updated_by"),'sector_of_enterprises.deleted_at')
            ->leftjoin('users as u','u.id','=','sector_of_enterprises.updated_by');

            if($request->sort == 'name') {
                $data = ($request->order == 1)? $sector->orderBy('sector_of_enterprise') : $sector->orderBy('sector_of_enterprise','DESC');
            } else if($request->sort == 'date') {
                $data = ($request->order == 1) ? $sector->orderBy('updated_at') : $sector->orderBy('updated_at','DESC');
            } else {
                $data = $sector->orderBy('updated_at','DESC');
            }

             $search = str_replace("%20"," ",$request->keyword);
            if(isset($search) && !empty($search))
            {
                $data = $sector->where('sector_of_enterprises.sector_of_enterprise','like',$search.'%');
            }
            $data = isset($request->flag)?$data->withTrashed()->get():$data->withTrashed()->paginate($limit);
            // $data = $data->paginate($limit);

            if($data->count()==0) {
                return new JsonResponse(['message' => 'No Data found'], 200);
            } 
            
            return new JsonResponse($data);
        
    }

    public function show($id) {
        try {
            $sector_of_enterprise = SectorOfEnterprise::find($id);

            if($sector_of_enterprise === null)
                return new JsonResponse(['message' => 'No Data found'], 200);

            $userName = Users::where('id',$sector_of_enterprise['updated_by'])->first();
            if($userName) {
                $sector_of_enterprise['updated_by'] =  trim($userName->first_name.' '.$userName->last_name);
            } else {
                $sector_of_enterprise['updated_by'] = '';
            }
            return new JsonResponse($sector_of_enterprise);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

    public function store(Request $request) {
        try {
            $this->validate($request,[
                'sector_of_enterprise'   => 'required|unique:sector_of_enterprises'
            ]);

            $data = $request->toArray();
            $data['updated_by'] = $request->user_id;

            $sector_of_enterprise = new SectorOfEnterprise($data);

            // HERE: additional, non-fillable fields

            $sector_of_enterprise->save();

            if($sector_of_enterprise)
                return new JsonResponse([],200);

            return new JsonResponse(['message' => 'Server Error'], 500);
        } catch (\Illuminate\Validation\ValidationException $e) {
            return validation_exception($e);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

    public function destroy($id) {
        try {
            $sector_of_enterprise = SectorOfEnterprise::find($id);

            if($sector_of_enterprise === null)
                return new JsonResponse(['message' => 'Not found'], 404);

            $sector_of_enterprise->delete();

            return new JsonResponse(['message' => 'Deleted Successfuly'], 200);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

    public function update($id, Request $request) {
        try {
            $sector_of_enterprise = SectorOfEnterprise::find($id);

            if($sector_of_enterprise === null)
                return new JsonResponse(['message' => 'Not found'], 404);
                
            $data = $request->toArray();
            $data['updated_by'] = $request->user_id;
            $sector_of_enterprise->update($data);

            return new JsonResponse([],200);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

    public function search(Request $request, $enterprise) {
        try {
            $limit = $request->limit?$request->limit:10;
            $data = SectorOfEnterprise::
            select('sector_of_enterprises.sector_of_enterprise','sector_of_enterprises.id','sector_of_enterprises.updated_at',DB::raw("TRIM(CONCAT_WS(' ',u.first_name,u.last_name)) as updated_by"),'sector_of_enterprises.deleted_at')
            ->leftjoin('users as u','u.id','=','sector_of_enterprises.updated_by')
            ->where('sector_of_enterprises.sector_of_enterprise','like',$enterprise.'%')->paginate($limit);
            if(count($data) == 0)
                return new JsonResponse(['message' => 'No Data found'], 200);

            return new JsonResponse($data);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

    public function restore($id) {
        SectorOfEnterprise::withTrashed()->find($id)->restore();
        return new JsonResponse([],200);
    }
}
