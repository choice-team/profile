<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Role;
use App\Http\Requests;
use App\Models\Module;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $role_id = DB::table('users_roles')->where('user_id', Auth::user()->id)->get()->pluck('role_id');
        $roleData = Role::with(['permissions'=> function($q){
            $q->DISTINCT();
        }])->whereIn('id',$role_id)->get();
        $dev= [];
        foreach($roleData as $value) {
            foreach($value['permissions'] as $val) {
                array_push($dev,$val);
            }
        }
        $data =  Module::with('children')->whereIn('parent_id',[0,99999])->where('deleted_at',null)->get()
        ->transform(function($item,$key) use($dev) {
           if(count($item->children)>0)  {
                foreach ($item->children as $value) {
                    $operation = [];
                    foreach($dev as $perm) {
                        if($perm->module_id == $value->id && !in_array($perm->operation_id,$operation)) {
                        
                            array_push($operation, $perm->operation_id);
                        }
                    }
                $value->operations = $operation; 
                }
                $item->operations = [];
           } else {
                $operation = [];
                foreach($dev as $perm) {
                    if($perm->module_id == $item->id && !in_array($perm->operation_id,$operation)) {
                        array_push($operation, $perm->operation_id);
                    }
                }
                $item->operations = $operation;
           }
           return $item;
        });

        return $data;

    }

    public function store(Request $request) {
       try {
            $this->validate($request,[
                'name'   => 'required|unique:modules',
                'slug' => 'required|unique:modules',
            ]);
       
            $module = new Module($request->all());
            $module->save();
        return $module->id;
        } catch (\Illuminate\Validation\ValidationException $e) {
            return validation_exception($e);
        }
    }

    public function update(Request $request,$name) {
        try {
            $this->validate($request,[
                 'name'   => 'unique:modules',
                 'slug' => 'unique:modules',
            ]);
            $data = Module::where('name',$name)->update($request->all());
            return $data;
        } catch (\Illuminate\Validation\ValidationException $e) {
            return validation_exception($e);
        }
     }

    public function destroy($id) {
        $data = Module::find($id);
        $data->delete();
    }

    public function showOperation() {
        return DB::table('operations')->get();
    }
}