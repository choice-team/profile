<?php
namespace App\Http\Controllers;

use App\State;
use App\Users;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;


class StatesController extends Controller
{
    public function index(Request $request) {
        try {
            $limit = $request->limit? $request->limit:10;
            $state = new State();

            $state = State::select('state.state_name','state.id','state.updated_at',DB::raw("TRIM(CONCAT_WS(' ',u.first_name,u.last_name)) as updated_by"),'countries.name as country_name','countries.id as countries_id','state.deleted_at')
            ->leftjoin('users as u','u.id','=','state.updated_by')
            ->leftjoin('countries','countries.id','=','state.countries_id');

            if($request->sort == 'name') {
                $data = ($request->order == 1)? $state->orderBy('state_name') : $state->orderBy('state_name','DESC');
            } else if($request->sort == 'date') {
                $data = ($request->order == 1) ? $state->orderBy('updated_at') : $state->orderBy('updated_at','DESC');   
            } else {
                $data = $state->orderBy('updated_at','DESC');
            }

            if(isset($request->keyword) && !empty($request->keyword))
            {
                $data = $state->where('state.state_name','like',$request->keyword.'%');
            }
            
            $data = isset($request->flag)?$data->withTrashed()->get():$data->withTrashed()->paginate($limit);
            
            if($data->count()==0) {
                return new JsonResponse(['message' => 'Not found'], 404);
            } 
            
            return new JsonResponse($data);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

    public function show($id) {
        try {
            $state = State::withTrashed()->find($id);

            if($state === null)
                return new JsonResponse(['message' => 'Not found'], 404);

    
            $userName = Users::where('id',$state['updated_by'])->first();
            if($userName) {
                $state['updated_by'] =  trim($userName->first_name.' '.$userName->last_name);
            } else {
                $state['updated_by'] = '';
            }
            return new JsonResponse($state);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

    public function store(Request $request) {
        try {
            $this->validate($request,[
                'state_name'   => 'required|unique:state',
                'countries_id' => 'required|numeric'
            ]);

            $data = $request->toArray();
            $data['updated_by'] = $request->user_id;

            $state = new State($data);

            // HERE: additional, non-fillable fields

            $state->save();

            if($state)
                return new JsonResponse([$state], 200);

            return new JsonResponse(['message' => 'Server Error'], 500);
        } catch (\Illuminate\Validation\ValidationException $e) {
            return validation_exception($e);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

    public function destroy($id) {
        try {
            $state = State::find($id);

            if($state === null)
                return new JsonResponse(['message' => 'Not found'], 404);

            $state->delete();

            return new JsonResponse(['message'=>'Deleted Sucessfully'], 200);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

    public function update(Request $request, $id) {
        try {
            $state = State::find($id);

            if($state === null)
                return new JsonResponse(['message' => 'Not found'], 404);

            $data = $request->toArray();
            $data['updated_by'] = $request->user_id;
            $state->update($data);

            return new JsonResponse([],200);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }


    public function search(Request $request, $state) {
        try {
            $limit = $request->limit?$request->limit:10;
            $data = State::
            select('state.state_name','state.id','state.updated_at',DB::raw("TRIM(CONCAT_WS(' ',u.first_name,u.last_name)) as updated_by"),'countries.name as country_name','state.deleted_at')
            ->leftjoin('users as u','u.id','=','state.updated_by')
            ->leftjoin('countries','countries.id','=','state.countries_id')
            ->where('state.state_name','like',$state.'%')->withTrashed()->paginate($limit);

            if(count($data) == 0)
                return new JsonResponse(['message' => 'Not found'], 200);

            return new JsonResponse($data);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

    public function restore($id) {
        State::withTrashed()->find($id)->restore();
        return new JsonResponse([],200);
    }
}
