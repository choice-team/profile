<?php

namespace App\Http\Controllers;

use DB;
use App\Users;
use App\PartnerServices;
use Illuminate\Http\Request;
use App\EnterpreneurInterest;
use Illuminate\Http\Response;
use League\Flysystem\Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\PartnerEntrepreneurRelationship;
use App\Permissions\HasPermissionsTrait;


class MyProfileController extends Controller
{
    use HasPermissionsTrait;
    
    private $user_id;

    public function __construct()
    {
      $this->user_id = Auth::user()->id;
    }



    public function getUserId(Request $request)
    {
    	if($request->user_slug) {
            $userData = Users::where('user_slug',$request->user_slug)->first();
            if(!$userData) {
                return response()->json([
                    'data' => $data
                ],
                Response::HTTP_NOT_FOUND
                ); 
            }
            $user_id = $userData->id;
        } else {
            $user_id = $this->user_id;
        }
         return  $user_id;
            
    }

      /**
 * @api {get} api/profile/my-profile Profile Information
 * @apiName My profile
 * @apiGroup Profile
 *
 * @apiParam {string} user_slug Users unique user_slug.
   * @apiSuccessExample Success-Response-2000:
   HTTP/1.1 2000 OK
{
    "status_code": "2000",
    "message": "success",
    "body": {
        "data": {
            "connection_status": [],
            "userProfile": [
                {
                    "first_name": "geeta",
                    "last_name": "ahuja",
                    "email": "Z2VldGFhaHVqYTg1QGdtYWlsLmNvbQ==",
                    "gender": "female",
                    "birth_year": "1919",
                    "country": "India",
                    "state": "West Bengal\r\n",
                    "about": "",
                    "profile_pic": "1586753296face9686a126c31e936870d7c956b970.png",
                    "entreprise_name": null,
                    "mentorship_status": 0,
                    "other_city": "",
                    "city": "Calcutta"
                }
            ]
        }
    }
}

  * @apiErrorExample Error-Response-4001:
  *   HTTP/1.1 4001 Unauthorized
  *   {
   "status_code": 4001,
   "message": "Unauthorized Accecss",
   "body": []
   }
  *
  * @apiErrorExample Error-Response-5000:
  *   HTTP/1.1 5000 Internal Server Error
  *      {
   "status_code": 5000,
   "message": "Internal Error, Try again later",
   "body": []
   }
  */
     public function myProfile(Request $request)
    {
    	$user_id = $this->getUserId($request);
        $data = [];
        
        if(isset($this->user_id)){
            $data['connection_status'] = PartnerEntrepreneurRelationship::select('connection_status')->where(['user_id'=>$this->user_id,'connected_to'=>$user_id,'deleted_at'=>NULL])
        ->get();

        }
        $data['userProfile'] = Users::select('users.first_name','users.last_name','users.email','user_profile.gender','user_profile.birth_year','user_profile.country','user_profile.state','user_profile.about','user_profile.profile_pic','user_profile.entreprise_name','users.mentorship_status','user_profile.other_city',DB::raw('
        CASE
            WHEN user_profile.state>0
            THEN state.state_name
            ELSE user_profile.state
        END AS state'),DB::raw('
        CASE
            WHEN user_profile.city>0
            THEN city.city_name
            ELSE user_profile.city
        END AS city'))
        ->leftjoin('user_profile','user_profile.user_id','=','users.id')
        ->leftjoin('state','state.id','=','user_profile.state')
        ->leftjoin('city','city.id','=','user_profile.city')
        ->where(['users.id'=>$user_id])
        ->get()->transform(function($item,$key){
                $item->email = base64_encode($item->email);
                return $item;
        });
        return response()->json([
              'data' => $data
            ]);
    }

     /**
 * @api {get} api/profile/suggested-mentor Suggested Mentor
 * @apiName Suggested Mentor
 * @apiGroup Profile
 *
 * @apiParam {string} user_slug Users unique user_slug.
   * @apiSuccessExample Success-Response-2000:
   HTTP/1.1 2000 OK
{
    "status_code": "2000",
    "message": "success",
    "body": {
        "data": {
            "suggested_mentors": [
                {
                    "slug": "ganesh-1",
                    "first_name": "Ganesh",
                    "last_name": "",
                    "profile_pic": "1544691975fa841d1d24b136e619893f43ce11c2a3.jpg"
                },
                {
                    "slug": "geeta-ahuja-2",
                    "first_name": "Geeta",
                    "last_name": "Ahuja",
                    "profile_pic": null
                }
            ]
        }
    }
}

  * @apiErrorExample Error-Response-4001:
  *   HTTP/1.1 4001 Unauthorized
  *   {
   "status_code": 4001,
   "message": "Unauthorized Accecss",
   "body": []
   }
  *
  * @apiErrorExample Error-Response-5000:
  *   HTTP/1.1 5000 Internal Server Error
  *      {
   "status_code": 5000,
   "message": "Internal Error, Try again later",
   "body": []
   }
  */
    public function suggestedMentor(Request $request)
    {
    	$user_id = $this->getUserId($request);

    	$data['suggested_mentors'] = EnterpreneurInterest::select(DB::Raw('DISTINCT(users.user_slug) as slug'),'users.first_name','users.last_name','user_profile.profile_pic','user_profile.gender')
        ->leftjoin('users','users.id','=','entrepreneur_interest_area.user_id')
        ->leftjoin('user_profile','user_profile.user_id','=','users.id')
        ->whereIn('interest_area_id', function($query) use ($user_id)
        {

        $query->select(\DB::raw('interest_area_id'))
              ->from('entrepreneur_interest_area')
              ->whereRaw('entrepreneur_interest_area.deleted_at is null')
              ->whereRaw('user_id ='.$user_id);
        })
        ->where('users.mentorship_status','=','1')
        ->where('users.id','!=',$user_id)
        ->whereNotIn('users.id',function($query) use ($user_id){
           $query->select(\DB::raw('connected_to'))
                   ->from('user_mentor_partner_connection_mapper')
                   ->where('user_id',$user_id)
                   ->where('connection_status',1)
                   ->where('deleted_at',null);
       })
        ->get();
        return response()->json([
              'data' => $data
         ]);
    }

        /**
 * @api {get} api/profile/connected-partners Connected Partners
 * @apiName Connected Partners
 * @apiGroup Profile
 *
 * @apiParam {string} user_slug Users unique user_slug.
   * @apiSuccessExample Success-Response-2000:
   HTTP/1.1 2000 OK
{{
    "status_code": "2000",
    "message": "success",
    "body": {
        "data": {
            "connected_partners": [
                {
                    "first_name": "CRISIL Limited",
                    "logo": "157674671116db163683672142f73bb4865145d842.JPG",
                    "slug": "crisil-limited"
                },
                {
                    "first_name": "Access Livelihoods Consulting India",
                    "logo": "partner-alc-india.png",
                    "slug": "alc-india"
                }
            ]
        }
    }
}

  * @apiErrorExample Error-Response-4001:
  *   HTTP/1.1 4001 Unauthorized
  *   {
   "status_code": 4001,
   "message": "Unauthorized Accecss",
   "body": []
   }
  *
  * @apiErrorExample Error-Response-5000:
  *   HTTP/1.1 5000 Internal Server Error
  *      {
   "status_code": 5000,
   "message": "Internal Error, Try again later",
   "body": []
   }
  */
    public function connectedPartners(Request $request)
    {
    	$user_id = $this->getUserId($request);
    	 $data['connected_partners'] = Users::select('users.first_name','partner_profile.logo','partner_profile.slug')
        // ->leftjoin('user_profile','user_profile.user_id','=','users.id')
        ->leftjoin('partner_profile','partner_profile.user_id','=','users.id')
        ->leftjoin('user_mentor_partner_connection_mapper','user_mentor_partner_connection_mapper.connected_to','=','users.id')
        ->where(['user_mentor_partner_connection_mapper.user_id'=>$user_id,'users.user_type'=>'partner'])
        ->where('user_mentor_partner_connection_mapper.connection_status', 1)
        ->get();
        
    	
        return response()->json([
              'data' => $data
         ]);
    }
    /**
 * @api {get} api/profile/connected-members Connected Members
 * @apiName Connected Members
 * @apiGroup Profile
 *
 * @apiParam {string} user_slug Users unique user_slug.
   * @apiSuccessExample Success-Response-2000:
   HTTP/1.1 2000 OK
{
    "status_code": "2000",
    "message": "success",
    "body": {
        "data": {
            "connected_members": [
                {
                    "first_name": "Yogesh",
                    "last_name": "Jadhav",
                    "profile_pic": null,
                    "slug": "yogesh-jadhav"
                },
                {
                    "first_name": "Tester",
                    "last_name": "Choice",
                    "profile_pic": "1546861693fa841d1d24b136e619893f43ce11c2a3.jpg",
                    "slug": "tester-choice"
                }
            ]
        }
    }
}

  * @apiErrorExample Error-Response-4001:
  *   HTTP/1.1 4001 Unauthorized
  *   {
   "status_code": 4001,
   "message": "Unauthorized Accecss",
   "body": []
   }
  *
  * @apiErrorExample Error-Response-5000:
  *   HTTP/1.1 5000 Internal Server Error
  *      {
   "status_code": 5000,
   "message": "Internal Error, Try again later",
   "body": []
   }
  */
    public function connectedMembers (Request $request)
    {
    	  $user_id = $this->getUserId($request);

    	$data['connected_members'] = Users::select('users.first_name','users.last_name','user_profile.profile_pic','users.user_slug as slug','user_profile.gender')
        ->leftjoin('user_profile','user_profile.user_id','=','users.id')
        ->join('user_mentor_partner_connection_mapper','user_mentor_partner_connection_mapper.connected_to','=','users.id')
        ->where(['user_mentor_partner_connection_mapper.user_id'=>$user_id,'users.user_type'=>'entrepreneur'])
        ->where('user_mentor_partner_connection_mapper.connection_status', 1)
        ->get();

        return response()->json([
              'data' => $data
         ]);
    }

     /**
 * @api {get} api/profile/suggested-partners Suggested Partners
 * @apiName Suggested Partners
 * @apiGroup Profile
 *
 * @apiParam {string} user_slug Users unique user_slug.
   * @apiSuccessExample Success-Response-2000:
   HTTP/1.1 2000 OK
	{
    "status_code": "2000",
    "message": "success",
    "body": {
        "data": {
            "suggested_partners": [
                {
                    "slug": "crisil-limited",
                    "first_name": "CRISIL Limited",
                    "last_name": "",
                    "logo": "157674671116db163683672142f73bb4865145d842.JPG"
                },
                {
                    "slug": "alc-india",
                    "first_name": "Access Livelihoods Consulting India",
                    "last_name": "",
                    "logo": "partner-alc-india.png"
                }
            ]
        }
    }
}

  * @apiErrorExample Error-Response-4001:
  *   HTTP/1.1 4001 Unauthorized
  *   {
   "status_code": 4001,
   "message": "Unauthorized Accecss",
   "body": []
   }
  *
  * @apiErrorExample Error-Response-5000:
  *   HTTP/1.1 5000 Internal Server Error
  *      {
   "status_code": 5000,
   "message": "Internal Error, Try again later",
   "body": []
   }
  */
    public function suggestedPartners(Request $request)
    {
    	$user_id = $this->getUserId($request);

        $data['suggested_partners'] = PartnerServices::select(DB::raw('DISTINCT(partner_profile.slug) as slug'),'users.first_name','users.last_name','partner_profile.logo','user_profile.gender')
        ->leftjoin('users','users.id','=','partner_services.user_id')
        ->leftjoin('user_profile','user_profile.user_id','=','users.id')
        ->leftjoin('partner_profile','partner_profile.user_id','=','users.id')
        ->leftjoin('user_mentor_partner_connection_mapper','user_mentor_partner_connection_mapper.connected_to','=','users.id')
        ->join('service_category_mapper','service_category_mapper.service_id','=','partner_services.id')
        ->whereIn('service_category_mapper.category_id', function($query) use ($user_id)
        {
        $query->select(\DB::raw('interest_area_id'))
              ->from('entrepreneur_interest_area')
              ->whereRaw('entrepreneur_interest_area.deleted_at is null')
              ->whereRaw('user_id ='.$user_id);
        })
        ->whereNotIn('users.id',function($query) use ($user_id){
           $query->select(\DB::raw('connected_to'))
                   ->from('user_mentor_partner_connection_mapper')
                   ->where('user_id',$user_id)
                   ->where('connection_status',1)
                   ->where('deleted_at',null);
       })
        ->where('user_mentor_partner_connection_mapper.connection_status', "0")
        ->get();
    	
        return response()->json([
              'data' => $data
         ]);
    }
    

}