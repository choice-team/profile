<?php
namespace App\Http\Controllers;

use App\Users;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Spatie\Activitylog\Models\Activity;

class ActivityController extends Controller
{


    /**
     * @api {get} api/profile/activity-log Activity log user
     * @apiName index
     * @apiGroup admin
     *
     * @apiParam {Integer} id  of user
     *
     * @apiSuccess {String} Success
     */
    public function index(Request $request) {
        if($request->has('limit')) {
            $limit = $request->get('limit');
        }else{
              $limit = 10;
        }
        if($request->has('page')) {
            $page = $request->get('page');
        }else{
              $page = 0;
        }
        $query = Activity::select('activitylog.log_name','activitylog.description','activitylog.subject_id','activitylog.subject_type','activitylog.causer_type','activitylog.properties','activitylog.created_at','activitylog.updated_at','users.first_name','users.last_name','users.user_slug')
        ->leftjoin('users','activitylog.causer_id','=','users.id')
        ->orderBy('updated_at','DESC');
       
        $data['count'] = $query->count();
        $data['data'] = $query
        ->skip($page*$limit)
        ->limit($limit)
        ->get();
        return $data;
    }

      /**
     * @api {get} api/profile/activity-log/{slug} Activity log user
     * @apiName show
     * @apiGroup admin
     *
     * @apiParam {String} slug  of user
     * 
     * @apiSuccess {String} Success
     */
    public function show(Request $request, $id) {
        return Activity::select('activitylog.log_name','activitylog.description','activitylog.subject_id','activitylog.subject_type','activitylog.causer_type','activitylog.properties','activitylog.created_at','activitylog.updated_at','users.first_name','users.last_name')
        ->leftjoin('users','activitylog.causer_id','=','users.id')
        ->where('activitylog.id',$id)
        ->first();
    }
}
?>
