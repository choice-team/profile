<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Role;
use App\Notification;
use App\Http\Requests;
use App\Models\Module;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{

    private $user_id;

    public function __construct() {
        $this->user_id = Auth::user()->id;
    }

    public function index(Request $request) {
        $limit = $request->limit?$request->limit:10;
        $data['count'] = Notification::count();
        $data['notification'] = Notification::orderBy('created_at','DESC')->paginate($limit);
        return $data;
    }

    public function update(Request $request, $id) {
        $notification =  Notification::find($id);
        $user = Auth::user();
        $notification->users()->detach($user);
        return new JsonResponse([],200);
    }
}
?>