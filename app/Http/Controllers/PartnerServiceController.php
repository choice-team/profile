<?php

namespace App\Http\Controllers;

use App\User;
use App\Users;
use Exception;
use App\Category;
use App\Http\Requests;
use App\PartnerProfile;
use App\PartnerServices;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Validators\MathCaptcha;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use League\Flysystem\FilesystemInterface;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use App\Models\ApprovalThreads;
use Illuminate\Support\Facades\Hash;

// use Illuminate\Http\Exceptions\HttpResponseException;

class PartnerServiceController extends Controller
{
    use MathCaptcha;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $partner_services = $this->getServiceList($request); 

        return view('admin.partner_services.index', compact('partner_services'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function indexApi(Request $request)
    {

        try {
 
            $record = $this->getServiceList($request); 

            return response()->json([
            'status_code' => 2000,
            'message' => 'Success',
            'body' => [
              // 'total_count' => $perPage,
              'data_count' => count($record),
              'data' => $record
            ]
          ],
            Response::HTTP_OK
          );

        } catch (Exception $e) {
            echo $this->exceptionResponse($e);
        }
        
    }

    public function getServiceList($request,$user_id)
    {

        $limit = $request->input('limit');
        $offset = $request->input('offset');
        $perPage =  10;
        $keyword = $request->get('search');


        if (!empty($keyword)) {

            $partner_services = PartnerServices::select('partner_services.service_desc','partner_services.service_title','users.first_name as name')
            ->leftjoin('users','users.id','=','partner_services.user_id')
            ->where('partner_services.user_id','=',$user_id)
            ->where('users.first_name', 'LIKE', "%$keyword%")
            ->orWhere('service_desc', 'LIKE', "%$keyword%")
            ->orWhere('service_title', 'LIKE', "%$keyword%")
            ->orWhere('description', 'LIKE', "%$keyword%")
            ->latest()->paginate($perPage);

        } else {
            
            $partner_services = PartnerServices::select('partner_services.service_desc','partner_services.service_title','users.first_name as name')
            ->join('users','users.id','=','partner_services.user_id')
            ->where('partner_services.user_id','=',$user_id)
            ->orderBy('partner_services.created_at','desc')
            ->paginate($perPage);

        }

        return $partner_services;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {  
        $users = User::select('id','first_name as name')
            ->orderBy('id','desc')
            ->get();

        return view('admin.partner_services.create', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'service_desc' => 'required|string|unique:partner_services,service_desc,NULL,id,deleted_at,NULL',
            'service_title' => 'required|string|unique:partner_services,service_title,NULL,id,deleted_at,NULL'
        ]);

        try {

            $result = $this->storeService($request); 

            if ($result) {
                return redirect('admin/partner_services')->with('flash_message', 'partner_service added!');
            } else {
                return redirect('admin/partner_services')->with('danger', 'something went wrong while adding data!');
            }

        } catch (Exception $e) {

            echo $this->exceptionResponse($e);
        }
        
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function storeApi(Request $request)
    {
        try {
            
            $result = $this->storeService($request);  

            return response()->json([
            'status_code' => 2000,
            'message' => 'Success',
            'body' => [
             
            ]
          ],
            Response::HTTP_OK
          );

        } catch (\Illuminate\Validation\ValidationException $exception) {
            // $error = $this->getErrorString($e);
            $error = $exception->getMessage();
            // Log::error($error);
            return response()->json([
                'status_code' => 4000,
                'message' => $error,
                'body' => ['id' => null]
            ],
                Response::HTTP_BAD_REQUEST
            );

        } catch (Exception $e) {

            echo $this->exceptionResponse($e);
        }
        
    }

    public function storeService(Request $request)
    {
        $requestData['service_title'] = $request->service_title;
        $requestData['service_desc'] = $request->service_desc; 
        
        $requestData['user_id'] = PartnerProfile::where('slug',$request->slug)->first()->user_id;

        $partner = PartnerServices::create($requestData);
        $categories = Category::find($request->category);
        $partner->category()->sync($categories);
        return new JsonResponse([],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $partner_service = $this->getServiceListById($id); 
        
        return view('admin.partner_services.show', compact('partner_service'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function showApi($id)
    {
        try {
 
            $record = $this->getServiceListById($id); 

            return response()->json([
            'status_code' => 2000,
            'message' => 'Success',
            'body' => [
                $record
            ]
          ],
            Response::HTTP_OK
          );

        } catch (Exception $e) {
            echo $this->exceptionResponse($e);
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $users = User::select('id','first_name as name')
            ->orderBy('id','desc')
            ->get();
        $partner_service = $this->getServiceListById($id); 

        return view('admin.partner_services.edit', compact('partner_service','users'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function editApi($id)
    {

        try {
 
            $record = $this->getServiceListById($id); 

            return response()->json([
            'status_code' => 2000,
            'message' => 'Success',
            'body' => [
                $record
            ]
          ],
            Response::HTTP_OK
          );

        } catch (Exception $e) {
            echo $this->exceptionResponse($e);
        }
        
    }

    public function getServiceListById($id)
    {

        $partner_services = PartnerServices::select('partner_services.*','users.first_name as name')
            ->join('users','users.id','=','partner_services.user_id')   
            ->orderBy('partner_services.created_at','desc')
            ->where('partner_services.id','=',$id)
            ->first();

        // $partner_service = partner_service::findOrFail($id);

        return $partner_services;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'service_desc' => 'required|string|unique:partner_services,service_desc,'.$id.',id,deleted_at,NULL',
			'service_title' => 'required|string|unique:partner_services,service_title,'.$id.',id,deleted_at,NULL'
		]); 

        $result = $this->updateService($request,$id);  

        if ($result) {
            return redirect('admin/partner_services')->with('flash_message', 'partner_service updated!');
        } else {
            return redirect('admin/partner_services')->with('danger', 'something went wrong while updating data!');
        }

        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateApi(Request $request,$id)
    {

        try {
          
            $result = $this->updateService($request,$id);  

            return response()->json([
            'status_code' => 2000,
            'message' => 'Success',
            'body' => [
             
            ]
          ],
            Response::HTTP_OK
          );

        } catch (\Illuminate\Validation\ValidationException $e) {

            // $error = $this->getErrorString($e);
            $error = $e->getMessage();
            // Log::error($error);

            return response()->json([
                'status_code' => 4000,
                'message' => $error,
                'body' => ['id' => null]
            ],
                Response::HTTP_BAD_REQUEST  
            );

        } catch (Exception $e) {

            echo $this->exceptionResponse($e);
        }
        
    }

    public function updateService($request,$id)
    {
        $requestData = $request->all();
        $partner_service = PartnerServices::findOrFail($id);
        unset($requestData['user_id']);
        $partner_service->update($requestData);
        $category = Category::find($request->category);
        $partner_service->category()->sync($category);
        return $partner_service;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $result = $this->destroyService($id); 

        if ($result) {
            return redirect('admin/partner_services')->with('flash_message', 'partner_service deleted!');
        } else {
            return redirect('admin/partner_services')->with('danger', 'something went wrong while deleting data!');
        }

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroyApi($id)
    {
        try {

            $result = $this->destroyService($id);  

            return response()->json([
            'status_code' => 2000,
            'message' => 'Success',
            'body' => []
          ],
            Response::HTTP_OK
          );

        } catch (\Illuminate\Validation\ValidationException $e) {

            // $error = $this->getErrorString($e);
            $error = $e->getMessage();
            // Log::error($error);

            return response()->json([
                'status_code' => 4000,
                'message' => $error,
                'body' => []
            ],
                Response::HTTP_BAD_REQUEST
            );

        } catch (Exception $e) {

            echo $this->exceptionResponse($e);
        }
    }

    public function destroyService($id)
    {
       return  PartnerServices::destroy($id);
    }

    public function getErrorString($e) {

        $errors = $e->getResponse()->getOriginalContent();

        // return response()->json($e->getResponse()->getOriginalContent(), \Illuminate\Http\Response::HTTP_UNPROCESSABLE_ENTITY);

        $error = array();

            foreach ($errors as $message) {
                array_push($error, $message[0]);
            }

        return implode(',', $error);
    }

    public function exceptionResponse($e) {

        return response()->json([
          'status_code' => 5000,
          'message' => 'Internal Error, Try again later',
          'dev_message' => $e->getMessage(),
          'body' => []
        ],
            Response::HTTP_INTERNAL_SERVER_ERROR
        );
    }

     /*
  * @api {GET} api/profile/partner Get Partner List
  * @apiName Get Partner List
  * @apiGroup Partner
  * @apiDescription This api is used to get all list of Partner registred in the WEP Website.
  * @apiHeader {String} Authorization api token key.
  * @apiSuccessExample Success-Response-2000:
   HTTP/1.1 2000 OK
     {
       "status_code": 2000,
       "message": "Success",
       "body": {
         "total_count": 20,
         "count": 20,
         "data": [
           {

           .
           .
           .},
           {

           }
         ]
       }
   }

  * @apiErrorExample Error-Response-4001:
  *   HTTP/1.1 4001 Unauthorized
  *   {
   "status_code": 4001,
   "message": "Unauthorized Accecss",
   "body": []
   }
  *
  * @apiErrorExample Error-Response-5000:
  *   HTTP/1.1 5000 Internal Server Error
  *      {
   "status_code": 5000,
   "message": "Internal Error, Try again later",
   "body": []
   }
  */
  public function partnerList(Request $request){
    $data = [];
    $totalCount = 0;
    /*Removed the unncessary columns */
    $data = PartnerProfile::select('users.first_name as partner_name','partner_profile.slug', 'partner_profile.logo','partner_profile.company_description','user_profile.gender')
    
    ->join('users','partner_profile.user_id','=','users.id')
    ->leftjoin('user_profile','user_profile.user_id','=','users.id')
    ->where(['users.user_type'=>'partner','users.approval_status'=>'approved']);


    if($request->has('keyword') && !empty($request->keyword)){
        $data = $data->where(function($data) use ($request) {
        $data->orWhere('users.first_name', 'LIKE', "%$request->keyword%");
        $data->orWhere('users.last_name', 'LIKE', "%$request->keyword%");
        $data->orWhere('users.mobile', 'LIKE', "%$request->keyword%");
        $data->orWhere('users.email', 'LIKE', "%$request->keyword%");
        $data->orWhere('users.user_code', 'LIKE', "%$request->keyword%");
        });
    }
    $totalCount = $data->count();
    $data = $data->orderBy('partner_name')
        ->get();

    return response()->json([
        'total_count' => $totalCount,
        'data_count' => count($data),
        'data' => $data
    ]);
}

 /*
  * @api {GET} api/profile/enterprenaur-count Get Partner List
  * @apiName Get Enterprenaur count
  * @apiGroup Partner
  * @apiDescription This api is used to get all count of enterprenaur registred in the WEP Website.
  * @apiSuccessExample Success-Response-2000:
   HTTP/1.1 2000 OK
     {
       "status_code": 2000,
       "message": "Success",
       "body": {
         "total_count": 20
       }
   }

  *
  * @apiErrorExample Error-Response-5000:
  *   HTTP/1.1 5000 Internal Server Error
  *      {
   "status_code": 5000,
   "message": "Internal Error, Try again later",
   "body": []
   }
  */
public function entrepreneurCount(){

    $data['count']=Users::where('user_type','entrepreneur')->count();
    return response()->json([
          'data' => $data
        ]);
}

 /*
  * @api {GET} api/profile/partner/{slug}- Get Partner detail
  * @apiName singlePartnerProfile Get Partner Detail
  * @apiGroup Partner
  * @apiDescription This api is used to get detail of partner registred in the WEP Website.
  * @apiSuccessExample Success-Response-2000:
   HTTP/1.1 2000 OK
     {
       "status_code": 2000,
       "message": "Success",
       "body": {
         "total_count": 20
       }
   }

  *
  * @apiErrorExample Error-Response-5000:
  *   HTTP/1.1 5000 Internal Server Error
  *      {
   "status_code": 5000,
   "message": "Internal Error, Try again later",
   "body": []
   }
  */
public function singlePreLoginPartnerProfile(Request $request,$slug){
    
    try {
        $user_id = PartnerProfile::where('slug',$slug)->first();
        if($user_id){
            $user_id = $user_id->user_id; 
        }else{
            $user_id = NULL;
        }
        $user_exists = Users::where('deleted_at',NULL)->where('users.id',$user_id)->first();
        if($user_exists==NULL){
            return new JsonResponse([],404);
         }
 
        if($user_id!=NULL){
        $select = [
            'users.first_name as partner_name',
            'partner_profile.slug',
            'partner_profile.office_address_1',
            'partner_profile.office_address_2',
            'partner_profile.city',
            'partner_profile.state',
            'partner_profile.country',
            'partner_profile.postal_code',
            'partner_profile.logo',
            'partner_profile.website_url',
            'partner_profile.linkedin_url',
            'partner_profile.facebook_url',
            'partner_profile.area_of_business',
            'partner_profile.type_of_establishment',
            'partner_profile.company_description',
            'partner_profile.cxo_name',
            'partner_profile.cxo_qualification',
            'partner_profile.cxo_designation',
            'partner_profile.cxo_contact',
            'partner_profile.cxo_linkedin_url',
            'partner_profile.contact_person_name',
            'partner_profile.collaboration_model_description',
            'partner_profile.proposed_deliverables_description',
            'partner_profile.proposed_deliverables_benefit',
            'partner_profile.expectations',
            'partner_profile.about_wep_description',
            'partner_profile.created_by',
            'partner_profile.updated_by',
        ];

        $data['userProfile'] = PartnerProfile::select($select)
        ->join('users','partner_profile.user_id','=','users.id')
        ->where(['partner_profile.user_id'=>$user_id,'users.deleted_at'=>NULL])
        ->get();
      
        $data['partner_services'] = $this->getServiceList($request,$user_id);
        return response()->json([
            'data' => $data
        ],
            Response::HTTP_OK
        );
        }
        } catch (Exception $e) {
            return response()->json(['msg'=>$this->exceptionResponse($e)]);
        }

    }

    public function storeParterApi(Request $request)
    {
       // try {

            //print_r($request->all());exit;
	/*
            $messages = [
                'contact_person_mobile.unique_mobile' => 'Mobile number is already exist.',
                'contact_person_email.unique_email' => 'Email address is already exist.',
                'recaptcha' => 'captcha validation failed'
            ];

            $this->validate($request, [
                'name' => 'required',
                'contact_person_mobile' => 'required|min:12|max:13|unique_mobile:contact_person_mobile',
                'contact_person_email' => 'required|email|unique_email:contact_person_email',
                'recaptcha.id' => 'required',
                'recaptcha.ans' => 'required'
            ], $messages);

            if(!$this->postMc($request->recaptcha)) {
              
                return response()->json([
                   
                ],
                    Response::HTTP_BAD_REQUEST

                ); 
            }
            $userData = [
                'first_name' => $request->name,
                'mobile' => $request->contact_person_mobile,
                'email' => $request->contact_person_email,
                'user_type' => 'partner'
            ];
		*/
 $messages = [
                'contact_person_mobile.unique_mobile' => 'Mobile number already exists.',
                'contact_person_email.unique_email' => 'Email id already exists.',
                'recaptcha' => 'captcha validation failed'
            ];

         $request['contact_person_mobile']=$request->contact_person_mobile?base64_decode($request->contact_person_mobile):'';
            $request['contact_person_email']=$request->contact_person_email?base64_decode($request->contact_person_email):'';

            $this->validate($request, [
                'name' => 'required',
                'contact_person_mobile' => 'required|min:12|max:13|unique_mobile:contact_person_mobile',
                'contact_person_email' => 'required|email|unique_email:contact_person_email',
                'recaptcha.id' => 'required',
                'recaptcha.ans' => 'required'
            ], $messages);

            if(!$this->postMc($request->recaptcha)) {

                return response()->json([

                ],
                    Response::HTTP_BAD_REQUEST

                );
            }
            $userData = [
                'first_name' => $request->name,
                'mobile' => $request->contact_person_mobile,
                'email' => $request->contact_person_email,
                'user_type' => 'partner'
            ];

            $result = DB::transaction(function () use ($userData, $request) {

                $mobileUser = Users::where('mobile', $request->contact_person_mobile)->count();
                $emailUser = Users::where('email', $request->contact_person_email)->count();

                if($mobileUser > 0){

                    $result = Users::updateOrCreate([
                        'mobile' => $request->contact_person_mobile,
                    ], $userData);
                }
                else if($emailUser > 0){

                    $result = Users::updateOrCreate([
                        'email' => $request->contact_person_email,
                    ], $userData);
                }
                else{

                    $result = Users::updateOrCreate([
                        'mobile' => $request->contact_person_mobile,
                        'email' => $request->contact_person_email,
                    ], $userData);

                    //try {
                          $subject = "Receipt of Partner Registration EOI Form";
                    Mail::send('Mail.partner_eoi', [
                              'name' => ucwords($request->name),
                            
                          ], function($message) use($subject,$request){
                              $message->to($request->contact_person_email)->subject($subject);
                    });
                      // } catch(Exception $e) {
                      //   Log::info($e->getMessage());
                      // }
                }

                Users::where('id', $result->id)->update([

                    'user_code' => "WEPPTR".sprintf('%010d', $result->id)
                ]);

                return $result;                
            });


            if (isset($result->id) && !empty($result->id)) {
                $request->slug = $result->user_slug;
                return $this->storeUpdateParter($request, $result->id);
            }
                       

            return response()->json([],Response::HTTP_INTERNAL_SERVER_ERROR);
                       

                   

        // } catch (\Illuminate\Validation\ValidationException $e) {

        //     return validation_exception($e);

        // } catch (\Exception $e) {

        //     return general_expection($e);
        // }
    }


    public function storeUpdateParter(Request $request, $userId)
    {
        $profileData = [
            'user_id' => $userId,
            'office_address_1' => $request->input('office_address_1'),
            'office_address_2' => $request->input('office_address_2'),
            'city' => $request->input('city'),
            'state' => $request->input('state'),
            'country' => $request->input('country'),
            'postal_code' => $request->input('postal_code'),
            'slug' => strtolower(str_replace(' ', '-', $request->name)),
            'website_url' => $request->input('website_url'),
            'linkedin_url' => $request->input('linkedin_url'),
            'facebook_url' => $request->input('facebook_url'),
            'area_of_business' => $request->input('area_of_business'),
            'type_of_establishment' => $request->input('type_of_establishment'),
            'company_description' => $request->input('company_description'),
            'cxo_name' => $request->input('cxo_name'),
            'cxo_qualification' => $request->input('cxo_qualification'),
            'cxo_designation' => $request->input('cxo_designation'),
            'cxo_contact' => $request->input('cxo_contact'),
            'cxo_linkedin_url' => $request->input('cxo_linkedin_url'),
            'contact_person_name' => $request->input('contact_person_name'),
            'collaboration_model_description' => $request->input('collaboration_model_description'),
            'proposed_deliverables_description' => $request->input('proposed_deliverables_description'),
            'proposed_deliverables_benefit' => $request->input('proposed_deliverables_benefit'),
            'expectations' => $request->input('expectations'),
            'about_wep_description' => $request->input('about_wep_description'),
            'slug'=> $request->slug
        ];

        if(!empty($request->file('logo'))){

            $uploadPath = 'uploads/profile_pics';

            if(!File::exists($uploadPath)) {

                File::makeDirectory($uploadPath, 0775, true, true);
            }

            $file = $request->file('logo');
            $fileName = str_replace(" ", "_", time() . '_' . $file->getClientOriginalName());

            if ($file->move($uploadPath, $fileName)) {

                $profileData['logo'] = $fileName;
            }
        }

        $result = PartnerProfile::updateOrCreate(['user_id' => $userId], $profileData);

        if (isset($result->id) && !empty($result->id)) {

            return response()->json([
                ],Response::HTTP_OK
            );
        }

        return response()->json([],Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function updateParterApi(Request $request, $slug, FilesystemInterface $filesystem)
    {
    	
        try {

            $query = PartnerProfile::where('slug', $slug)->first();
            $user = Users::where('id',$query->user_id)->first();
            if($request->title) {
                $user->first_name = $request->title;
                $user->save();
            }
            $data = $request->all();
            unset($data['user_id']);
            $data['slug'] = $user->user_slug;
            if($request->hasfile('logo')) {
                $data['logo'] = store_media($request->file('logo'), $filesystem );
            } 
            PartnerProfile::updateOrCreate(['id'=>$query->id],$data);

            return new JsonResponse(['slug'=>$user->user_slug],200);


        } catch (\Illuminate\Validation\ValidationException $e) {

            return validation_exception($e);

        } catch (\Exception $e) {
           
            return general_expection($e);
        }
    }

    public function partnerPendingList(Request $request) {
        $limit = $request->limit?$request->limit:10;
        $query = Users::with(['threads'=>function($q){$q->where('type','partner');}])->select('id','first_name','user_slug','email','user_code','mobile','approval_status','deleted_at')
        ->where('user_type','partner')
        ->where('approval_status','!=','approved');
        if(isset($request->search) && $request->search) {
            return $query->where('first_name','like','%'.$request->search.'%')
            ->withTrashed()
            ->orderBy('updated_at','DESC')  
            ->paginate($limit);
        } else {
            return $query
            ->withTrashed()
            ->orderBy('updated_at','DESC')  
            ->paginate($limit);
        }   
    }

    public function partnerApproveList(Request $request) {
   
        $limit = $request->limit?$request->limit:10;
        $query = Users::select('first_name','user_slug','email','user_code','mobile','approval_status','deleted_at')
                ->where('user_type','partner')
                ->where('approval_status','=','approved'); 


        if(isset($request->search) &&  $request->search) {
            return $query->where('first_name','like','%'.$request->search.'%')
            ->withTrashed()
            ->orderBy('updated_at','DESC')  
            ->paginate($limit);

        } else if($request->sort == 'name') {
           return ($request->order==1)?$query->orderBy('first_name','DESC')->paginate($limit):$query->orderBy('first_name')->paginate($limit);
             

        } else {
            return $query
            ->withTrashed()
            ->orderBy('updated_at','DESC')  
            ->paginate($limit);
        }   
    }

    // deactivate_partner
    public function delete(Request $request, $slug) {
       $user =  Users::where(['user_type'=>'partner','user_slug'=>$slug])->first();
    
       if(!$user) {
        return new JsonResponse(['message'=>'No partner found'],400);
       }
       PartnerProfile::where('user_id',$user->id)->delete();
       PartnerServices::where('user_id',$user->id)->delete();
       $user->delete();

       try {
        $subject = "Account Deactivated by WEP Admin";
        Mail::send('Mail.partner-reject', [
                  'name' => ucwords($user->first_name . " " . $user->last_name),
                
              ], function($message) use($user,$subject){
                  $message->to($user->email)->subject($subject);
        });
      } catch(Exception $e) {
        Log::info($e->getMessage());
      }
       return new JsonResponse([],200);
    }

    public function restore(Request $request, $slug) {
  
        $user =  Users::where(['user_type'=>'partner','user_slug'=>$slug])->withTrashed()->first();
      
        PartnerProfile::where('user_id',$user->id)->withTrashed()->update(['deleted_at'=>NULL]);
        PartnerServices::where('user_id',$user->id)->withTrashed()->update(['deleted_at'=>NULL]);
        $user->update(['deleted_at'=>NULL]);
        $password = $this->generatePassword();
       $user->update(['approval_status'=>'approved','password' => Hash::make(sha1($password))]);
         try {
        $subject = "Account Reactivated by WEP Admin";
        Mail::send('Mail.partner-approve', [
                  'name' => ucwords($user->first_name . " " . $user->last_name),
                  'password'=>$password,
                  'WEPID' => $user->user_code,
                  'email' => $user->email
              ], function($message) use($user,$subject){
                  $message->to($user->email)->subject($subject);
        });
      } catch(Exception $e) {
        Log::info($e->getMessage());
      }
        return new JsonResponse([],200);
    }


    public function approvePartner(Request $request, $slug) {

       $partner =  Users::where(['user_slug'=>$slug,'user_type'=>'partner'])->first();
       if(!$partner) {
           return new JsonResponse(['message'=>'No partner found'],400);
       }
       $password = $this->generatePassword();
       $partner->update(['approval_status'=>'approved','password' => Hash::make(sha1($password))]);
       

       try {
        $subject = "Partner EOI Registration Form Approved";
        Mail::send('Mail.approval', [
                  'name' => ucwords($partner->first_name . " " . $partner->last_name),
                  'approval'=> 'approved',
                  'password'=>$password,
                  'WEPID' => $partner->user_code,
                  'email' => $partner->email
              ], function($message) use($partner,$subject){
                  $message->to($partner->email)->subject($subject);
        });
      } catch(Exception $e) {
        Log::info($e->getMessage());
      }
       return new JsonResponse([],200);
    }

    public function rejectPartner(Request $request, $slug) {
        $partner =  Users::where(['user_slug'=>$slug,'user_type'=>'partner'])->first();
        if(!$partner) {
            return new JsonResponse(['message'=>'No partner found'],400);
        }
        $thread = new ApprovalThreads();
        $thread->type = 'partner';
        $thread->user_id = Auth::user()->id;
        $thread->description = $request->description;
        $partner->update(['approval_status'=>'rejected']);
        $partner->threads()->save($thread);
        
        try {
            $subject = "Partner EOI Form Rejected";
            Mail::send('Mail.rejection', [
                      'name' => ucwords($partner->first_name . " " . $partner->last_name),
                      'approval'=> 'rejected',
                      'remark' => $request->description
                  ], function($message) use($partner,$subject){
                      $message->to($partner->email)->subject($subject);
            });
          } catch(Exception $e) {
            Log::info($e->getMessage());
          }
        return new JsonResponse([],200);
     }

     protected function generatePassword($lenght = 8)
    {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';

        $pass = []; 
        $alphaLength = strlen($alphabet) - 1; 
        
        for ($i = 0; $i < $lenght; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }

        return implode($pass);
    }

    public function partnerExport()
    {
        $partner = PartnerProfile::select('slug as name_of_enterprise','office_address_1','office_address_2',DB::raw('CASE WHEN partner_profile.state>0 THEN state.state_name ELSE partner_profile.state END AS state_name'),DB::raw('CASE WHEN partner_profile.city>0 THEN city.city_name ELSE partner_profile.city END AS city_name'),'country','postal_code','website_url','linkedin_url','facebook_url','area_of_business','type_of_establishment','cxo_name','cxo_qualification','cxo_designation','cxo_contact','cxo_linkedin_url','contact_person_name','users.first_name as name','users.email as contact_email','users.mobile as contact_phone','collaboration_model_description as collaboration_model_with_wep','proposed_deliverables_description as proposed_deliverables_towards_WEP','proposed_deliverables_benefit','expectations','about_wep_description','partner_profile.created_at','partner_profile.created_by','company_description')
        ->leftjoin('state','state.id','=','partner_profile.state')
        ->leftjoin('city','city.id','=','partner_profile.city')
        ->join('users','users.id','=','partner_profile.user_id')
        ->where('user_type','partner')
        ->where('approval_status','!=','approved')
        ->distinct('partner_profile.id')
        ->get()->toArray();

 
        if($partner) {
                return $partner;  
            } else {
                $data['status_code']= 200;
                $data['message']='Data Not Found';
                $data['data']= $partner;
            } 
    }

}
