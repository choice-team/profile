<?php
namespace App\Http\Controllers;

use App\City;
use App\State;
use App\Users;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;


class CityController extends Controller
{
    
	public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index']]);
    }

    public function index(Request $request,$id) {
        try {
            $city = City::select('id','city_name')->where('state_id',$id)->get();
            return new JsonResponse($city);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

    public function list(Request $request) {
        try {
            $limit = $request->limit? $request->limit:10;
            $city = new City();

            $city = City::select('city.id','city.city_name','state.id as state_id','state.state_name','city.updated_at',DB::raw("TRIM(CONCAT_WS(' ',u.first_name,u.last_name)) as updated_by"),'city.deleted_at')
            ->leftjoin('users as u','u.id','=','city.updated_by')
            ->leftjoin('state','state.id','=','city.state_id');

            if($request->sort == 'name') {
                $data = ($request->order == 1)? $city->orderBy('city_name') : $city->orderBy('city_name','DESC');
            } else if($request->sort == 'date') {
                $data = ($request->order == 1) ? $city->orderBy('updated_at') : $city->orderBy('updated_at','DESC');   
            } else {
                $data = $city->orderBy('updated_at','DESC');
            }
            if(isset($request->keyword) && !empty($request->keyword))
            {
                $data = $city->where('city.city_name','like',$request->keyword.'%');
            }
            $data = isset($request->flag)?$data->withTrashed()->get():$data->withTrashed()->paginate($limit);
            
            if($data->count()==0) {
                return new JsonResponse(['message' => 'Not found'], 404);
            } 
            
            return new JsonResponse($data);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

    public function store(Request $request) {
         try {
            $this->validate($request,[
                'city_name'   => 'required|unique:city',
                'state_id' => 'required'
            ]);
           
            if($request->state_id > 0) {
              
                $data['updated_by'] = $request->user_id;
                $data['city_name'] = $request->city_name;
                $data['state_id']= $request->state_id; 
            } else {
                $getstate = State::where('state_name','=',$request->state_id)->get()->toArray();
                if(isset($getstate) && !empty($getstate)) { 
                    $data['updated_by'] = $request->user_id;
                    $data['city_name'] = $request->city_name;
                    $data['state_id']= $getstate[0]['id'];  
                }
            }
            $city = new City($data);

            $city->save();

            if($city)
                return new JsonResponse([$city], 200);

            return new JsonResponse(['message' => 'Server Error'], 500);
        } catch (\Illuminate\Validation\ValidationException $e) {
            return validation_exception($e);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

    public function show($id) {
        try {
            $city = City::withTrashed()->find($id);

            if($city === null)
                return new JsonResponse(['message' => 'Not found'], 404);

    
            $userName = Users::where('id',$city['updated_by'])->first();
            if($userName) {
                $city['updated_by'] =  trim($userName->first_name.' '.$userName->last_name);
            } else {
                $city['updated_by'] = '';
            }
            return new JsonResponse($city);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

    public function destroy($id) {
        try {
            $city = City::find($id);

            if($city === null)
                return new JsonResponse(['message' => 'Not found'], 404);

            $city->delete();

            return new JsonResponse(['message'=>'Deleted Sucessfully'], 200);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

    public function update(Request $request, $id) {
         
        try {
            $city = City::find($id);

            if($city === null)
                return new JsonResponse(['message' => 'Not found'], 404);

            $data = $request->toArray();
            $data['updated_by'] = $request->user_id;
           
            $city->update($data);

            return new JsonResponse([$city],200);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

      public function restore($id) {
        City::withTrashed()->find($id)->restore();
        return new JsonResponse([],200);
    }


}
