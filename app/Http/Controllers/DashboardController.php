<?php

namespace App\Http\Controllers;

use DB;
use App\Menu;
use App\Users;
use App\Models\Events;
use App\Models\ForumQuestions;
use App\Models\Blog;
use App\category;
use App\Language;
use App\Countries;
use App\HomePageContent;
use Illuminate\Http\Request;
use Illuminate\Http\Response;


class DashboardController extends Controller
{

  public function userCount() {
    return DB::select(DB::raw("SELECT id, mentorship_status,created_at FROM `users` where mentorship_status is  NULL AND users.deleted_at is null"));
  }

  public function membershipCount() {
  
    return DB::select(DB::raw("SELECT id, mentorship_status,created_at FROM `users` where mentorship_status = 1 AND  users.deleted_at is null"));
  }

  public function schemwiseBlog() {
   
    return DB::select(DB::raw("SELECT distinct t1.blog_title,t1.created_at,t1.id from blog_posts as t1
      where t1.deleted_at is null and t1.blog_status='Published'"));
  }

  public function groupwiseBlog() {
    return DB::select(DB::raw("SELECT distinct blog_title,t1.id,sum(like_status),blog_taxonomy_id,t3.taxonomy_name FROM `blog_posts` as t1
      join `user_blog_like_mapper` as t2
      on t1.id = t2.blog_id 
      left join `taxonomies` as t3 
      on t1.blog_taxonomy_id = t3.id
      where like_status != 0
     and blog_posts.deleted_at is null and user_blog_like_mapper.deleted_at is null and taxonomies.deleted_at is null 
      group by blog_title
      ORDER BY NULL 
      "));
  }

  public function forum() {
    return DB::select(DB::raw("SELECT count(thread_answer),forum_questions.id,forum_questions.forum_question,categories.category_name,sum(forum_thread.likes)
      from forum_thread
      join forum_questions
      on forum_questions.id = forum_thread.question_id
      join categories on categories.id = forum_questions.topic_id
      where forum_questions.deleted_at is null and forum_thread.deleted_at is null and categories.deleted_at is null and forum_questions.status='Approved'
      group by question_id"));
  }

  public function topUserAnswerCount() {
   
    return DB::select(DB::raw("SELECT first_name,last_name,count(question_id)  FROM `users` join forum_thread on users.id = forum_thread.user_id where forum_thread.deleted_at is null and users.deleted_at is null group by  users.id order by NULL"));
  }

  public function postCount() {
    return DB::select(DB::raw("SELECT count(id) as count FROM `forum_questions` where deleted_at is null  and forum_questions.status='Approved'"));
  }

  public function postAnsCount() {
    return DB::select(DB::raw("SELECT count(id) as count FROM `forum_thread` where deleted_at is null"));
  }

  public function entreprenaurConnectingPartner() {
    
    $data= DB::select(DB::raw("SELECT count(user_type) as count FROM `user_mentor_partner_connection_mapper` as t1 join users on t1.connected_to = users.id where users.user_type = 'partner' AND t1.connection_status =1 AND  t1.deleted_at is null and users.deleted_at is null"));
    return $data[0]->count;
  }

  public function partnerWiseConnectingUser() {
   
    return DB::select(DB::raw(" SELECT user_type,user_id,first_name FROM `user_mentor_partner_connection_mapper` as t1 join users on t1.connected_to = users.id where user_type = 'partner' AND connection_status =1 AND user_mentor_partner_connection_mapper.deleted_at is null"));
  }

  public function userRegistrationFromWeb() {
    $data= DB::select(DB::raw("select count(*) as count from users where facebook_token is null and google_token is null and deleted_at is null and user_type='entrepreneur'"));
    return $data[0]->count;
  }

  public function userRegistrationFromFacebook() {
    $data= DB::select(DB::raw("select count(*) as count from users where facebook_token is not null and deleted_at is null"));
    return $data[0]->count;
  }

  public function userRegistrationFromGoogle() {
    $data= DB::select(DB::raw("select count(*) as count from users where google_token is not null and deleted_at is null"));
    return $data[0]->count;
  }

  public function userFromContact() {
    $data = DB::select(DB::raw("select count(*)  as count from contact_us where deleted_at is null"));
    return $data[0]->count;
  }

  public function userFromCity() {
   
    $data = DB::select(DB::raw("SELECT COUNT(user_id) as count ,city.city_name from user_profile join city on user_profile.city=city.id where user_profile.deleted_at is null and city.deleted_at is null GROUP by city ORDER by count DESC LIMIT 10"));
    return $data;
  }

  public function trendingBlog() {
   
    $data = DB::select(DB::raw("Select taxonomy_name ,sum(t.blog_likes) as blog_likes_count from (SELECT blog_likes,blog_posts.id,taxonomies.taxonomy_name from blog_posts join blog_taxonomy_mapper on blog_posts.id=blog_taxonomy_mapper.blog_id join taxonomies on blog_taxonomy_mapper.taxonomy_id=taxonomies.id GROUP BY blog_posts.id,taxonomies.taxonomy_name ORDER BY `blog_posts`.`blog_likes` DESC) as t where blog_posts.blog_status='Published'  group by taxonomy_name ORDER BY blog_likes_count desc"));
    return $data;
  }
  public function countWtiApplicants(){
   $data = DB::select(DB::raw("Select count(*) as count  from wti_award_applicants where deleted_at is null"));
   return $data;
 } 
 public function countWtiApplicantsStateWise(){
  $data = DB::select(DB::raw("SELECT (
    CASE
    WHEN wti_award_applicants.state>0
    THEN UPPER(state.state_name)
    ELSE UPPER(wti_award_applicants.state)
    END
  ) AS state,count(wti_award_applicants.id) as count FROM `wti_award_applicants` left JOIN state on state.id = wti_award_applicants.state GROUP BY UPPER(state)"));
  return $data;
}

  }
 ?>
