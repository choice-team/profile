<?php
namespace App\Http\Controllers;

use App\Users;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use App\Models\AreaOfSpecialization;


class AreaOfSpecializationsController extends Controller
{
  
    public function __construct()
    {       
        
        $this->middleware('auth:api', ['except' => ['index']]);
    }
  
  
    /**
 * @api {get} api/profile/area_of_specializations list of area of specialization
 * @apiName index
 * @apiGroup admin
 *
 * @apiSuccess {String} area_of_specialization  of the area_of_specialization.
 * @apiSuccess {String} updated_by  of the area_of_specialization.
 * @apiSuccess {timestamp} updated_at  of the area_of_specialization.
 * @apiSuccess {timestamp} created_at  of the area_of_specialization.
 */
    public function index(Request $request) {
        
            $limit = $request->limit?$request->limit:10;
            
           // $area  = new AreaOfSpecialization();
           
           $area = AreaOfSpecialization::
            select('area_of_specializations.area_of_specialization','area_of_specializations.id','area_of_specializations.updated_at',DB::raw("TRIM(CONCAT_WS(' ',u.first_name,u.last_name)) as updated_by,area_of_specializations.deleted_at"))
            ->leftjoin('users as u','u.id','=','area_of_specializations.updated_by');
                        
            if($request->sort == 'name') {
                $data = ($request->order == 1)? $area->orderBy('area_of_specialization') : $area->orderBy('area_of_specialization','DESC');
            } else if($request->sort == 'date') {
                $data = ($request->order == 1) ? $area->orderBy('updated_at') : $area->orderBy('updated_at','DESC');
            } else {
                $data = $area->orderBy('updated_at','DESC');
            }

            $search = str_replace("%20"," ",$request->keyword);
            if(isset($search) && !empty($search))
            {
                $data = $area->where('area_of_specializations.area_of_specialization','like','%'.$search.'%');
            }
            $data = isset($request->flag)?$data->withTrashed()->get():$data->withTrashed()->paginate($limit);
            // $data = $data->paginate($limit);
            if($data->count()==0) {
                return new JsonResponse(['message' => 'No Data found'], 200);
            } 
            
            return new JsonResponse($data);
        
    }

    /**
 * @api {get} api/profile/area_of_specializations/{id} show speicific area of specialization
 * @apiName show area of specialization
 * @apiGroup admin
 *
 * @apiSuccess {String} area_of_specialization  of the area_of_specialization.
 * @apiSuccess {String} updated_by  of the area_of_specialization.
 * @apiSuccess {timestamp} updated_at  of the area_of_specialization.
 * @apiSuccess {timestamp} created_at  of the area_of_specialization.
 */
    public function show($id) {
        try {
            $area_of_specialization = AreaOfSpecialization::find($id);

            if($area_of_specialization === null)
                return new JsonResponse(['message' => 'No Data found'], 200);   

            $userName = Users::where('id',$area_of_specialization['updated_by'])->first();
            if($userName) {
                $area_of_specialization['updated_by'] =  trim($userName->first_name.' '.$userName->last_name);
            } else {
                $area_of_specialization['updated_by'] = '';
            }
            return new JsonResponse($area_of_specialization);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

/**
 * @api {post} api/profile/area_of_specializations store  area of specialization
 * @apiName store area of specialization
 * @apiGroup admin
 *
 * @apiParam {String} area_of_specialization to add in area_of_specialization
 * @apiParam {String} updated_by role of the user who add
 * 
 * @apiSuccess {String} area_of_specialization  of the area_of_specialization.
 * @apiSuccess {String} updated_by  of the area_of_specialization.
 * @apiSuccess {timestamp} updated_at  of the area_of_specialization.
 * @apiSuccess {timestamp} created_at  of the area_of_specialization.
 */
    public function store(Request $request) {
        try {
            $this->validate($request,[
                'area_of_specialization'   => 'required|unique:area_of_specializations'
            ]);
            $data = $request->toArray();
            
            $data['updated_by'] = $request->user_id;
            $area_of_specialization = new AreaOfSpecialization($data);

            // HERE: additional, non-fillable fields

            $area_of_specialization->save();

            if($area_of_specialization)
                return new JsonResponse([$area_of_specialization], 200);

            return new JsonResponse(['message' => 'Server Error'], 500);

        } catch (\Illuminate\Validation\ValidationException $e) {

            return validation_exception($e);

        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

/**
 * @api {delete} api/profile/area_of_specialization/{id} delete  area of specialization
 * @apiName destroy area of specialization
 * @apiGroup admin
 *
 * @apiParam {Numeric} id to delete the respective area_of_specialization
 * 
 * @apiSuccess {String} area_of_specialization  of the area_of_specialization.
 */
    public function destroy($id) {
        try {
            $area_of_specialization = AreaOfSpecialization::find($id);

            if($area_of_specialization === null)
                return new JsonResponse(['message' => 'No Data found'], 200);

            $area_of_specialization->delete();

            return new JsonResponse(['message' => 'Deleted Successfuly'], 200);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

    /**
 * @api {patch} api/profile/area_of_specialization/{id} update  area of specialization
 * @apiName udpate area of specialization
 * @apiGroup admin
 *
 * @apiParam {String} area_of_specialization to add in area_of_specialization
 * @apiParam {String} updated_by role of the user who add
 * 
 * @apiSuccess {String} area_of_specialization  of the area_of_specialization.
 * @apiSuccess {String} updated_by  of the area_of_specialization.
 * @apiSuccess {timestamp} updated_at  of the area_of_specialization.
 * @apiSuccess {timestamp} created_at  of the area_of_specialization.
 */
    public function update($id, Request $request) {
        try {
            $area_of_specialization = AreaOfSpecialization::find($id);

            if($area_of_specialization === null)
                return new JsonResponse(['message' => 'Not found'], 404);

            $data = $request->toArray();
            $data['updated_by'] = $request->user_id;
            $area_of_specialization->update($data);

            return new JsonResponse($area_of_specialization);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

/**
 * @api {get} api/area_of_specializations/search/{name} update  area of specialization
 * @apiName udpate area of specialization
 * @apiGroup admin
 *
 * @apiParam {String} name to search in area_of_specialization
 * 
 * @apiSuccess {String} area_of_specialization  of the area_of_specialization.
 * @apiSuccess {String} updated_by  of the area_of_specialization.
 * @apiSuccess {timestamp} updated_at  of the area_of_specialization.
 * @apiSuccess {timestamp} created_at  of the area_of_specialization.
 */
    public function search(Request $request, $name) {
        try {
            $limit = $request->limit?$request->limit:10;

            $data = AreaOfSpecialization::
            select('area_of_specializations.area_of_specialization','area_of_specializations.id','area_of_specializations.updated_at','area_of_specializations.deleted_at',DB::raw("TRIM(CONCAT_WS(' ',u.first_name,u.last_name)) as updated_by"))
            ->leftjoin('users as u','u.id','=','area_of_specializations.updated_by')
            ->where('area_of_specializations.area_of_specialization','like',$name.'%')->paginate($limit);
            
            if(count($data) == 0)
                return new JsonResponse(['message' => 'No Data found'], 200);

            return new JsonResponse($data);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }
    public function restore($id) {
        AreaOfSpecialization::withTrashed()->find($id)->restore();
        return new JsonResponse([],200);
    }
}
