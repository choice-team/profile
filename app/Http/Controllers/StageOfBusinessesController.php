<?php
namespace App\Http\Controllers;

use App\Users;
use Illuminate\Http\Request;
use App\Models\StageOfBusiness;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;


class StageOfBusinessesController extends Controller
{
    public function index(Request $request) {
        $limit = $request->limit? $request->limit:10;
        $offset = $request->offset? $request->offset:0;
        
        $stage = StageOfBusiness::select('stage_of_businesses.stage_of_business','stage_of_businesses.id','stage_of_businesses.updated_at',DB::raw("TRIM(CONCAT_WS(' ',u.first_name,u.last_name)) as updated_by"),'stage_of_businesses.deleted_at')
        ->leftjoin('users as u','u.id','=','stage_of_businesses.updated_by');

        if($request->sort == 'name') {
            $data = ($request->order == 1)? $stage->orderBy('stage_of_business') : $stage->orderBy('stage_of_business','DESC');
        } else if($request->sort == 'date') {
            $data = ($request->order == 1) ? $stage->orderBy('updated_at') : $stage->orderBy('updated_at','DESC');   
        } else {
            $data = $stage->orderBy('updated_at','DESC');
        }
        
        $search = str_replace("%20"," ",$request->keyword);
        if(isset($search) && !empty($search))
        {
            $data = $stage->where('stage_of_businesses.stage_of_business','like','%'.$search.'%');
        }

        $data = isset($request->flag)?$data->withTrashed()->get():$data->withTrashed()->paginate($limit);

        
        if($data->count()==0) {
            return new JsonResponse(['message' => 'No Data found'], 200);
        } 
        
        return new JsonResponse($data);
    }

    public function show($id) {
        try {
            $stage_of_business = StageOfBusiness::find($id);

            if($stage_of_business === null)
                return new JsonResponse(['message' => 'Not found'], 404);

            $userName = Users::where('id',$stage_of_business['updated_by'])->first();
            if($userName) {
                $stage_of_business['updated_by'] =  trim($userName->first_name.' '.$userName->last_name);
            } else {
                $stage_of_business['updated_by'] = '';
            }
            return new JsonResponse($stage_of_business);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

    public function store(Request $request) {
        try {
            $this->validate($request,[
                'stage_of_business'   => 'required|unique:stage_of_businesses'
            ]);
            $data = $request->toArray();
            $data['updated_by'] = $request->user_id;
            
            $stage_of_business = new StageOfBusiness($data);

            // HERE: additional, non-fillable fields

            $stage_of_business->save();

            if($stage_of_business)
                return new JsonResponse([],200);

            return new JsonResponse(['message' => 'Server Error'], 500);
        } catch (\Illuminate\Validation\ValidationException $e) {
            return validation_exception($e);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

    public function destroy($id) {
        try {
        $stage_of_business = StageOfBusiness::find($id);

        if($stage_of_business === null)
            return new JsonResponse(['message' => 'Not found'], 404);

        $stage_of_business->delete();

        return new JsonResponse(['message' => 'Deleted Successfuly'], 200);
        } catch (\Exception $e) {
            return general_expection($e);
        }
        
    }

    public function update($id, Request $request) {
        try {
            $stage_of_business = StageOfBusiness::find($id);

            if($stage_of_business === null)
                return new JsonResponse(['message' => 'Not found'], 404);

            $data = $request->toArray();
            $data['updated_by'] = $request->user_id;
            $stage_of_business->update($data);

            return new JsonResponse([],200);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }


    public function search(Request $request, $business) {
        try {
            $limit = $request->limit?$request->limit:10;
            $data = StageOfBusiness::
            select('stage_of_businesses.stage_of_business','stage_of_businesses.id','stage_of_businesses.updated_at','stage_of_businesses.deleted_at',DB::raw("TRIM(CONCAT_WS(' ',u.first_name,u.last_name)) as updated_by"),'stage_of_businesses.deleted_at')
            ->leftjoin('users as u','u.id','=','stage_of_businesses.updated_by')
            ->where('stage_of_businesses.stage_of_business','like', $business.'%')->paginate($limit);
            if(count($data) == 0)
                return new JsonResponse(['message' => 'No Data found'], 200);

            return new JsonResponse($data);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

    public function restore($id) {
        StageOfBusiness::withTrashed()->find($id)->restore();
        return new JsonResponse([],200);
    }
}
