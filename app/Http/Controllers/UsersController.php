<?php

namespace App\Http\Controllers;

use DB;
use App\City;
use App\State;
use App\Users;
use App\parnter;
use App\Category;
use App\Language;
use Carbon\Carbon;
use App\Enterpreneur;
use App\UsersProfile;
use App\PartnerProfile;
use App\PartnerServices;
use App\CompanyDirectors;
use App\companyDocuments;
use App\UserQualification;
use Illuminate\Http\Request;
use App\EnterpreneurInterest;
use App\Rules\DeactivateDate;
use Illuminate\Http\Response;
use App\Rules\GoogleRecaptcha;
use League\Flysystem\Exception;
use Illuminate\Http\JsonResponse;
use App\Services\PermissionMapper;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Cache;
use App\Models\EntrepreneurBusinessInfo;
use App\PartnerEntrepreneurRelationship;
use App\Permissions\HasPermissionsTrait;
use League\Flysystem\FilesystemInterface;
use App\UserMentorPartnerConnectionMapper;
use GrahamCampbell\Flysystem\Facades\Flysystem;
use App\UsersForDeactivatedList;


class UsersController extends Controller
{
  use HasPermissionsTrait;

  private $user_id;

  public function __construct()
  {
    $this->user_id = Auth::user()->id;
  }

  public function entrepreneurList(Request $request){

    $limit = $request->input('limit', 10);
    $offset = $request->input('offset', 0);

    $data = [];
    $totalCount = 0;

    $data = Users::select(DB::raw('group_concat(DISTINCT category_name SEPARATOR "|")  as category_name'),'users.id','users.user_slug', 'users.first_name', 'users.last_name', 'users.mobile', 'users.email', 'users.user_code', 'users.approval_status','users.id','user_profile.city','user_profile.state')
    ->leftjoin('user_profile','user_profile.user_id','=','users.id')
    ->leftjoin('entrepreneur_interest_area','users.id','=','entrepreneur_interest_area.user_id')
    ->leftjoin('categories','entrepreneur_interest_area.interest_area_id','=','categories.id')
    ->where(['users.user_type'=>'entrepreneur'])
    ->where('entrepreneur_interest_area.deleted_at',null)
    ->groupBy('users.id');

    if($request->has('approval_status')){

      $data = $data->where('users.approval_status', $request->approval_status);
    }
    $searchString = $request->keyword;
    if($request->has('keyword') && !empty($request->keyword)){
      $data = $data->where(function($data) use ($request,$searchString) {

        $data->orWhere('users.first_name', 'LIKE', "%$request->keyword%");
        $data->orWhere('users.last_name', 'LIKE', "%$request->keyword%");
        $data->orWhere('users.mobile', 'LIKE', "%$request->keyword%");
        $data->orWhere('users.email', 'LIKE', "%$request->keyword%");
        $data->orWhere('users.user_code', 'LIKE', "%$request->keyword%");
        $data->orWhere('categories.category_name', 'LIKE', "%$request->keyword%");
      });

    } 
    $totalCount = count($data->get());

    $data = $data->limit($limit)
    ->offset($offset)
    ->orderBy('users.id','desc')
    ->get()
    ->transform(function($item,$key) {
      $item->mobile = base64_encode($item->mobile);
      $item->email = base64_encode($item->email);
      $item->user_code = base64_encode($item->user_code);
      $item->city = City::find($item->city)?City::find($item->city)->city_name:"";
      $item->state = State::find($item->state)?State::find($item->state)->state_name:"";
      return $item;
    });

    return response()->json([
      'total_count' => $totalCount,
      'data' => $data
    ]);
  }

    /**
 * @api {get} api/profile/profile/entrepreneur/{id} Request User information
 * @apiName getEntreprenuer
 * @apiGroup Profile
 *
 * @apiParam {Number} id Users unique ID.
 *
 * @apiSuccess {String} firstname Firstname of the User.
 * @apiSuccess {String} lastname  Lastname of the User.
 */
    public function entrepreneurProfile(Request $request)
    {

      $data = [];
      if($request->user_slug) {
        $userData = Users::where('user_slug',$request->user_slug)->first();
        if(!$userData) {
          return response()->json([
            'data' => $data
          ],
          Response::HTTP_NOT_FOUND
        ); 
        }
        $user_id = $userData->id;
      } else {
        $user_id = $this->user_id;
      }

      if(isset($this->user_id)){
        $data['connection_status'] = PartnerEntrepreneurRelationship::select('connection_status')->where(['user_id'=>$this->user_id,'connected_to'=>$user_id,'deleted_at'=>NULL])
        ->get();

      }
      $data['userProfile'] = Users::select('users.first_name','users.last_name','users.email','user_profile.gender','user_profile.birth_year','user_profile.country','user_profile.state','user_profile.about','user_profile.profile_pic','user_profile.entreprise_name','users.mentorship_status','user_profile.other_city',DB::raw('
        CASE
        WHEN user_profile.state>0
        THEN state.state_name
        ELSE user_profile.state
        END AS state'),DB::raw('
        CASE
        WHEN user_profile.city>0
        THEN city.city_name
        ELSE user_profile.city
        END AS city'))
      ->leftjoin('user_profile','user_profile.user_id','=','users.id')
      ->leftjoin('state','state.id','=','user_profile.state')
      ->leftjoin('city','city.id','=','user_profile.city')


      ->where(['users.id'=>$user_id])
      ->get()->transform(function($item,$key){
        $item->email = base64_encode($item->email);
        return $item;
      });

      $data['suggested_mentors'] = EnterpreneurInterest::select(DB::Raw('DISTINCT(users.user_slug) as slug'),'users.first_name','users.last_name','user_profile.profile_pic','user_profile.gender')
      ->leftjoin('users','users.id','=','entrepreneur_interest_area.user_id')
      ->leftjoin('user_profile','user_profile.user_id','=','users.id')
      ->whereIn('interest_area_id', function($query) use ($user_id)
      {

        $query->select(\DB::raw('interest_area_id'))
        ->from('entrepreneur_interest_area')
        ->whereRaw('entrepreneur_interest_area.deleted_at is null')
        ->whereRaw('user_id ='.$user_id);
      })
        // ->leftjoin('user_mentor_partner_connection_mapper','user_mentor_partner_connection_mapper.connected_to','=','users.id')
        // ->where('user_mentor_partner_connection_mapper.connection_status', 0)
      ->where('users.mentorship_status','=','1')
      ->where('users.id','!=',$user_id)
      ->whereNotIn('users.id',function($query) use ($user_id){
       $query->select(\DB::raw('connected_to'))
       ->from('user_mentor_partner_connection_mapper')
       ->where('user_id',$user_id)
       ->where('connection_status',1)
       ->where('deleted_at',null);
     })
      ->get();
         // echo $data['suggested_mentors']; exit;
      $data['connected_partners'] = Users::select('users.first_name','partner_profile.logo','partner_profile.slug','user_profile.gender')
      ->leftjoin('user_profile','user_profile.user_id','=','users.id')
      ->leftjoin('partner_profile','partner_profile.user_id','=','users.id')
      ->leftjoin('user_mentor_partner_connection_mapper','user_mentor_partner_connection_mapper.connected_to','=','users.id')
      ->where(['user_mentor_partner_connection_mapper.user_id'=>$user_id,'users.user_type'=>'partner'])
      ->where('user_mentor_partner_connection_mapper.connection_status', 1)
      ->get();
      $data['connected_members'] = Users::select('users.first_name','users.last_name','user_profile.profile_pic','users.user_slug as slug','user_profile.gender')
      ->leftjoin('user_profile','user_profile.user_id','=','users.id')
      ->join('user_mentor_partner_connection_mapper','user_mentor_partner_connection_mapper.connected_to','=','users.id')
      ->where(['user_mentor_partner_connection_mapper.user_id'=>$user_id,'users.user_type'=>'entrepreneur'])
      ->where('user_mentor_partner_connection_mapper.connection_status', 1)
      ->get();



      $data['suggested_partners'] = PartnerServices::select(DB::raw('DISTINCT(partner_profile.slug) as slug'),'users.first_name','users.last_name','partner_profile.logo','user_profile.gender')
      ->leftjoin('users','users.id','=','partner_services.user_id')
      ->leftjoin('user_profile','user_profile.user_id','=','users.id')
      ->leftjoin('partner_profile','partner_profile.user_id','=','users.id')
      ->leftjoin('user_mentor_partner_connection_mapper','user_mentor_partner_connection_mapper.connected_to','=','users.id')
      ->join('service_category_mapper','service_category_mapper.service_id','=','partner_services.id')
      ->whereIn('service_category_mapper.category_id', function($query) use ($user_id)
      {
        $query->select(\DB::raw('interest_area_id'))
        ->from('entrepreneur_interest_area')
        ->whereRaw('entrepreneur_interest_area.deleted_at is null')
        ->whereRaw('user_id ='.$user_id);
      })
      ->whereNotIn('users.id',function($query) use ($user_id){
       $query->select(\DB::raw('connected_to'))
       ->from('user_mentor_partner_connection_mapper')
       ->where('user_id',$user_id)
       ->where('connection_status',1)
       ->where('deleted_at',null);
     })
      ->where('user_mentor_partner_connection_mapper.connection_status', "0")
      ->get();
        // echo $data['suggested_partners']; exit;

      return response()->json([
              // 'data_count' => count($data['partners']),
        'data' => $data
      ]);
    // return Response()->json(array('result' => $data), 200);
    }
    public function mentorProfile(Request $request,$user_id){

      $data['userProfile'] =Users::find($user_id)->userProfile;
      $data['mentor_expertise'] = MentorSkill::select('skill_name')
      ->join('skills','mentor_skill_mapper.skill_id','=','skills.skill_id')
      ->where(['mentor_skill_mapper.user_id'=>$user_id])
      ->get();
      $data['connected_entrepreneurs'] = Users::select('users.name')
      ->join('user_mentor_partner_connection_mapper','user_mentor_partner_connection_mapper.user_id','=','users.id')
      ->where(['user_mentor_partner_connection_mapper.connected_to'=>$user_id,'users.user_type'=>'entrepreneur'])
      ->get();
        // return $data['connected_entrepreneurs'];
      return Response()->json(array('result' => $data), 200);

    }


    public function update(Request $request, $user_id) {

      $request['user_id'] = $user_id;
      $this->validate($request, [
        'user_id'=>'required|exists:user_profile',
      ]);


      $userProfile = UsersProfile::where('user_id','=','3')->findorFail($request->input('user_id'));
      foreach ($request->all() as $key=>$value) {

        if($key == 'user_id'){
          continue;
        }
        $users->$key = $value;
      }

      $userProfile->save();

      return $userProfile;
    }

    public function getServiceList($request,$user_id)
    {

      $limit = $request->input('limit');
      $offset = $request->input('offset');
      $perPage =  10;
      $keyword = $request->get('search');


      if (!empty($keyword)) {

        $partner_services = PartnerServices::with(['category'=>function($q) {
          $q->select('categories.id');
        }])->select('partner_services.*','users.first_name as name')
        ->leftjoin('users','users.id','=','partner_services.user_id')
        ->where('users.first_name', 'LIKE', "%$keyword%")
        ->orWhere('service_desc', 'LIKE', "%$keyword%")
        ->orWhere('service_title', 'LIKE', "%$keyword%")
        ->orWhere('description', 'LIKE', "%$keyword%")
        ->latest()->paginate($perPage);

      } else {

        $partner_services = PartnerServices::with(['category'=>function($q) {
          $q->select('categories.id');
        }])
        ->select('partner_services.id','service_desc','service_title','users.first_name as name')
        ->leftjoin('users','users.id','=','partner_services.user_id')
        ->where(['partner_services.user_id'=>$user_id])
        ->orderBy('partner_services.created_at','desc')
        ->paginate($perPage);
      }

      return $partner_services;
    }
    public function entrepreneurCount(){

      $data['count']=Users::where('user_type','entrepreneur')->count();
      return response()->json([

        'data' => $data
      ]);
    }
    public function getAreaOfInterest(){
      $data['category_type'] = Category::select('id','category_name','category_type')->get();
      return response()->json([
        'count'=>count($data['category_type']),
        'data' => $data
      ]);
    }
    public function storeAreaOfInterest(Request $request){

      $provideMentorShip = "";
      $record['mentorship_status'] =0;

      if($request->has('provideMentorShip')) {
        if($request->provideMentorShip===TRUE){
          $record['mentorship_status']=1;
        }else{
          $record['mentorship_status']=0;
        }
        $unique = ['id' => $request->user_id];
        $data = Users::updateOrCreate($unique,$record);
      }

      $delete_data = EnterpreneurInterest::where('user_id',$this->user_id)->delete();
      $inserts = [];
      $this->validate($request,[
        'user_id' => 'required'
      ]);
      foreach ($request->area_of_interest as $key => $value) {

       $inserts[] = [ 'user_id' => $this->user_id,
       'interest_area_id' => $value,
     ];
   }

   $insert_data = DB::table('entrepreneur_interest_area')->insert($inserts);
   if ($insert_data) {
    $data['status_code'] = '200';
    $data['message'] = "Inserted Successfully";
  }

  return response()->json([
  ],Response::HTTP_OK
);

}
public function getUserAreaOfInterest($user_slug){
  $user = Users::where('user_slug',$user_slug)->first();
  if(!$user) {
    return response()->json([

    ],
    Response::HTTP_NOT_FOUND
  );
  } 
  $id = $user->id;
  $data['area_of_interest'] = EnterpreneurInterest::select('users.first_name','users.last_name','categories.category_name','categories.id','categories.category_type','user_profile.gender')
  ->join('users','users.id','=','entrepreneur_interest_area.user_id')
  ->join('user_profile','users.id','=','user_profile.user_id')
  ->join('categories','categories.id','=','entrepreneur_interest_area.interest_area_id')
  ->where(['entrepreneur_interest_area.user_id'=>$id])
  ->get();

  $userDetails = Users::select('users.mentorship_status')->where('users.id', $id)->first();
  $data['mentorship_status'] = isset($userDetails->mentorship_status) ? ($userDetails->mentorship_status) : null;
  return response()->json([
    'data' => $data
  ]);
}

public function getQualification(Request $request){

  $data = UserQualification::select('qualification.id','qualification.qualification_name','users_qualification.area_of_specialisation','users_qualification.linked_in_link')
  ->join('qualification','qualification.id','=','users_qualification.qualification_id');
  if($request->get('user_slug') && $request->has('user_slug')) {

    $getUsersId =  Users::select('id')->where('user_slug','=',$request->user_slug)->first();

    $data = $data->where(['users_qualification.user_id'=> $getUsersId->id]);

  } else { 
   $data = $data->where(['users_qualification.user_id'=>$this->user_id]);
 }
 

 $data = $data->get();
 return response()->json([
   'data' => $data
 ]);
}

public function storeQualification(Request $request){
  $requestData = $request->all();

  $this->validate($request,[
    'user_id'   => 'required',
    'qualification_id'   => 'required'
  ]);
  $delete_data = UserQualification::where('user_id',$request->user_id)->delete();
  if ($delete_data) {
    $insert_qualification = UserQualification::create($requestData);
  }else{
    $insert_qualification = UserQualification::create($requestData);
  }

  if ($insert_qualification) {
    $data['last_id'] = $insert_qualification->id;
  }

  return response()->json([
  ],Response::HTTP_OK
);
}
public function updateQualification(Request $request) {
  $requestData = $request->all();
  $this->validate($request,[
    'qualification_id'   => 'required',
    'user_id'         => 'required'
  ]);

  $qualification = UserQualification::findOrFail($this->user_id);
  $qualification_update = $qualification->update($requestData);

  if ($qualification_update) {
    return $data;
  }
}

public function storeEntreprenuer(Request $request){
  $requestData = $request->all();
  $delete_data = UsersProfile::where('user_id',$request->user_id)->delete();
  if ($delete_data) {
    $insert_qualification = UsersProfile::create($requestData);
  }else{
    $insert_qualification = UsersProfile::create($requestData);
  }

  if ($insert_qualification) {
    $data['last_id'] = $insert_qualification->id;
  }

  return $data;

}

public function getBusinessInfoListApi(Request $request)
{
  try {

    $record = $this->getBusinessInfoList($request);
    if(count($record)==0) {
      return response()->json([

      ],
      Response::HTTP_NOT_FOUND
    ); 
    }
    return response()->json([
      'total_count' => $record['total_count'],
      'data_count' => count($record['data']),
      'data' => $record['data']
    ],Response::HTTP_OK
  );

  } catch (\Exception $e) {

    return general_expection($e);
  }

}

public function getBusinessInfoList($request)
{

        // $limit = $request->input('limit',10);
        // $offset = $request->input('offset',0);
  if($request->user_slug) {
    $userData = Users::where('user_slug',$request->user_slug)->first();

    if(!$userData) {
      return [];
    }
    $user_id = $userData->id;
  } else {
    $user_id = $this->user_id;
  }

  $data = EntrepreneurBusinessInfo::select('entrepreneur_business_info.*','users.first_name')
  ->leftjoin('users','users.id','=','entrepreneur_business_info.user_id')
  ->where(['entrepreneur_business_info.deleted_at'=>NULL]);

  if ($user_id && !empty($user_id)) {
    $data->where('entrepreneur_business_info.user_id','=',$user_id);
  }

  if ($request->has('business_info_id') && !empty($request->business_info_id)) {
    $data->where('entrepreneur_business_info.id','=',$request->business_info_id);
  }



  $data = $data->limit(1)
  ->orderBy('entrepreneur_business_info.id','desc')
  ->get()->transform(function($item,$key) {
    unset($item->user_id);
    unset($item->id);
    return $item;
  });
  $totalCount = count($data);
  return [
    'data' => $data,
    'total_count' => $totalCount
  ];
}


public function storeBusinessInfoApi(Request $request)
{
  try {
    $id = $this->user_id;

    $slug = Users::where('id',$id)->first()->user_slug;

    $request->merge(['user_slug' => $slug]);

            $validator = $this->businessInfoValidator($request); // validate the request params

            $result = $this->storeBusinessInfo($request);

            return response()->json([
                   'id' => $result->user_slug,  //changes id to  slug
                 ],Response::HTTP_OK
               );

          } catch (\Illuminate\Validation\ValidationException $e) {

            return validation_exception($e);

          } catch (\Exception $e) {

            return general_expection($e);
          }

        }

        public function businessInfoValidator($request)
        {
         $validator = $this->validate($request, [
          'user_slug' => 'required|string',
          'existing_business_status' => 'required|boolean',
          'registered_entity_status' => 'required_if:existing_business_status,true,1|boolean',
          'name_of_enterprise' => 'required_if:existing_business_status,true,1|string|max:150',
          'country' => 'required_if:existing_business_status,true,1|max:150',
          'state' => 'required_if:existing_business_status,true,1|string|max:150',
          'city' => 'required_if:existing_business_status,true,1|string|max:150',
          'other_city' => 'required_if:city,==,Other',
          //'address_line_1' => 'required_if:existing_business_status,true,1|string',
          'sector_of_entreprise' => 'required_if:registered_entity_status,true,1|string',
          //'type_of_enterprise' => 'required_if:existing_business_status,true,1|string',
          'registration_year' => 'required_if:existing_business_status,true,1|string',
          'stage_of_business' => 'required_if:existing_business_status,true,1|string',
          'number_of_employee' => 'required_if:existing_business_status,true,1|string',
          'financial_turn_over' => 'required_if:existing_business_status,true,1|string',
            // 'website_link' => 'required_if:existing_business_status,true,1|string',
          'registered_with_pdp_status' => 'required_if:existing_business_status,true,1|boolean',
          'supported_by_gov_status' => 'required_if:existing_business_status,true,1|boolean',
          'future_plan_for_business_status' => 'required_if:existing_business_status,false,0|boolean',
          'student_type_status'=> 'required_if:existing_business_status,false,0|boolean',
          'business_plan_ready_status' => 'required_if:existing_business_status,false,0|boolean',
          //'setting_up' => 'required_if:registered_entity_status,false,0|string',
          'financing'=>'required_if:existing_business_status,true,1|string',
          'investments'=>'required_if:existing_business_status,true,1|string',
          'business_start' =>'required_if:existing_business_status,true,1|string'
        ]);

        
         return $validator;

       }

       public function storeBusinessInfo($request)
       {


        $unique = ['user_id' => $request->id];

        $record = $this->businessInfoRequestParams($request);

        return EntrepreneurBusinessInfo::updateOrCreate($unique,$record);
      }

      public function updateBusinessInfoApi(Request $request, $business_info_id)
      {
       try {

            $validator = $this->businessInfoValidator($request);  // validate the request params

            $record = $this->updateBusinessInfo($request, $business_info_id);

            if (!empty($record)) {

              return response()->json([
                'success',
              ],Response::HTTP_OK
            );

            } else {

              return response()->json([
                'error',
              ],Response::HTTP_BAD_REQUEST
            );
            }

          } catch (\Illuminate\Validation\ValidationException $e) {

            return validation_exception($e);

          } catch (\Exception $e) {

            return general_expection($e);
          }

        }

        public function updateBusinessInfo($request, $business_info_id)
        {

          $record = $this->businessInfoRequestParams($request);

          return EntrepreneurBusinessInfo::where(['id'=>$business_info_id])->update($record);
        }

        public function businessInfoRequestParams($request)
        {
        // echo $request->user_slug; exit;
          $id = Users::where('user_slug',$request->user_slug)->first()->id;
        // echo "id is $id"; exit;

          $record = [
            'user_id' => $id,
            'existing_business_status' => $request->input('existing_business_status',null),
            'registered_entity_status' => $request->input('registered_entity_status',null),
            'name_of_enterprise' => $request->input('name_of_enterprise',null),
            'country' => $request->input('country',null),
            'state' => $request->input('state',null),
            'city' => $request->input('city',null),
            'other_city' => $request->input('other_city',null),
            'address_line_1' => $request->input('address_line_1',null),
            'address_line_2' => $request->input('address_line_2',null),
            'sector_of_entreprise' => $request->input('sector_of_entreprise',null),
            'type_of_enterprise' => $request->input('type_of_enterprise',null),
            'registration_year' => $request->input('registration_year',null),
            'stage_of_business' => $request->input('stage_of_business',null),
            'number_of_employee' => $request->input('number_of_employee',null),
            'financial_turn_over' => $request->input('financial_turn_over',null),
            'website_link' => $request->input('website_link',null),
            'enterprise_fb_link' => $request->input('enterprise_fb_link',null),
            'enterprise_twitter_link' => $request->input('enterprise_twitter_link',null),
            'registered_with_pdp_status' => $request->input('registered_with_pdp_status',null),
            'supported_by_gov_status' => $request->input('supported_by_gov_status',null),
            'future_plan_for_business_status' => $request->input('future_plan_for_business_status',null),
            'student_type_status' => $request->input('student_type_status',null),
            'business_plan_ready_status' => $request->input('business_plan_ready_status',null),
            'setting_up' => $request->input('setting_up',null),
            'business_start' => $request->input('business_start',null),
            'cofounder' => $request->input('cofounder',null),
            'other_sector_of_entreprise' => $request->input('other_sector_of_entreprise',null),
            'financing' => $request->input('financing',null),
            'stage_of_funding' => $request->input('stage_of_funding',null),
            'investments' => $request->input('investments',null),
            'enterprise_linkedin_link' => $request->input('enterprise_linkedin_link',null)
          ];

          return $record;
        }

        public function storePersonalInfoApi(Request $request)
        {

          try {

            $validator = $this->personalInfoValidator($request); // validate the request params

            $result = $this->storePersonalInfo($request);

            return response()->json([
            ],Response::HTTP_OK
          );

          } catch (\Illuminate\Validation\ValidationException $e) {
            Log::error($e->getMessage());
            return array("error_message"=>'Something went wrong');

            //return validation_exception($e);

          } catch (\Exception $e) {
            Log::error($e->getMessage());
            return array("error_message"=>'Something went wrong');

            //return general_expection($e);
          }

        }

        public function personalInfoValidator($request)
        {

          $validator = $this->validate($request, [

            'user_id' => 'required|integer',
            'gender' => 'required|string',
            'birth_year' => 'required|string',
            'country' => 'required|string',
            'state' => 'required|string',
            'city' => 'required|string',
            'other_city' => 'required_if:city,==,Other',
            'last_name' => 'string',
          ]);
          return $validator;


        }

        public function storePersonalInfo(Request $request)
        {

          $unique = ['user_id' => $request->user_id];

          if($request->has('last_name')){

            Users::where('id', $request->user_id)
            ->update(['last_name' => $request->last_name]);
          }

          $record = $this->personalInfoRequestParams($request);

          return UsersProfile::updateOrCreate($unique,$record);
        }

        public function personalInfoRequestParams(Request $request)
        {
          if(!empty($request->file('profile_pic'))){

            $uploadPath = 'uploads/profile_pic';

            if(!File::exists($uploadPath)) {

              File::makeDirectory($uploadPath, 0775, true, true);
            }

            $file = $request->file('profile_pic');
            $fileName = str_replace(" ", "_", time() . '_' . $file->getClientOriginalName());

            if ($file->move($uploadPath, $fileName)) {

              $record['profile_pic'] = $fileName;
            }
          }


          $record = [

            'user_id' => $request->user_id,
            'gender' => $request->gender,
            'birth_year' => $request->birth_year,
            'country' => $request->country,
            'state' => $request->state,
            'city' => $request->city,
            'other_city' => $request->other_city,
            'about' => $request->about,
          ];
          return $record;


        }

    //
        public function storeEducationalInfoApi(Request $request)
        {
          try {

            $custom_msg = [
              'qualification_id.required' => 'The qualification field is required.'
            ];

            $this->validate($request, [

              'user_id' => 'required|integer',
              'qualification_id' => 'required|integer',

            ],$custom_msg); // validate the request params

            $result = $this->storeEducationalInfo($request);

            return response()->json([
             'id' => $result->id,
           ],Response::HTTP_OK
         );

          } catch (\Illuminate\Validation\ValidationException $e) {

            return validation_exception($e);

          } catch (\Exception $e) {

            return general_expection($e);
          }

        }

        public function storeEducationalInfo($request)
        {

          $unique = ['user_id' => $request->user_id];

          $record = $this->educationalInfoRequestParams($request);

          return UserQualification::updateOrCreate($unique,$record);
        }

        public function educationalInfoRequestParams($request)
        {

          $record = [

            'user_id' => $request->user_id,
            'qualification_id' => $request->qualification_id,
            'area_of_specialisation' => $request->area_of_specialisation,
            'linked_in_link' => $request->linked_in_link,

          ];

          return $record;
        }

        public function storeConnectionStatus(Request $request){
          if(Auth::user()->user_type == 'partner') {
            $partner_id = Users::where('user_slug',$request->connected_to)->first()->id;
            $entrepreneur_id = $this->user_id;
            $following_to = $request->connection_status;
            $requestData = $request->all();
            $data = PartnerEntrepreneurRelationship::select('connected_to')->where(['user_id'=>$partner_id,'connected_to'=>$entrepreneur_id,'deleted_at'=>NULL])
            ->get();
            if($data->count()==0){
              $data['insert_value'] = PartnerEntrepreneurRelationship::create(['user_id'=>$partner_id,'connected_to'=>$entrepreneur_id,'connection_status'=> $following_to]);
            }else{
              $data['insert_value'] =  PartnerEntrepreneurRelationship::where(['user_id'=>$partner_id,'connected_to'=>$entrepreneur_id])
              ->update(['connection_status' => $request->connection_status]);
            }
          } else {  
            $partner_id = Users::where('user_slug',$request->connected_to)->first()->id;
            $entrepreneur_id = $this->user_id;
            $following_to = $request->connection_status;
            $requestData = $request->all();
            $data = PartnerEntrepreneurRelationship::select('connected_to')->where(['user_id'=>$entrepreneur_id,'connected_to'=>$partner_id,'deleted_at'=>NULL])
            ->get();
            if($data->count()==0){              
              $data['insert_value'] = PartnerEntrepreneurRelationship::create(['user_id'=>$entrepreneur_id,'connected_to'=>$partner_id,'connection_status'=> $following_to]);
              PartnerEntrepreneurRelationship::create(['user_id'=>$partner_id,'connected_to'=>$entrepreneur_id,'connection_status'=> $following_to]);
            }else{
              $data['insert_value'] =  PartnerEntrepreneurRelationship::where(['user_id'=>$entrepreneur_id,'connected_to'=>$partner_id])
              ->update(['connection_status' => $request->connection_status]);
              PartnerEntrepreneurRelationship::where(['user_id'=>$partner_id,'connected_to'=>$entrepreneur_id])->update(['connection_status' => $request->connection_status]);

            }
          }

          return response()->json([
          ],Response::HTTP_OK
        );
        }

        public function updateParterApi(Request $request, $id)
        {
          try {

            $messages = [];

            $this->validate($request, [
              'office_address_1' => 'required',

            ], $messages);

            return $this->storeUpdateParter($request, $id);

          } catch (\Illuminate\Validation\ValidationException $e) {

            return validation_exception($e);

          } catch (\Exception $e) {

            return general_expection($e);
          }
        }


        public function updateProfilePic(Request $request,FilesystemInterface $filesystem) {
          $imageExtension = array('jpeg','png','jpg','gif','svg','bmp');
          $imageMimeType = array('image/gif','image/jpeg','image/png','image/svg+xml','image/bmp');
          $uploadPath = 'uploads/profile_pics';
          if (!empty($request->file('profile_pic'))) {
           $file = $request->file('profile_pic');
           $file_name =  $file->getClientOriginalName();
           $filename = pathinfo($file_name, PATHINFO_FILENAME);
           $extension = pathinfo($file_name, PATHINFO_EXTENSION);

           $mime = mime_content_type($_FILES['profile_pic']['tmp_name']);
           if(!in_array($extension,$imageExtension) || !in_array($mime,$imageMimeType)) {
            return response()->json([
              'message'=> 'Not an image type'
            ],Response::HTTP_BAD_REQUEST
          );
          }
          $date = new \DateTime(null, new \DateTimeZone('Asia/Kolkata'));
          $current_date = $date->getTimestamp();
          $fileName =  $current_date . md5($filename).".".$extension;

          $stream = fopen($file->getRealPath(), 'r+');
          try
          {
           $return =  $filesystem->writeStream('/uploads/profile_pics/'.$fileName,$stream);

         }
         catch(Exception $e)
         {
           return $this->setStatusCode(404)->respondWithError('Already image exists');
         }
         $unique = ['user_id' => $this->user_id];
         $record['profile_pic'] = $fileName;
         $data = UsersProfile::updateOrCreate($unique, $record);

         return $data->profile_pic;
       }
     }

     public function updateSummary(Request $request)
     {
      try {

        $this->validate($request, [
          'about' => 'required',
        ]);

        if(UsersProfile::where('user_id', $this->user_id)->update(['about' => $request->about])){

          return response()->json([
            'success'
          ],Response::HTTP_OK
        );
        }

        return response()->json([
          'failed'
        ],Response::HTTP_INTERNAL_SERVER_ERROR
      );

      } catch (\Illuminate\Validation\ValidationException $e) {

        return validation_exception($e);

      } catch (\Exception $e) {

        return general_expection($e);
      }
    }

    /**
 * @api {get} api/profile/partner/view/{userId} Request User information
 * @apiName showPartnerProfile
 * @apiGroup Profile
 *
 * @apiParam {userId} userId Users unique ID.
 *
 * @apiSuccess {String} first_name Firstname of the User.
 * @apiSuccess {String} email  of the User.
 * @apiSuccess {String} mobile  of the User.
 * @apiSuccess {Object} partner_profile of User from partner_profile.
 */
    public function showPartnerProfile(Request $request) {
      try {
        $this->validate($request, [
          'userId' => 'required',
        ]);
        $data = PartnerProfile::select('partner_profile.*','users.first_name','users.email','users.mobile')->leftjoin('users','users.id','=','partner_profile.user_id')->where(['slug'=> $request->userId,'partner_profile.deleted_at'=>NULL])->get();
		//dd($data[0]);
        $data = isset($data[0])?$data[0]:null;
        if($data) {
          return response()->json([
            'data' => $data,
          ],Response::HTTP_OK
        );
        }  else {
          return response()->json([
            'status_code' => 4004,
            'message' => 'No data found',
          ],
          Response::HTTP_NOT_FOUND
        );
        }      
      }  catch (\Illuminate\Validation\ValidationException $e) {

        return validation_exception($e);

      } 
      catch (\Exception $e) {
       return general_expection($e);
     }
   }

    /**
     * @api {get} api/profile/users list users for admin
     * @apiName index
     * @apiGroup admin
     * 
     *
     * @apiSuccessExample Success-Response-2000:
     HTTP/1.1 2000 OK
       {
         "status_code": "2000",
        "message": "success",
        "body": {
        "current_page": 1,
        "data": [
            {
                "display_name": null,
                "email": "abc@gmail.in",
                "mobile": "212121212",
                "activation_date": null,
                "deactivation_date": null,
                "created_at": "2018-12-03 07:26:08",
                "updated_at": "2019-01-24 09:49:29",
                "role": "Administer",
                "username": "WEP RolE",
                "created_by": ""
            }
        ],
        "first_page_url": "http://localhost:8001/api/profile/users?page=1",
        "from": 1,
        "last_page": 2,
        "last_page_url": "http://localhost:8001/api/profile/users?page=2",
        "next_page_url": "http://localhost:8001/api/profile/users?page=2",
        "path": "http://localhost:8001/api/profile/users",
        "per_page": 10,
        "prev_page_url": null,
        "to": 10,
        "total": 11
        }
    }
  
    * @apiErrorExample Error-Response-4001:
    *   HTTP/1.1 4001 Unauthorized
    *   {
     "status_code": 4001,
     "message": "Unauthorized Accecss",
     "body": []
     }
    *
    * @apiErrorExample Error-Response-5000:
    *   HTTP/1.1 5000 Internal Server Error
    *      {
     "status_code": 5000,
     "message": "Internal Error, Try again later",
     "body": []
     }
     **/

     public function index(Request $request) {

      $limit = $request->limit? $request->limit:10;
      $type = $request->type;
      $today = date("Y-m-d");
      $userData = Users::Select('users.id','users.user_slug','users.display_name','users.email','users.mobile','users.activation_date','users.deactivation_date','users.created_at','users.updated_at',DB::raw("TRIM(CONCAT_WS(' ',users.first_name,users.last_name)) as first_name"),DB::raw("TRIM(CONCAT_WS(' ',userdata.first_name,userdata.last_name)) as created_by"))
      ->leftjoin('users as userdata','users.created_by','=','userdata.id')
      ->whereNull('users.deleted_at')
      ->where('users.user_type',$type)
      ->where(function ($userData) use ($today,$type) {
        $userData =   $userData->where('users.deactivation_date','>=',$today);
        $userData = $userData ->orWhere('users.deactivation_date','=',null);
      });

      if($request->has('keyword') && !empty($request->keyword)){
        $userData = $userData->where(function($userData) use ($request) {
          $userData->orWhere('users.email', 'LIKE', "%$request->keyword%");
          $userData->orWhere('users.display_name', 'LIKE', "%$request->keyword%");
          $userData->orWhere('users.mobile', 'LIKE', "%$request->keyword%");
          $userData->orWhere('users.first_name', 'LIKE', "%$request->keyword%");
        });
      }

      if($request->sort == 'name') {
        $data = ($request->order == 1)? $userData->orderBy('display_name') : $userData->orderBy('display_name','DESC');
      } else if($request->sort == 'date') {
        $data = ($request->order == 1) ? $userData->orderBy('updated_at') : $userData->orderBy('updated_at','DESC');
      } else {
        $data = $userData->orderBy('updated_at','DESC');
      }
      $data = $data->paginate($limit);
      if($data->count()==0) {
        return new JsonResponse(['message' => 'No Data found'], 200);
      }

      foreach($data as $value) {  
        $roles = DB::table('users_roles')->join('roles','users_roles.role_id','=','roles.id')->select('roles.id','roles.name')->where('users_roles.user_id',$value->id)->get();
        $value['role'] = $roles;          
      }
      return new JsonResponse($data);
    }

    /**
     * @api {get} api/profile/user/{id} detailed view of users for admin
     * @apiName show
     * @apiGroup admin
     *
     * @apiSuccessExample Success-Response-2000:
     HTTP/1.1 2000 OK
       {
         "status_code": "2000",
        "message": "success",
        "body": {
        "data": [
            {
                "display_name": null,
                "email": "abc@gmail.in",
                "mobile": "212121212",
                "activation_date": null,
                "deactivation_date": null,
                "created_at": "2018-12-03 07:26:08",
                "updated_at": "2019-01-24 09:49:29",
                "role": "Administer",
                "username": "WEP RolE",
                "created_by": ""
            }
        ]
    }
  
    * @apiErrorExample Error-Response-4001:
    *   HTTP/1.1 4001 Unauthorized
    *   {
     "status_code": 4001,
     "message": "Unauthorized Accecss",
     "body": []
     }
    *
    * @apiErrorExample Error-Response-5000:
    *   HTTP/1.1 5000 Internal Server Error
    *      {
     "status_code": 5000,
     "message": "Internal Error, Try again later",
     "body": []
     }

     **/
     public function show($slug) {
        //  $userData = Users::Select('users.id','users.display_name','users.created_by','users.email','users.mobile','users.activation_date','users.deactivation_date','users.created_at','users.updated_at',DB::raw("TRIM(CONCAT_WS(' ',users.first_name,users.last_name)) as first_name"),DB::raw("TRIM(CONCAT_WS(' ',userdata.first_name,userdata.last_name)) as created_by"))
        //     ->leftjoin('users as userdata','users.created_by','=','userdata.id')
        //     ->whereNotNull('users.activation_date')
        //     ->whereNull('users.deleted_at')
        //     ->where('users.id',$id)->get();

        // if($userData) {
        //     $roles = DB::table('users_roles')->join('roles','users_roles.role_id','=','roles.id')->select('roles.id','roles.name')->where('users_roles.user_id',$userData->id)->get();
        //     $userData['role'] = $roles;          
        // }
        // return new JsonResponse($userData);
      $data = Users::with('roles.permissions')->where('users.user_slug',$slug)->first();
        // foreach($data['roles'] as $value) {
        //     foreach($value['permissions'] as $val) {
        //         $val['parent_module_id'] = Module::with('parent')->where('id',$val['module_id'])->get();
        //     }
        // }
      return $data;
    }


    /**
     * @api {post} api/profile/users store user
     * @apiName store
     * @apiGroup admin
     *
     * @apiParam {String} first_name to add in user
     * @apiParam {String} display_name role of the user 
     * @apiParam {String} mobile  of the user.
     * @apiParam {String} email  of the user.
     * @apiParam {date} activation_date  of the user.
     * @apiParam {date} deactivation_date  of the user.
     * 
     * @apiSuccess {String} first_name to add in user
     * @apiSuccess {String} display_name role of the user 
     * @apiSuccess {String} mobile  of the user.
     * @apiSuccess {String} email  of the user.
     * @apiSuccess {date} activation_date  of the user.
     * @apiSuccess {date} deactivation_date  of the user.
     * @apiSuccess {String}  Updated_by of the user.
     */
    public function store(Request $request) {
      try {
        $request['mobile']=$request->mobile?base64_decode($request->mobile):'';
        $request['email']=$request->email?base64_decode($request->email):'';

        $this->validate($request,[
          'first_name' => 'required',
          'display_name'   => 'required',
          'mobile' => 'required|min:10|max:12|unique:users,mobile',
          'email' => 'required|email|unique:users,email',
          'activation_date' => 'date',
          'deactivation_date' => ["date",new DeactivateDate($request->get('activation_date'))],
        ]);
        $data = $request->toArray();
            //$password = 'choice@123';
        $password = $this->generatePassword();
        $data['user_type'] = $request->user_type;
        $data['created_by'] = $request->user_id;
        $data['activation_date'] = isset($data['activation_date'])?date('Y-m-d', strtotime($data['activation_date'])):NULL;
        $data['deactivation_date'] =  isset($data['deactivation_date'])?date('Y-m-d', strtotime($data['deactivation_date'])):NULL;
        $data['approval_status'] = "approved";
        $data['password'] = Hash::make(sha1($password));
        $userData = new Users($data);
            // HERE: additional, non-fillable fields

        $userData->save();
        if($userData)
          if(isset($request->role)) {
            $userData->giveRolesTo($request->role);
          }
          try {
            $mailSubject = "Set your Password on NITI Aayog WEP Portal";
            Mail::send('Mail.setPassword', [
              'name' => ucwords($userData->first_name . " " . $userData->last_name),
              'password' => $password,
              'email' => $userData->email
            ], function($message) use($userData, $mailSubject){
              $message->to($userData->email)->subject($mailSubject);
            });
          } catch(Exception $e) {
            $userData->forceDelete();
            return new JsonResponse([], 200);
          }
          return new JsonResponse([], 200);

          return new JsonResponse(['message' => 'Server Error'], 500);
        } catch (\Illuminate\Validation\ValidationException $e) {

          return validation_exception($e);
        } catch (\Exception $e) {
          return general_expection($e);
        }
      }

    /**
     * @api {delete} api/profile/user/{id} delete user
     * @apiName destroy
     * @apiGroup admin
     *
     * @apiParam {Integer} id  of user
     * 
     * @apiSuccess {String} Success
     */
    public function destroy(Request $request, $slug) {
      try {
        $userData = Users::with('roles')->where('user_slug',$slug)->first();
        
        if($userData === null)
          return new JsonResponse(['message' => 'Not found'], 404);
          // $userData->withdrawlRolesTo($userData->roles->pluck('id'));

        $mailSubject = "You are deleted on NITI Aayog WEP Portal";
        try {
          Mail::send('Mail.deleteUser', [
            'name' => ucwords($userData->first_name . " " . $userData->last_name),
          ], function($message) use($userData, $mailSubject){
            $message->to($userData->email)->subject($mailSubject);
          });
        } catch(Exception $e) {
          $userData->delete();
          return new JsonResponse(['message'=>'Deleted Sucessfully without mail'], 200);
        }

        $userData->delete();

        return new JsonResponse(['message'=>'Deleted Sucessfully'], 200);
      } catch (\Exception $e) {
        return general_expection($e);
      }
    }

    /**
     * @api {patch} api/profile/user/{id} update user
     * @apiName updateUser
     * @apiGroup admin
     *
     * @apiParam {Integer} id  of user
     * @apiParam {Array} parameter to be updated
     * 
     * @apiSuccessExample Success-Response-2000:
     HTTP/1.1 2000 OK
       {
         "status_code": "2000",
        "message": "success",
        "body": {
        "current_page": 1,
        "data": [
            {
                "display_name": null,
                "email": "abc@gmail.in",
                "mobile": "212121212",
                "activation_date": null,
                "deactivation_date": null,
                "created_at": "2018-12-03 07:26:08",
                "updated_at": "2019-01-24 09:49:29",
                "role": "Administer",
                "username": "WEP RolE",
                "created_by": ""
            }
        ],
         }

    * @apiErrorExample Error-Response-4001:
    *   HTTP/1.1 4001 Unauthorized
    *   {
     "status_code": 4001,
     "message": "Unauthorized Accecss",
     "body": []
     }
    *
    * @apiErrorExample Error-Response-5000:
    *   HTTP/1.1 5000 Internal Server Error
    *      {
     "status_code": 5000,
     "message": "Internal Error, Try again later",
     "body": []
     }
     */
     public function updateUser($slug, Request $request) {
      try {
        $userData = UsersForDeactivatedList::where('user_slug',$slug)->first();
        $role = $request->role;

        if($userData == null)
          return new JsonResponse(['message' => 'Not found'], 404);

        $request['mobile']=$request->mobile?base64_decode($request->mobile):'';
        $request['email']=$request->email?base64_decode($request->email):'';

        $previous_role = $userData->user_type;
        if($userData->user_type != $request->user_type){
          $data = $request->toArray();
          $userData->update($data);
          $mailSubject = "Change of Role  on NITI Aayog WEP Portal";
          try {
            Mail::send('Mail.update-role', [
              'name' => ucwords($userData->first_name . " " . $userData->last_name),
              'updated_role'=> $request->user_type,
              'previous_role'=> $previous_role
            ], function($message) use($userData,$mailSubject){
              $message->to($userData->email)->subject($mailSubject);
            });
          } catch(Exception $e) {
            return $e->getMessage();
          }
          unset($previous_role);
        } else {
          $data = $request->toArray();
          $userData->update($data);

        }

        if($request->role) {
          $userData->giveRolesTo($request->role);
        }
        return new JsonResponse(['message' => 'Success'], 200);
      } catch (\Exception $e) {
        return general_expection($e);
      }
    }

    /**
     * @api {get} api/profile/user/search/{user} Search user
     * @apiName search
     * @apiGroup admin
     *
     * @apiParam {String} user  parameter
     * 
    * @apiSuccessExample Success-Response-2000:
     HTTP/1.1 2000 OK
       {
         "status_code": "2000",
        "message": "success",
        "body": {
        "current_page": 1,
        "data": [
            {
                "display_name": null,
                "email": "abc@gmail.in",
                "mobile": "212121212",
                "activation_date": null,
                "deactivation_date": null,
                "created_at": "2018-12-03 07:26:08",
                "updated_at": "2019-01-24 09:49:29",
                "role": "Administer",
                "username": "WEP RolE",
                "created_by": ""
            }
        ],
        "first_page_url": "http://localhost:8001/api/profile/users?page=1",
        "from": 1,
        "last_page": 2,
        "last_page_url": "http://localhost:8001/api/profile/users?page=2",
        "next_page_url": "http://localhost:8001/api/profile/users?page=2",
        "path": "http://localhost:8001/api/profile/users",
        "per_page": 10,
        "prev_page_url": null,
        "to": 10,
        "total": 11
        }
    }
  
    * @apiErrorExample Error-Response-4001:
    *   HTTP/1.1 4001 Unauthorized
    *   {
     "status_code": 4001,
     "message": "Unauthorized Accecss",
     "body": []
     }
    *
    * @apiErrorExample Error-Response-5000:
    *   HTTP/1.1 5000 Internal Server Error
    *      {
     "status_code": 5000,
     "message": "Internal Error, Try again later",
     "body": []
     }
     */
     public function search(Request $request, $user) {

      $limit = $request->limit?$request->limit:10;
      if(isset($request->deleted)) {
        $data = Users::Select('users.id','users.display_name','users.created_by','users.email','users.mobile','users.activation_date','users.deactivation_date','users.created_at','users.updated_at',DB::raw("TRIM(CONCAT_WS(' ',users.first_name,users.last_name)) as first_name"),DB::raw("TRIM(CONCAT_WS(' ',userdata.first_name,userdata.last_name)) as created_by"))
        ->leftjoin('users as userdata','users.created_by','=','userdata.id')
        ->whereNotNull('users.deleted_at')
        ->where('users.user_type',"admin")
        ->where('users.first_name','like','%'.$user.'%')
        ->orWhere('users.display_name','like','%'.$user.'%')
        ->withTrashed()
        ->paginate($limit);
      } else {
        $data = Users::Select('users.id','users.display_name','users.created_by','users.email','users.mobile','users.activation_date','users.deactivation_date','users.created_at','users.updated_at',DB::raw("TRIM(CONCAT_WS(' ',users.first_name,users.last_name)) as first_name"),DB::raw("TRIM(CONCAT_WS(' ',userdata.first_name,userdata.last_name)) as created_by"))
        ->leftjoin('users as userdata','users.created_by','=','userdata.id')
        ->whereNull('users.deleted_at')
        ->where('users.user_type',"admin")
        ->where('users.first_name','like','%'.$user.'%')
        ->orWhere('users.display_name','like','%'.$user.'%')
        ->paginate($limit);
      }
      if(count($data) == 0)
        return new JsonResponse(['message' => 'Not found'], 200);

      foreach($data as $value) {  
        $roles = DB::table('users_roles')->select('roles.id','roles.name')->join('roles','users_roles.role_id','=','roles.id')->where('users_roles.user_id',$value->id)->get();
        $value['role'] = $roles;          
      }
      return new JsonResponse($data);

    }
    
    public function partnerListLoginPage() {
      return PartnerProfile::select('users.email','users.mobile','users.approval_status','users.first_name as partner_name', 'partner_profile.slug', 'partner_profile.logo')       ->join('users','partner_profile.user_id','=','users.id')
      ->where(['users.user_type'=>'partner'])->get();
    }

     /*
  * @api {GET} api/profile/partner/{slug}- Get Partner detail
  * @apiName singlePartnerProfile Get Partner Detail
  * @apiGroup Partner
  * @apiDescription This api is used to get detail of partner registred in the WEP Website.
  * @apiSuccessExample Success-Response-2000:
   HTTP/1.1 2000 OK
     {
       "status_code": 2000,
       "message": "Success",
       "body": {
         "total_count": 20
       }
   }

  *
  * @apiErrorExample Error-Response-5000:
  *   HTTP/1.1 5000 Internal Server Error
  *      {
   "status_code": 5000,
   "message": "Internal Error, Try again later",
   "body": []
   }
  */
   public function singlePartnerProfile(Request $request,$slug) {
    try {
      $user_id = PartnerProfile::where('slug',$slug)->join('users','users.id','=','partner_profile.user_id')
      ->where('users.deleted_at',null)->first();


      if($user_id){
        $user_id = $user_id->user_id; 
      }else{
       return new JsonResponse([],404);
     }

     if($user_id!=NULL){
      $select = [
        'users.first_name as partner_name',
        'partner_profile.slug',
        'partner_profile.office_address_1',
        'partner_profile.office_address_2',
        'partner_profile.city',
        'partner_profile.state',
        'partner_profile.country',
        'partner_profile.postal_code',
        'partner_profile.logo',
        'partner_profile.website_url',
        'partner_profile.linkedin_url',
        'partner_profile.facebook_url',
        'partner_profile.area_of_business',
        'partner_profile.type_of_establishment',
        'partner_profile.company_description',
        'partner_profile.cxo_name',
        'partner_profile.cxo_qualification',
        'partner_profile.cxo_designation',
        'partner_profile.cxo_contact',
        'partner_profile.cxo_linkedin_url',
        'partner_profile.contact_person_name',
        'partner_profile.collaboration_model_description',
        'partner_profile.proposed_deliverables_description',
        'partner_profile.proposed_deliverables_benefit',
        'partner_profile.expectations',
        'partner_profile.about_wep_description',
        'partner_profile.created_by',
        'partner_profile.updated_by',
      ];

      $data['userProfile'] = PartnerProfile::select($select)
      ->join('users','partner_profile.user_id','=','users.id')
      ->where(['partner_profile.user_id'=>$user_id])
      ->get();
      
      $data['partner_services'] = $this->getServiceList($request,$user_id);
      $data['connected_entrepreneurs'] = Users::select('users.first_name','users.last_name','users.user_slug','user_profile.profile_pic','user_profile.entreprise_name')
      ->leftjoin('user_mentor_partner_connection_mapper','user_mentor_partner_connection_mapper.user_id','=','users.id')
      ->leftjoin('user_profile','user_profile.user_id','=','users.id')
      ->where(['user_mentor_partner_connection_mapper.connected_to'=>$user_id,'users.user_type'=>'entrepreneur','user_profile.deleted_at'=>NULL])
      ->where('user_mentor_partner_connection_mapper.connection_status',1)
      ->get();
      $loggedInUserStatus = UserMentorPartnerConnectionMapper::where(['user_id'=>$this->user_id,'connected_to'=>$user_id,'connection_status'=>1])->count();
      $data['loggedInUserStatus'] = $loggedInUserStatus?true:false;

      return response()->json([
        'data' => $data
      ],
      Response::HTTP_OK
    );
    }
  } catch (Exception $e) {
    return response()->json(['msg'=>$this->exceptionResponse($e)]);
  }

}

public function restore($slug) {

   try {
       Users::withTrashed()->with('roles')->where('user_slug',$slug)->restore();
         $userData =  Users::with('roles')->where('user_slug',$slug)->first();

        
        if($userData === null)
          return new JsonResponse(['message' => 'Not found'], 404);
        

        $mailSubject = "You are Activated on NITI Aayog WEP Portal";
        try {
          Mail::send('Mail.activate-user', [
            'name' => ucwords($userData->first_name . " " . $userData->last_name),
          ], function($message) use($userData, $mailSubject){
            $message->to($userData->email)->subject($mailSubject);
          });
        } catch(Exception $e) {
          $userData->delete();
          return new JsonResponse(['message'=>'Activated Sucessfully without mail'], 200);
        }

        return new JsonResponse(['message'=>'Activated Sucessfully'], 200);
      } catch (\Exception $e) {
        return general_expection($e);
      }

}

    /**
     * @api {get} api/profile/users list users for admin
     * @apiName deactivatelist
     * @apiGroup admin
     * 
     *
     * @apiSuccessExample Success-Response-2000:
     HTTP/1.1 2000 OK
       {
         "status_code": "2000",
        "message": "success",
        "body": {
        "current_page": 1,
        "data": [
            {
                "display_name": null,
                "email": "abc@gmail.in",
                "mobile": "212121212",
                "activation_date": null,
                "deactivation_date": null,
                "created_at": "2018-12-03 07:26:08",
                "updated_at": "2019-01-24 09:49:29",
                "role": "Administer",
                "username": "WEP RolE",
                "created_by": ""
            }
        ],
        "first_page_url": "http://localhost:8001/api/profile/users?page=1",
        "from": 1,
        "last_page": 2,
        "last_page_url": "http://localhost:8001/api/profile/users?page=2",
        "next_page_url": "http://localhost:8001/api/profile/users?page=2",
        "path": "http://localhost:8001/api/profile/users",
        "per_page": 10,
        "prev_page_url": null,
        "to": 10,
        "total": 11
        }
    }
  
    * @apiErrorExample Error-Response-4001:
    *   HTTP/1.1 4001 Unauthorized
    *   {
     "status_code": 4001,
     "message": "Unauthorized Accecss",
     "body": []
     }
    *
    * @apiErrorExample Error-Response-5000:
    *   HTTP/1.1 5000 Internal Server Error
    *      {
     "status_code": 5000,
     "message": "Internal Error, Try again later",
     "body": []
     }
     **/

     public function deactivateUserList(Request $request) {

      $limit = $request->limit? $request->limit:10;
      $today = date("Y-m-d");
      $type = $request->type;

      $userData = UsersForDeactivatedList::Select('users.id','users.user_slug','users.display_name','users.email','users.mobile','users.activation_date','users.deactivation_date','users.created_at','users.updated_at',DB::raw("TRIM(CONCAT_WS(' ',users.first_name,users.last_name)) as first_name"),DB::raw("TRIM(CONCAT_WS(' ',userdata.first_name,userdata.last_name)) as created_by"))
      ->leftjoin('users as userdata','users.created_by','=','userdata.id')
      ->where('users.user_type',$request->type);

      if($type=='admin'){
        $userData = $userData->whereNotNull('users.deleted_at');
      }else{
       $userData = $userData->where('users.user_type',$request->type);
       $userData = $userData->where(function($userData)
       {
        $today = date("Y-m-d");  
        $userData->where('users.deactivation_date','<',$today);
        $userData->orWhereNotNull('users.deleted_at');
      });




     }
     
     if($request->sort == 'name') {
      $data = ($request->order == 1)? $userData->orderBy('display_name') : $userData->orderBy('display_name','DESC');
    } else if($request->sort == 'date') {
      $data = ($request->order == 1) ? $userData->orderBy('updated_at') : $userData->orderBy('updated_at','DESC');
    } else {
      $data = $userData->orderBy('updated_at','DESC');
    }

    if($request->has('keyword') && !empty($request->keyword)){
      $data = $userData->where(function($userData) use ($request) {
        $userData->orWhere('users.email', 'LIKE', "%$request->keyword%");
        $userData->orWhere('users.display_name', 'LIKE', "%$request->keyword%");
        $userData->orWhere('users.mobile', 'LIKE', "%$request->keyword%");
        $userData->orWhere('users.first_name', 'LIKE', "%$request->keyword%");
      });
    }
    
    $data = $data->paginate($limit);


    if($data->count()==0) {
      return new JsonResponse(['message' => 'No Data found'], 200);
    }

    foreach($data as $value) {  
      $roles = DB::table('users_roles')->join('roles','users_roles.role_id','=','roles.id')->select('roles.id','roles.name')->where('users_roles.user_id',$value->id)->get();
      $value['role'] = $roles;          
    }

    return new JsonResponse($data);
  }


  public function suggestedMentor(Request $request,$search=null) {
    $user_id = $this->user_id;
    $limit = $request->limit?$request->limit:10;
    $data['suggested_mentors'] = EnterpreneurInterest::select(DB::Raw('DISTINCT(users.user_slug) as slug'),'users.first_name','users.last_name','user_profile.profile_pic','users.updated_at')
    ->leftjoin('users','users.id','=','entrepreneur_interest_area.user_id')
    ->leftjoin('user_profile','user_profile.user_id','=','users.id')
    ->whereIn('interest_area_id', function($query) use ($user_id)
    {

      $query->select(\DB::raw('interest_area_id'))
      ->from('entrepreneur_interest_area')
      ->whereRaw('entrepreneur_interest_area.deleted_at is null')
      ->whereRaw('user_id ='.$user_id);
    })
        // ->leftjoin('user_mentor_partner_connection_mapper','user_mentor_partner_connection_mapper.connected_to','=','users.id')
        // ->where('user_mentor_partner_connection_mapper.connection_status', 0)
    ->where('users.mentorship_status','=','1')
    ->where('users.id','!=',$user_id)
    ->whereNotIn('users.id',function($query) use ($user_id){
     $query->select(\DB::raw('connected_to'))
     ->from('user_mentor_partner_connection_mapper')
     ->where('user_id',$user_id)
     ->where('connection_status',1)
     ->where('deleted_at',null);
   });

    if(isset($request->search)) {
     return  $data['suggested_mentors']
     ->where('users.first_name','like','%'.$request->search.'%')
     ->orWhere('users.last_name','like','%'.$request->search.'%')
     ->orderBy('users.updated_at')
     ->paginate($limit); 
   }
   return  $data['suggested_mentors']->orderBy('users.updated_at')->paginate($limit);
 }

 public function suggestedPartner(Request $request,$search=null) {
  $user_id = $this->user_id;
  $limit = $request->limit?$request->limit:10;
  $data['suggested_partners'] = PartnerServices::select(DB::raw('DISTINCT(partner_profile.slug) as slug'),'users.first_name','users.last_name','partner_profile.logo','users.updated_at')
  ->leftjoin('users','users.id','=','partner_services.user_id')
  ->leftjoin('user_profile','user_profile.user_id','=','users.id')
  ->leftjoin('partner_profile','partner_profile.user_id','=','users.id')
  ->leftjoin('user_mentor_partner_connection_mapper','user_mentor_partner_connection_mapper.connected_to','=','users.id')
  ->join('service_category_mapper','service_category_mapper.service_id','=','partner_services.id')
  ->whereIn('service_category_mapper.category_id', function($query) use ($user_id)
  {
    $query->select(\DB::raw('interest_area_id'))
    ->from('entrepreneur_interest_area')
    ->whereRaw('entrepreneur_interest_area.deleted_at is null')
    ->whereRaw('user_id ='.$user_id);
  })
  ->whereNotIn('users.id',function($query) use ($user_id){
   $query->select(\DB::raw('connected_to'))
   ->from('user_mentor_partner_connection_mapper')
   ->where('user_id',$user_id)
   ->where('connection_status',1)
   ->where('deleted_at',null);
 })
  ->where('user_mentor_partner_connection_mapper.connection_status', "0");
  if(isset($request->search)) {
   return  $data['suggested_partners']->where('users.first_name','like','%'.$request->search.'%')->orderBy('users.updated_at')->paginate($limit); 
 }
 return  $data['suggested_partners']->orderBy('users.updated_at')->paginate($limit);
}

public function exportEntreprenaurList(Request $request) {

  $data = Users::select('users.first_name', 
    'users.last_name', 
    'users.email', 
    'users.user_type', 
    'users.user_code', 
    'users.user_slug', 
    'users.mobile',
    DB::raw('group_concat(DISTINCT category_name SEPARATOR "|")  as area_of_interest'),
    DB::raw('
      CASE
      WHEN user_profile.state>0
      THEN state.state_name
      ELSE user_profile.state
      END AS state_name'),DB::raw('
      CASE
      WHEN user_profile.city>0
      THEN city.city_name
      ELSE user_profile.city
      END AS city_name'), 

      'entrepreneur_business_info.sector_of_entreprise',
      'entrepreneur_business_info.type_of_enterprise',
      'entrepreneur_business_info.stage_of_business',
      'users_qualification.area_of_specialisation as area_of_specialization','users.created_at as date_of_registration')
  ->leftjoin('user_profile','user_profile.user_id','=','users.id')
  ->leftjoin('entrepreneur_interest_area','users.id','=','entrepreneur_interest_area.user_id')
  ->leftjoin('categories','entrepreneur_interest_area.interest_area_id','=','categories.id')
  ->leftjoin('entrepreneur_business_info','entrepreneur_business_info.user_id','=','users.id')
  ->leftjoin('users_qualification','users_qualification.user_id','=','users.id')
  ->leftjoin('state','user_profile.state','=','state.id')
  ->leftjoin('city','user_profile.city','=','city.id')
  ->where('entrepreneur_interest_area.deleted_at',null)
  ->where(['users.user_type'=>'entrepreneur'])
  ->groupBy('users.id');

  $data = $data->orderBy('users.id','desc')->get();


  return $data;
}
protected function generatePassword($lenght = 8)
{
  $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';

  $pass = []; 
  $alphaLength = strlen($alphabet) - 1; 

  for ($i = 0; $i < $lenght; $i++) {
    $n = rand(0, $alphaLength);
    $pass[] = $alphabet[$n];
  }

  return implode($pass);
}


}