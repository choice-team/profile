<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\offer;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class offersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {

        $offers = $this->getOffersList($request); 

        return view('admin.offers.index', compact('offers'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function indexApi(Request $request)
    {

        try {
 
            $record = $this->getOffersList($request); 

            return response()->json([
            'status_code' => 2000,
            'message' => 'Success',
            'body' => [
              // 'total_count' => $perPage,
              'data_count' => count($record),
              'data' => $record
            ]
          ],
            Response::HTTP_OK
          );

        } catch (Exception $e) {
            echo $this->exceptionResponse($e);
        }
        
    }

    public function getOffersList($request)
    {
        $perPage =  10;
        $keyword = $request->get('search');

        if (!empty($keyword)) {
            $offers = offer::where('offer_name', 'LIKE', "%$keyword%")
                ->orWhere('title', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $offers = offer::latest()->paginate($perPage);
        }

        return $offers;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.offers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'offer_name' => 'required|string|unique:offers,offer_name,NULL,id,deleted_at,NULL',
			'title' => 'required|string|unique:offers,title,NULL,id,deleted_at,NULL'
		]);

        try {

            $result = $this->storeOffer($request); 

            if ($result) {
                return redirect('admin/offers')->with('flash_message', 'offer added!');
            } else {
                return redirect('admin/offers')->with('danger', 'something went wrong while adding data!');
            }

        } catch (Exception $e) {

            echo $this->exceptionResponse($e);
        }  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function storeApi(Request $request)
    {
        try {
            
            $this->validate($request, [
                'offer_name' => 'required|string|unique:offers,offer_name,NULL,id,deleted_at,NULL',
                'title' => 'required|string|unique:offers,title,NULL,id,deleted_at,NULL'
            ]);

            $result = $this->storeOffer($request);  

            return response()->json([
            'status_code' => 2000,
            'message' => 'Success',
            'body' => [
              'id' => $result->id
            ]
          ],
            Response::HTTP_OK
          );

        } catch (\Illuminate\Validation\ValidationException $exception) {
            // $error = $this->getErrorString($e);
            $error = $exception->getMessage();
            // Log::error($error);
            return response()->json([
                'status_code' => 4000,
                'message' => $error,
                'body' => ['id' => null]
            ],
                Response::HTTP_BAD_REQUEST
            );

        } catch (Exception $e) {

            echo $this->exceptionResponse($e);
        }
        
    }

    public function storeOffer(Request $request)
    {
        
        $requestData = $request->all();
        
        return offer::create($requestData);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $offer = offer::findOrFail($id);

        return view('admin.offers.show', compact('offer'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function showApi($id)
    {
        try {
 
            $record = $this->getServiceListById($id); 

            return response()->json([
            'status_code' => 2000,
            'message' => 'Success',
            'body' => [
                $record
            ]
          ],
            Response::HTTP_OK
          );

        } catch (Exception $e) {
            echo $this->exceptionResponse($e);
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $offer =  $this->getOffersListById($id); 

        return view('admin.offers.edit', compact('offer'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function editApi($id)
    {
        try {
 
            $record = $this->getOffersListById($id); 

            return response()->json([
            'status_code' => 2000,
            'message' => 'Success',
            'body' => [
                $record
            ]
          ],
            Response::HTTP_OK
          );

        } catch (Exception $e) {
            echo $this->exceptionResponse($e);
        }
        
    }

    public function getOffersListById($id)
    {

        $offer = offer::findOrFail($id);

        return $offer;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'offer_name' => 'required|string|unique:offers,offer_name,'.$id.',id,deleted_at,NULL',
			'title' => 'required|string|unique:offers,title,'.$id.',id,deleted_at,NULL'
		]);
        
        $result = $this->updateOffer($request,$id); 

        if ($result) {
            return redirect('admin/offers')->with('flash_message', 'offer updated!');
        } else {
            return redirect('admin/offers')->with('danger', 'something went wrong while updating data!');
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateApi(Request $request,$id)
    {
        try {
            
            $this->validate($request, [
                'offer_name' => 'required|string|unique:offers,offer_name,'.$id.',id,deleted_at,NULL',
                'title' => 'required|string|unique:offers,title,'.$id.',id,deleted_at,NULL'
            ]);

            $result = $this->updateOffer($request,$id);  

            return response()->json([
            'status_code' => 2000,
            'message' => 'Success',
            'body' => [
              'id' => $result->id
            ]
          ],
            Response::HTTP_OK
          );

        } catch (\Illuminate\Validation\ValidationException $e) {

            // $error = $this->getErrorString($e);
            $error = $e->getMessage();
            // Log::error($error);

            return response()->json([
                'status_code' => 4000,
                'message' => $error,
                'body' => ['id' => null]
            ],
                Response::HTTP_BAD_REQUEST
            );

        } catch (Exception $e) {

            echo $this->exceptionResponse($e);
        }
        
    }

    public function updateOffer($request,$id)
    {
        $requestData = $request->all();
        
        $offer = offer::findOrFail($id);
        $offer->update($requestData);

        return $offer;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $result = $this->destroyOffer($id); 

        if ($result) {
            return redirect('admin/offers')->with('flash_message', 'offer deleted!');
        } else {
            return redirect('admin/offers')->with('danger', 'something went wrong while deleting data!');
        }

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroyApi($id)
    {
        try {

            $result = $this->destroyOffer($id);  

            return response()->json([
            'status_code' => 2000,
            'message' => 'Success',
            'body' => []
          ],
            Response::HTTP_OK
          );

        } catch (\Illuminate\Validation\ValidationException $e) {

            // $error = $this->getErrorString($e);
            $error = $e->getMessage();
            // Log::error($error);

            return response()->json([
                'status_code' => 4000,
                'message' => $error,
                'body' => []
            ],
                Response::HTTP_BAD_REQUEST
            );

        } catch (Exception $e) {

            echo $this->exceptionResponse($e);
        }
    }

    public function destroyOffer($id)
    {
       return  offer::destroy($id);
    }
}
