<?php
namespace App\Http\Controllers;

use App\Users;
use Illuminate\Http\Request;
use App\FormData;
use App\Forms;
use App\SaveFormData;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\JsonResponse;
use League\Flysystem\FilesystemInterface;


class FormBuilderController extends Controller
{

	public function getFormList(Request $request) {

		$limit = $request->limit? $request->limit:10;
		$data = Forms::select('form_name','form_slug',DB::raw('count(dynamic_form_data.form_id) as number_of_forms'),'dynamic_forms.created_at')
		->leftjoin('dynamic_form_data','dynamic_form_data.form_id','=','dynamic_forms.id')
		->groupBy('dynamic_forms.id');

		if($request->sort == 'form_name') {
			$data = ($request->order == 1)? $data->orderBy('form_name') : $data->orderBy('form_name','DESC');
		} 
		if($request->sort == 'updated_at') {
			$data = ($request->order == 1)? $data->orderBy('updated_at') : $data->orderBy('updated_at','DESC');
		} 
		if(isset($request->search) && !empty($request->search)) {
			$data = $data->where('form_name','like','%'.$request->search.'%');
		}

		$data = $data->paginate($limit);
		if($data->count()==0) {
			return new JsonResponse(['message' => 'No Data found'], 200);
		}
		return new JsonResponse($data);
	}

	//save form structure
	public function saveNewlyCreatedForm(Request $request) {


	 	$form_id = Forms::insertGetId([
	 		'form_name' => $request->formTitle,
	 		// 'form_slug' => strtolower(str_replace(' ','-',$request->formTitle))
	 		'form_slug' => str_random(12)
	 	]);
	 
	 	if(!empty($form_id)){
	 		$save_form =  FormData::create([
	 		'form_id' => $form_id,
	 		'form_data' => json_encode($request->json()->all())
	 		]);
	 	}
	 	return new JsonResponse(['message' => 'Form Created Successfully!!'], 200);
	}

	//save form and data 
	public function saveFormData(Request $request) {

		//fetch slug of already saved form and fetch id
		if($request->has('formTitle')){
			$slug = Forms::select('id')->where('form_slug',$request->formTitle)->first();
		} else {
			return new JsonResponse(['message' => 'Invalid Json Key'], 200);
		} 
		//save form_id and received json body with form filled details
		if(!empty($slug->id)) {
			$data = SaveFormData::create([
	 		'form_id' => $slug->id,
	 		'form_details' => json_encode($request->json()->all())
	 		]);
		} else {
			return new JsonResponse(['message' => 'No Data found'], 200);
		}

		if($data){
			return new JsonResponse(['message' => 'Form Data Saved Successfully!!'], 200);
		} else {
			return new JsonResponse(['message' => 'Something went wrong!!'], 500);
		}		
	}


	public function getFormFields(Request $request) {

		if($request->slug){
			$slug = Forms::select('id')->where('form_slug',$request->slug)->first();
		} else {
			return new JsonResponse(['message' => 'Invalid Json Key'], 200);
		} 

		if(empty($slug))
		return new JsonResponse(['message' => 'Slug Not found'], 200);	

		$form_fields =  FormData::select('form_data')->where('form_id',$slug->id)->get();

		if($form_fields){
			return $form_fields;
		} else {
			return new JsonResponse(['message' => 'No Data found'], 200);
		}
	}

	public function getAllForms(Request $request) {
		if($request->slug){
			$slug = Forms::select('id')->where('form_slug',$request->slug)->first();
		} else {
			return new JsonResponse(['message' => 'Invalid Json Key'], 200);
		}
	
		if(empty($slug))
		return new JsonResponse(['message' => 'Slug Not found'], 200);		

		$form_fields =  SaveFormData::select('form_id','form_details')
		->where('form_id',$slug->id)
		->paginate();

		return $form_fields; 
	}

	public function updateFormFields(Request $request) {

		if($request->slug){
			$slug = Forms::select('id')->where('form_slug',$request->slug)->first();
		} else {
			return new JsonResponse(['message' => 'Invalid Json Key'], 200);
		}

		if(empty($slug))
		return new JsonResponse(['message' => 'Slug Not found'], 200);

		Forms::where(['id' => $slug->id])->update([
			'form_name'=> json_encode($request->formTitle)
		]);

		$update = FormData::where(['form_id' => $slug->id])
		->update(['form_data'=> json_encode($request->json()->all())]);

		if($update){
			return new JsonResponse(['message' => 'Form Fields Updated Successfully!!'], 200);
		} else {
			return new JsonResponse(['message' => 'Something went wrong!!'], 500);
		}
	}

	public function deleteForm(Request $request){
		if($request->slug){
			$slug = Forms::select('id')->where('form_slug',$request->slug)->first();
		} else {
			return new JsonResponse(['message' => 'Invalid Json Key'], 200);
		}
		
		if(empty($slug))
		return new JsonResponse(['message' => 'Slug Not found'], 200);	

		$data['forms'] = Forms::where('id',$slug->id)->delete();
		$data['form_fields'] = FormData::where('form_id',$slug->id)->delete();
		$data['form_data'] = SaveFormData::where('form_id',$slug->id)->delete();

		return $data;
	}

	public function uploadFile(Request $request,FilesystemInterface $filesystem){
	
		if($request->hasfile('banner') && !empty($request->hasfile('banner'))) {
                $data['banner'] = store_file($request->file('banner'), $filesystem );
         } else {
         	return new JsonResponse(['message' => 'Please Upload file!!'], 200);
         }
          return $data;
	}


}