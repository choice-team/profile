<?php
namespace App\Http\Controllers;

use App\Users;
use App\Countries;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Spatie\Activitylog\Models\Activity;


class CountriesController extends Controller
{
    public function index(Request $request) {
        try {
            $limit = $request->limit? $request->limit:10;
            
            $country = Countries::with('state')->select('countries.name','countries.updated_at','countries.id',DB::raw("TRIM(CONCAT_WS(' ',u.first_name,u.last_name)) as updated_by"),'countries.deleted_at')
            ->leftjoin('users as u','u.id','=','countries.updated_by');

            if($request->sort == 'name') {
                $data = ($request->order == 1)? $country->orderBy('name') : $country->orderBy('name','DESC');
            } else if($request->sort == 'date') {
                $data = ($request->order == 1) ? $country->orderBy('updated_at') : $country->orderBy('updated_at','DESC');
            } else {
                $data = $country->orderBy('updated_at','DESC');
            }

            $search = str_replace("%20"," ",$request->keyword);
            if(isset($search) && !empty($search))
            {
                $data = $country->where('name','like',$search.'%');
            }
            
            $data = isset($request->flag)?$data->withTrashed()->get():$data->withTrashed()->paginate($limit);
            if($data->count()==0) {
                return new JsonResponse(['message' => 'No Data found'], 200);
            } 
            
            return new JsonResponse($data);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

    public function show($id) {
        try {
            $country = Countries::withTrashed()->find($id);
            if($country === null)
                return new JsonResponse(['message' => 'No Data found'], 200);

            return new JsonResponse($country);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

    public function store(Request $request) {
        try {
            $this->validate($request,[
                'name'   => 'required|unique:countries'
            ]);


            $data = $request->toArray();
            $data['updated_by'] = $request->user_id;
            $country = new Countries($data);

            // HERE: additional, non-fillable fields

            $country->save();

            if($country)
                return new JsonResponse([], 200);

            return new JsonResponse(['message' => 'Server Error'], 500);
        } catch (\Illuminate\Validation\ValidationException $e) {
            return validation_exception($e);
        } catch (\Exception $e) {
            Log::info($e);
            return general_expection($e);
        }
    }

    public function destroy(Request $request, $id) {
        try {
            $country = Countries::find($id);
            $country['updated_by'] = $request->user_id;
            $country->save();
            if($country === null)
                return new JsonResponse(['message' => 'Not found'], 404);

            $country['updated_by'] = $request->user_id;
            $country->save();
            $country->delete();

            return new JsonResponse(['message'=>'Deleted Sucessfully'], 200);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

    public function update($id, Request $request) {
        try {
            $country = Countries::find($id);

            if($country === null)
                return new JsonResponse(['message' => 'Not found'], 404);

            $data = $request->toArray();
            $data['updated_by'] = $request->user_id;
            $country->update($data);

            return new JsonResponse([],200);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

    public function search(Request $request, $country) {
        try {
            $limit = $request->limit?$request->limit:10;
            $data = Countries::with('state')
            ->select('countries.name','countries.updated_at','countries.id',DB::raw("TRIM(CONCAT_WS(' ',u.first_name,u.last_name)) as updated_by"),'countries.deleted_at')
            ->leftjoin('users as u','u.id','=','countries.updated_by')
            ->where('name','like',$country.'%')->withTrashed()->paginate($limit);
            if(count($data) == 0)
                return new JsonResponse(['message' => 'No Data found'], 200);

            return new JsonResponse($data);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

    public function restore($id) {
        Countries::withTrashed()->find($id)->restore();
        return new JsonResponse([],200);
    }
}
