<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Language;
use App\HomePageContent;
use App\Countries;
use App\category;
use App\Menu;
use DB;


class LanguageController extends Controller
{


     public function getLanguages(){
        $data['lang']=Language::select('lang_name','short_name','id')
        ->get();
        return response()->json([
              'data' => $data
            ]);
     }
     public function getContents(){
        $data['contents']['hi']= HomePageContent::select('language','main_title','sub_title','description')
            ->where(['language'=>'hi'])
            ->get();
        $data['contents']['gu']= HomePageContent::select('language','main_title','sub_title','description')
            ->where(['language'=>'gu'])
            ->get();
        $data['contents']['en']= HomePageContent::select('language','main_title','sub_title','description')
            ->where(['language'=>'en'])
            ->get();
        $data['contents']['bn']= HomePageContent::select('language','main_title','sub_title','description')
            ->where(['language'=>'bn'])
            ->get();
        $data['contents']['pa']= HomePageContent::select('language','main_title','sub_title','description')
            ->where(['language'=>'pa'])
            ->get();
        $data['contents']['ta']= HomePageContent::select('language','main_title','sub_title','description')
            ->where(['language'=>'ta'])
            ->get();
        $data['contents']['te']= HomePageContent::select('language','main_title','sub_title','description')
            ->where(['language'=>'te'])
            ->get();
        $data['contents']['ml']= HomePageContent::select('language','main_title','sub_title','description')
            ->where(['language'=>'ml'])
            ->get();
        $data['contents']['mr']= HomePageContent::select('language','main_title','sub_title','description')
            ->where(['language'=>'mr'])
            ->get();

        return response()->json([

              'data' => $data
            ]);
     }
     public function getContentsByMenu(Request $request,$menu){

            $data['contents']['hi']= HomePageContent::select('language','main_title','sub_title','description')
            ->where(['menu_name'=>$menu,'language'=>'hi'])
            ->get();
        $data['contents']['gu']= HomePageContent::select('language','main_title','sub_title','description')
            ->where(['menu_name'=>$menu,'language'=>'gu'])
            ->get();
        $data['contents']['en']= HomePageContent::select('language','main_title','sub_title','description')
            ->where(['menu_name'=>$menu,'language'=>'en'])
            ->get();
        $data['contents']['bn']= HomePageContent::select('language','main_title','sub_title','description')
            ->where(['menu_name'=>$menu,'language'=>'bn'])
            ->get();
        $data['contents']['pa']= HomePageContent::select('language','main_title','sub_title','description')
            ->where(['menu_name'=>$menu,'language'=>'pa'])
            ->get();
        $data['contents']['ta']= HomePageContent::select('language','main_title','sub_title','description')
            ->where(['menu_name'=>$menu,'language'=>'ta'])
            ->get();
        $data['contents']['te']= HomePageContent::select('language','main_title','sub_title','description')
            ->where(['menu_name'=>$menu,'language'=>'te'])
            ->get();
        $data['contents']['ml']= HomePageContent::select('language','main_title','sub_title','description')
            ->where(['menu_name'=>$menu,'language'=>'ml'])
            ->get();
        $data['contents']['mr']= HomePageContent::select('language','main_title','sub_title','description')
            ->where(['menu_name'=>$menu,'language'=>'mr'])
            ->get();

        return response()->json([

              'data' => $data
            ]);
     }
     public function getCountries(){
        // $data['counties'] = Countries::select('id','name')
        //                     ->distinct()
        //                     ->get(['name']);
                       $data['counties'] =     DB::table('countries')->select( DB::raw('DISTINCT(name)') )->get();
        return response()->json([
              'data' => $data
            ]);

     }
     public function getStates(){
        // $data['counties'] = Countries::select('id','name')
        //                     ->distinct()
        //                     ->get(['name']);
                       $data['counties'] =     DB::table('states')->select('id','state_name')->get();
        return response()->json([
              'data' => $data
            ]);
     }
     public function getMenus(){
        $data['lang']=Menu::select('id','menu_name')
        ->get();
        return response()->json([
              'data' => $data
            ]);
     }
     public function getContentsByMenuAndLanguage(Request $request,$menu,$lang){

            $data['content'][$lang]= HomePageContent::select('menu_name','language','main_title','sub_title','description')
            ->where(['menu_name'=>$menu,'language'=>$lang])
            ->get();
        return response()->json([
              'data' => $data
            ]);
     }

     public function returnGoogleAPIKey()
     {
     	if(!empty(env('GOOGLE_API_KEY'))){
     		 return response()->json([
              'api_key' => env('GOOGLE_API_KEY')
            ]);
     	} else {
     			 return response()->json([
              'api_key' => 'API Key Not Found in Env File.'
            ]);
     	}
     	
     }


}

