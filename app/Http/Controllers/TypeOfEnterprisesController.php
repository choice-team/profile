<?php
namespace App\Http\Controllers;

use App\Users;
use Illuminate\Http\Request;
use App\Models\TypeOfEnterprise;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use function GuzzleHttp\json_encode;


class TypeOfEnterprisesController extends Controller
{
    public function index(Request $request) {
            $limit = $request->limit? $request->limit:10;
            
            $type = TypeOfEnterprise::select('type_of_enterprises.type_of_enterprise','type_of_enterprises.id','type_of_enterprises.updated_at',DB::raw("TRIM(CONCAT_WS(' ',u.first_name,u.last_name)) as updated_by"),'type_of_enterprises.deleted_at')
            ->leftjoin('users as u','u.id','=','type_of_enterprises.updated_by');

            if(isset($request->sort) && $request->sort == 'name') {
                $data = ($request->order == 1)? $type->orderBy('type_of_enterprise') : $type->orderBy('type_of_enterprise','DESC');
            } else if(isset($request->sort) && $request->sort == 'date') {
                $data = ($request->order == 1) ? $type->orderBy('updated_at') : $type->orderBy('updated_at','DESC');   
            } else {
                $data = $type->orderBy('updated_at','DESC');
            }

             $search = str_replace("%20"," ",$request->keyword);
            if(isset($search) && !empty($search))
            {
                $data = $type->where('type_of_enterprises.type_of_enterprise','like',$search.'%');
            }
            $data = isset($request->flag)?$data->withTrashed()->get():$data->withTrashed()->paginate($limit);
            // $data = $data->paginate($limit);

            if($data->count()==0) {
                return new JsonResponse(['message' => 'No Data found'], 200);
            } 
           
            return new JsonResponse($data);
    }

    public function show($id) {
        try {
            $type_of_enterprise = TypeOfEnterprise::find($id);

            if($type_of_enterprise === null)
                return new JsonResponse(['message' => 'Not found'], 404);

            $userName = Users::where('id',$type_of_enterprise['updated_by'])->first();
            if($userName) {
                $type_of_enterprise['updated_by'] =  trim($userName->first_name.' '.$userName->last_name);
            } else {
                $type_of_enterprise['updated_by'] = '';
            }
            return new JsonResponse($type_of_enterprise);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

    public function store(Request $request) {
        try {
            $this->validate($request,[
                'type_of_enterprise'   => 'required|unique:type_of_enterprises'
            ]);
            
            $data = $request->toArray();
            $data['updated_by'] = $request->user_id;

            $type_of_enterprise = new TypeOfEnterprise($data);

            // HERE: additional, non-fillable fields

            $type_of_enterprise->save();

            if($type_of_enterprise)
                return new JsonResponse([], 200);

            return new JsonResponse(['message' => 'Server Error'], 500);
        } catch (\Illuminate\Validation\ValidationException $e) {
            return validation_exception($e);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

    public function destroy($id) {
        try {
            $type_of_enterprise = TypeOfEnterprise::find($id);

            if($type_of_enterprise === null)
                return new JsonResponse(['message' => 'Not found'], 404);

            $type_of_enterprise->delete();

            return new JsonResponse(['message' => 'Deleted Successfuly'], 200);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

    public function update($id, Request $request) {
        try {
            $type_of_enterprise = TypeOfEnterprise::find($id);

            if($type_of_enterprise === null)
                return new JsonResponse(['message' => 'Not found'], 404);

            $data = $request->toArray();
            $data['updated_by'] = $request->user_id;

            $type_of_enterprise->update($data);

            return new JsonResponse([],200);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

    public function search(Request $request, $enterprise) {
        try {
            $limit = $request->limit?$request->limit:10;
            $data = TypeOfEnterprise::
            select('type_of_enterprises.type_of_enterprise','type_of_enterprises.id','type_of_enterprises.updated_at',DB::raw("TRIM(CONCAT_WS(' ',u.first_name,u.last_name)) as updated_by"))
            ->leftjoin('users as u','u.id','=','type_of_enterprises.updated_by')
            ->where('type_of_enterprises.type_of_enterprise','like',$enterprise.'%')->paginate($limit);
            if(count($data) == 0)
                return new JsonResponse(['message' => 'Not found'], 200);

            return new JsonResponse($data);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }        
    
    public function restore($id) {
        TypeOfEnterprise::withTrashed()->find($id)->restore();
        return new JsonResponse([],200);
    }
}
