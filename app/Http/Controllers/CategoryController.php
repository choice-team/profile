<?php
namespace App\Http\Controllers;

use App\Users;
use App\Category;
use App\Notification;
use App\Services\Slug;
use Illuminate\Http\Request;
use App\Events\NotificationEvent;
use Illuminate\Http\JsonResponse;
use App\Services\PermissionMapper;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\Jobs\CategoryNotificationJob;


class CategoryController extends Controller
{

    /**
 * @api {get} api/categories list of categories
 * @apiName index
 * @apiGroup admin
 *
 * @apiSuccess {String} area_of_specialization  of the area_of_specialization.
 * @apiSuccess {String} updated_by  of the area_of_specialization. 
 * @apiSuccess {timestamp} updated_at  of the area_of_specialization.
 * @apiSuccess {timestamp} created_at  of the area_of_specialization.
 */
    public function index(Request $request) {
            $limit = $request->limit? $request->limit:10;
            $categoryData = Category::select('categories.category_name','categories.updated_at','categories.id','categories.category_type','u.first_name as updated_by','categories.deleted_at')
            ->leftjoin('users as u','u.id','=','categories.updated_by');
            
            if($request->sort == 'name') {
                $data = ($request->order == 1)? $categoryData->orderBy('category_name') : $categoryData->orderBy('category_name','DESC');
            } else if($request->sort == 'date') {
                $data = ($request->order == 1) ? $categoryData->orderBy('updated_at') : $categoryData->orderBy('updated_at','DESC');
            } else {
                $data = $categoryData->orderBy('updated_at','DESC');
            }

            $search = str_replace("%20"," ",$request->keyword);
            if(isset($search) && !empty($search))
            {
                $data = $categoryData->where('categories.category_name','like','%'.$search.'%');
            }
            $data = $categoryData->withTrashed()->paginate($limit);
            if($data->count()==0) {
                return new JsonResponse(['message' => 'No Data found'], 200);
            }
            
            return new JsonResponse($data);
    }

    public function show($id) {
        try{
            $categoryData = Category::find($id);
            $categoryData['updated_by'] = $request->user_id;
            $categoryData->save();
            if($categoryData === null)
                return new JsonResponse(['message' => 'Not found'], 404);

            $userName = Users::where('id',$categoryData['updated_by'])->first();
            if($userName) {
                $categoryData['updated_by'] =  trim($userName->first_name.' '.$userName->last_name);
            } else {
                $categoryData['updated_by'] = '';
            }
            return new JsonResponse($categoryData);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

    public function store(Request $request) {
        try {
            $this->validate($request,[
                'category_name'   => 'required|unique:categories',
            ]);
            $data = $request->toArray();
            
            unset($data['user_id']);
            $data['category_type'] ='business experise';
            $data['updated_by'] = Auth::user()->id;
            $categoryData = new Category($data);
            
            // HERE: additional, non-fillable fields

            $categoryData->save();
            
            if($categoryData)
                $slugName =  Slug::createSlug($categoryData->category_name,'forum_topics','topic_slug');
                DB::table('forum_topics')->insert(['topic_name'=>$categoryData->id,'topic_slug'=>$slugName,'user_id'=>Auth::user()->id]);
                PermissionMapper::createPermission($categoryData->category_name,$slugName);
            
                $notification = new Notification;
                $notification->title = "Area of Interest-".Auth::user()->display_name." has add _".$categoryData->category_name;
                $notification->url = $slugName;
                $notification->created_by = Auth::user()->id;
                $categoryData->notifications()->save($notification);
                //dispatch(new CategoryNotificationJob($notification));
               
                // $user = Users::all();
                // $notification->users()->saveMany($user);
                return new JsonResponse([], 200);

           
        } catch (\Illuminate\Validation\ValidationException $e) {
            return validation_exception($e);
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            return general_expection($e);
        }
    }

    public function destroy($id) {
        try {
            $categoryData = Category::find($id);

            if($categoryData === null)
                return new JsonResponse(['message' => 'Not found'], 404);
            
            $query = DB::table('forum_topics');
            $slug = $query->where('topic_name',$categoryData->id)->first()->topic_slug;
            PermissionMapper::deletePermission($slug);
            $query->where(['topic_name'=>$categoryData->id])->delete();
            $categoryData->delete();

            return new JsonResponse(['message'=>'Deleted Sucessfully'], 200);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

    public function update(Request $request, $id) {
        try {

            $this->validate($request,[
                'category_name'   => 'unique:categories,category_name,'.$id
            ]);

            $categoryData = Category::find($id);

            if($categoryData === null)
                return new JsonResponse(['message' => 'Not found'], 404);

            $data = $request->toArray();
            $data['updated_by'] = $request->user_id;
            $oldName = $categoryData->category_name;
            $categoryData->update($data);
            $slugName =  Slug::createSlug($categoryData->category_name,'forum_topics','topic_slug');
            $query = DB::table('forum_topics')->where('topic_name',$categoryData->id);
            
            $query->update(['topic_slug'=>$slugName]);
            PermissionMapper::updatePermission($oldName,$categoryData->category_name ,$slugName);
            return new JsonResponse([],200);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

    public function search(Request $request, $category) {
        try {
            $limit = $request->limit?$request->limit:10;
            $data = Category::
            select('categories.category_name','categories.updated_at','categories.id','categories.category_type',DB::raw("TRIM(CONCAT(u.first_name,u.last_name)) as updated_by"),'categories.deleted_at')
            ->leftjoin('users as u','u.id','=','categories.updated_by')
            ->where('categories.category_name','like',$category.'%')
            ->paginate($limit);
            
            if(count($data) == 0)
                return new JsonResponse(['message' => 'Not found'], 200);

            return new JsonResponse($data);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

    public function restore($id) {
        Category::withTrashed()->find($id)->restore();
        $categoryData = Category::find($id);
        $slugName =  Slug::createSlug($categoryData->category_name,'forum_topics','topic_slug');
        DB::table('forum_topics')->insert(['topic_name'=>$categoryData->id,'topic_slug'=>$slugName,'user_id'=>Auth::user()->id]);
        PermissionMapper::createPermission($categoryData->category_name,$slugName);
        return new JsonResponse([],200);
    }
}
