<?php
namespace App\Http\Controllers;

use App\Users;
use App\Qualification;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class QualificationsController extends Controller
{
    public function index(Request $request) {
            $limit = isset($request->limit)? $request->limit:10;
            $qualification = Qualification::select('qualification.qualification_name','qualification.area_of_specialisation','qualification.updated_at','qualification.id',DB::raw("TRIM(CONCAT_WS(' ',u.first_name,u.last_name)) as updated_by"),'qualification.deleted_at')
            ->leftjoin('users as u','u.id','=','qualification.updated_by');

            if(isset($request->sort) && $request->sort == 'name') {
                $data = ($request->order == 1)? $qualification->orderBy('qualification_name') : $qualification->orderBy('qualification_name','DESC');
            } else if(isset($request->sort) && $request->sort == 'date') {
                $data = ($request->order == 1) ? $qualification->orderBy('updated_at') : $qualification->orderBy('updated_at','DESC');
            } else {
                $data = $qualification->orderBy('updated_at','DESC');
            }
            $search = str_replace("%20"," ",$request->keyword);
            if(isset($search) && !empty($search))
            {
                $data = $qualification->where('qualification.qualification_name','like',$search.'%');
            }
            $data =  isset($request->flag)?$data->withTrashed()->get():$data->withTrashed()->paginate($limit);
            if($data->count()==0) {
                return new JsonResponse(['message' => 'No Data found'], 200);
            } 
    
            return new JsonResponse($data);
        
    }

    public function show($id) {
        try {
            $qualification = Qualification::withTrashed()->find($id);

            if($qualification === null)
                 new JsonResponse(['message' => 'No Data found'], 200);
               
            $userName = Users::where('id',$qualification['updated_by'])->first();
            if($userName) {
                $qualification['updated_by'] =  trim($userName->first_name.' '.$userName->last_name);
            } else {
                $qualification['updated_by'] = '';
            }
            return new JsonResponse($qualification);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

    public function store(Request $request) {
        try {
            $this->validate($request,[
                'qualification_name'   => 'required|unique:qualification'
            ]);

            $data = $request->toArray();
            $data['updated_by'] = Auth::user()->id;
            $qualification = new Qualification($data);

            // HERE: additional, non-fillable fields

            $qualification->save();

            if($qualification)
                return new JsonResponse([],200);

            return new JsonResponse(['message' => 'Server Error'], 500);
        } catch (\Illuminate\Validation\ValidationException $e) {
            return validation_exception($e);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

    public function destroy($id) {
        try {
            $qualification = Qualification::find($id);

            if($qualification === null)
                return new JsonResponse(['message' => 'Not found'], 404);

            $qualification->delete();

            return new JsonResponse(['message'=>'Deleted Sucessfully'], 200);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

    public function update($id, Request $request) {
        try {
            $qualification = Qualification::find($id);

            if($qualification === null)
                return new JsonResponse(['message' => 'Not found'], 404);

            $data = $request->toArray();
            $data['updated_by'] = Auth::user()->id;
            $qualification->update($data);

            return new JsonResponse([],200);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

    public function search(Request $request, $qualification) {
        try {
            $limit = $request->limit?$request->limit:10;
            $data = Qualification::
            select('qualification.qualification_name','qualification.area_of_specialisation','qualification.updated_at','qualification.id',DB::raw("TRIM(CONCAT_WS(' ',u.first_name,u.last_name)) as updated_by"),'qualification.deleted_at')
            ->leftjoin('users as u','u.id','=','qualification.updated_by')
            ->where('qualification.qualification_name','like',$qualification.'%')->withTrashed()->paginate($limit);
            if(count($data) == 0)
                return new JsonResponse(['message' => 'No Data found'], 200);

            return new JsonResponse($data);
        } catch (\Exception $e) {
            return general_expection($e);
        }
    }

    public function restore($id) {
        Qualification::withTrashed()->find($id)->restore();
        return new JsonResponse([],200);
    }
}
