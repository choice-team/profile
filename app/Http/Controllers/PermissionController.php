<?php
namespace App\Http\Controllers;

use App\Users;
use App\Models\Role;
use App\Models\Module;
use App\Models\Permission;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use function GuzzleHttp\json_encode;


class PermissionController extends Controller
{

    public function show(Request $request, $role_id) {

        $module = Role::with('modules')->where('id',$role_id)->first()->modules->pluck('id');

        $permissionData = Module::with('children')->whereIn('modules.id',$module)->get();
        
        foreach($permissionData as $value) {
            if(count($value['children'])) {
                foreach($value['children'] as $val) {
                    $val['permissions'] =$val->permissions;
                }
            } else {
                $value['permissions'] = $value->permissions;
               
            } 
        }
        return $permissionData; 
    }

    public function showPermissionModule(Request $request) {
        $role_id = DB::table('users_roles')->where('user_id',372)->get()->pluck('role_id');
        $module = Role::whereIn('id',$role_id)->get()->pluck('module_id');
        $mod=[];
        foreach($module as $val) {
            $a = explode(',', $val);
            array_push($mod,$a);
        }
       
        $dev=[];
        foreach($mod as $value) {
            foreach($value as $val) {
                if(!in_array($val,$dev)) {
                    array_push($dev,$val);
                }
                
            }
        }
        //dd($dev);
        //$moduleArray = explode(',', $module);

        $permissionData = Module::with('children')->whereIn('modules.id',$dev)->get();
        
        foreach($permissionData as $value) {
            if(count($value['children'])) {
                foreach($value['children'] as $val) {
                    $val['permissions'] =$val->permissions;
                }
            } else {
                $value['permissions'] = $value->permissions;
               
            } 
        }
        return $permissionData; 
    }
 
    // public function show($id) {
    //     try{
    //         $permissionData = Permission::with('roles')->find($id);

    //         if($permissionData === null)
    //             return new JsonResponse(['message' => 'Not found'], 404);
            
    //         return new JsonResponse($permissionData);
    //     } catch (\Exception $e) {
    //         return general_expection($e);
    //     }
    // }

    // public function store(Request $request) {
    //     try {
    //         $this->validate($request,[
    //             'slug'   => 'required|unique:Permission'
    //         ]);
    //         $data = $request->toArray();
    //         $data['updated_by'] = $request->user_id;
    //         $roleData = new Permission($data);

    //         // HERE: additional, non-fillable fields

    //         $roleData->save();

    //         if($roleData)
    //             return new JsonResponse([$roleData], 200);

    //         return new JsonResponse(['message' => 'Server Error'], 500);
    //     } catch (\Illuminate\Validation\ValidationException $e) {
    //         return validation_exception($e);
    //     } catch (\Exception $e) {
    //         return general_expection($e);
    //     }
    // }

    // public function destroy($id) {
    //     try {
    //         $permissionData = Permission::find($id);

    //         if($permissionData === null)
    //             return new JsonResponse(['message' => 'Not found'], 404);
    //         DB::table('role_permission')->where('permission_id',$permissionData->id)->delete();
    //         $permissionData->delete();

    //         return new JsonResponse(['message'=>'Deleted Sucessfully'], 200);
    //     } catch (\Exception $e) {
    //         return general_expection($e);
    //     }
    // }

    // public function update($id, Request $request) {
    //     try {
    //         $permissionData = Permission::find($id);

    //         if($permissionData === null)
    //             return new JsonResponse(['message' => 'Not found'], 404);

    //         $data = $request->toArray();
    //         $data['updated_by'] = $request->user_id;
    //         $permissionData->update($request->toArray());

    //         return new JsonResponse($permissionData);
    //     } catch (\Exception $e) {
    //         return general_expection($e);
    //     }
    // }

    // public function search(Request $request, $role) {
    //     try {
    //         $limit = $request->limit?$request->limit:10;
    //         $data = Permission::with('roles')
    //         ->where('name','like',$role.'%')->get();
            
    //         if($data->count() == 0)
    //             return new JsonResponse(['message' => 'Not found'], 200);

    //         foreach($data as $value) {
    //             $userName = Users::find($value->updated_by);
    //             $value['updated_by'] = ($userName) ? trim($userName['first_name'].' '.$userName['last_name']):'';
    //         }
    //         return new JsonResponse($data);
    //     } catch (\Exception $e) {
    //         return general_expection($e);
    //     }
    // }

}
