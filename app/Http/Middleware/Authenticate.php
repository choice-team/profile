<?php

namespace App\Http\Middleware;

use Closure;
use App\Users;
use Illuminate\Contracts\Auth\Factory as Auth;

class Authenticate
{
    /**
     * The authentication guard factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory  $auth
     * @return void
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ($request->header('Authorization')) {
            $authorization = explode(' ',$request->header('Authorization'));
            if(isset($authorization[1])){
            $token =explode('~~', base64_decode($authorization[1]));
            $user = Users::where('api_token',$token[0])->where('user_slug',$token[count($token)-1])->first();
        }
        }
       
        if ($this->auth->guard($guard)->guest() || is_null($user)) {
            return response('Unauthorized.', 401);
        }
       
        return $next($request);
    }
}
