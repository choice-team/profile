<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyDirectors extends Model
{

     use SoftDeletes;
    protected $table = 'company_team';
    protected $fillable = ['user_id','member_name','designation','qualification_id','created_at','updated_at','deleted_at'];

}
