<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStageOfBusinessesTable extends Migration
{
    public function up()
    {
        Schema::create('stage_of_businesses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('stage_of_business',255);
            $table->integer('updated_by')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('stage_of_businesses');
    }
}
