<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSectorOfEnterprisesTable extends Migration
{
    public function up()
    {
        Schema::create('sector_of_enterprises', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sector_of_enterprise',255);
            $table->integer('updated_by')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('sector_of_enterprises');
    }
}
