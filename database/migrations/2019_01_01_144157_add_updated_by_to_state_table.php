<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUpdatedByToStateTable extends Migration
{
    public function up()
    {
        Schema::table('state', function (Blueprint $table) {
            $table->integer('updated_by')->unsigned();
            $table->integer('countries_id')->unsigned();
        });
    }

    public function down()
    {
        Schema::dropIfExists('states');
    }
}
