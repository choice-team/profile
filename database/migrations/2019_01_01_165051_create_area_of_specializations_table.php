<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreaOfSpecializationsTable extends Migration
{
    public function up()
    {
        Schema::create('area_of_specializations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('area_of_specialization',255);
            $table->integer('updated_by')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('area_of_specializations');
    }
}
