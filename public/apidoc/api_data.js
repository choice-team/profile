define({ "api": [
  {
    "type": "get",
    "url": "api/profile/profile/entrepreneur/{id}",
    "title": "Request User information",
    "name": "getEntreprenuer",
    "group": "Profile",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Users unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "firstname",
            "description": "<p>Firstname of the User.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "lastname",
            "description": "<p>Lastname of the User.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/UsersController.php",
    "groupTitle": "Profile"
  },
  {
    "type": "get",
    "url": "api/profile/partner/view/{userId}",
    "title": "Request User information",
    "name": "showPartnerProfile",
    "group": "Profile",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "userId",
            "optional": false,
            "field": "userId",
            "description": "<p>Users unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "firstname",
            "description": "<p>Firstname of the User.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>of the User.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mobile",
            "description": "<p>of the User.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "partner_profile",
            "description": "<p>of User from partner_profile.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/UsersController.php",
    "groupTitle": "Profile"
  },
  {
    "type": "delete",
    "url": "api/admin/area_of_specialization/{id}",
    "title": "delete  area of specialization",
    "name": "destroy_area_of_specialization",
    "group": "admin",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Numeric",
            "optional": false,
            "field": "id",
            "description": "<p>to delete the respective area_of_specialization</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "area_of_specialization",
            "description": "<p>of the area_of_specialization.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/AreaOfSpecializationsController.php",
    "groupTitle": "admin"
  },
  {
    "type": "get",
    "url": "api/admin/area_of_specializations",
    "title": "list of area of specialization",
    "name": "index",
    "group": "admin",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "area_of_specialization",
            "description": "<p>of the area_of_specialization.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "updated_by",
            "description": "<p>of the area_of_specialization.</p>"
          },
          {
            "group": "Success 200",
            "type": "timestamp",
            "optional": false,
            "field": "updated_at",
            "description": "<p>of the area_of_specialization.</p>"
          },
          {
            "group": "Success 200",
            "type": "timestamp",
            "optional": false,
            "field": "created_at",
            "description": "<p>of the area_of_specialization.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/AreaOfSpecializationsController.php",
    "groupTitle": "admin"
  },
  {
    "type": "get",
    "url": "api/admin/area_of_specializations/{id}",
    "title": "show speicific area of specialization",
    "name": "show_area_of_specialization",
    "group": "admin",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "area_of_specialization",
            "description": "<p>of the area_of_specialization.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "updated_by",
            "description": "<p>of the area_of_specialization.</p>"
          },
          {
            "group": "Success 200",
            "type": "timestamp",
            "optional": false,
            "field": "updated_at",
            "description": "<p>of the area_of_specialization.</p>"
          },
          {
            "group": "Success 200",
            "type": "timestamp",
            "optional": false,
            "field": "created_at",
            "description": "<p>of the area_of_specialization.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/AreaOfSpecializationsController.php",
    "groupTitle": "admin"
  },
  {
    "type": "post",
    "url": "api/admin/area_of_specializations",
    "title": "store  area of specialization",
    "name": "store_area_of_specialization",
    "group": "admin",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "area_of_specialization",
            "description": "<p>to add in area_of_specialization</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "updated_by",
            "description": "<p>role of the user who add</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "area_of_specialization",
            "description": "<p>of the area_of_specialization.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "updated_by",
            "description": "<p>of the area_of_specialization.</p>"
          },
          {
            "group": "Success 200",
            "type": "timestamp",
            "optional": false,
            "field": "updated_at",
            "description": "<p>of the area_of_specialization.</p>"
          },
          {
            "group": "Success 200",
            "type": "timestamp",
            "optional": false,
            "field": "created_at",
            "description": "<p>of the area_of_specialization.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/AreaOfSpecializationsController.php",
    "groupTitle": "admin"
  },
  {
    "type": "patch",
    "url": "api/admin/area_of_specialization/{id}",
    "title": "update  area of specialization",
    "name": "udpate_area_of_specialization",
    "group": "admin",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "area_of_specialization",
            "description": "<p>to add in area_of_specialization</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "updated_by",
            "description": "<p>role of the user who add</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "area_of_specialization",
            "description": "<p>of the area_of_specialization.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "updated_by",
            "description": "<p>of the area_of_specialization.</p>"
          },
          {
            "group": "Success 200",
            "type": "timestamp",
            "optional": false,
            "field": "updated_at",
            "description": "<p>of the area_of_specialization.</p>"
          },
          {
            "group": "Success 200",
            "type": "timestamp",
            "optional": false,
            "field": "created_at",
            "description": "<p>of the area_of_specialization.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/AreaOfSpecializationsController.php",
    "groupTitle": "admin"
  },
  {
    "type": "get",
    "url": "api/admin/area_of_specializations/{name}",
    "title": "update  area of specialization",
    "name": "udpate_area_of_specialization",
    "group": "admin",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>to search in area_of_specialization</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "area_of_specialization",
            "description": "<p>of the area_of_specialization.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "updated_by",
            "description": "<p>of the area_of_specialization.</p>"
          },
          {
            "group": "Success 200",
            "type": "timestamp",
            "optional": false,
            "field": "updated_at",
            "description": "<p>of the area_of_specialization.</p>"
          },
          {
            "group": "Success 200",
            "type": "timestamp",
            "optional": false,
            "field": "created_at",
            "description": "<p>of the area_of_specialization.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/AreaOfSpecializationsController.php",
    "groupTitle": "admin"
  }
] });
