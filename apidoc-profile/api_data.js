define({ "api": [
  {
    "type": "post",
    "url": "api/profile/dashboard/partner-wise-blog-list",
    "title": "Blog List Partner Wise",
    "name": "Blog_List_Partner_Wise",
    "group": "Blog",
    "description": "<p>Blog List</p>",
    "success": {
      "examples": [
        {
          "title": "Success-Response-2000:",
          "content": "    HTTP/1.1 2000 OK\n{\n    \"status_code\": \"2000\",\n    \"message\": \"success\",\n    \"body\": {\n        \"current_page\": 1,\n        \"data\": [\n            {\n                \"blog_count\": 2,\n                \"first_name\": \"Bharat\",\n                \"user_slug\": \"us-india\"\n            },\n            {\n                \"blog_count\": 4,\n                \"first_name\": \"US India\",\n                \"user_slug\": \"us-india\"\n            }\n        ],\n        \"first_page_url\": \"http://wepprofile.choicetechlab.com/api/profile/dashboard/partner-wise-blog-list?page=1\",\n        \"from\": 1,\n        \"last_page\": 1,\n        \"last_page_url\": \"http://wepprofile.choicetechlab.com/api/profile/dashboard/partner-wise-blog-list?page=1\",\n        \"next_page_url\": null,\n        \"path\": \"http://wepprofile.choicetechlab.com/api/profile/dashboard/partner-wise-blog-list\",\n        \"per_page\": 15,\n        \"prev_page_url\": null,\n        \"to\": 2,\n        \"total\": 2\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response-4001:",
          "content": "HTTP/1.1 4001 Unauthorized\n\n  {\n  \"status_code\": 4001,\n  \"message\": \"Unauthorized Accecss\",\n  \"body\": []\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response-5000:",
          "content": "HTTP/1.1 5000 Internal Server Error\n   {\n  \"status_code\": 5000,\n  \"message\": \"Internal Error, Try again later\",\n  \"body\": []\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/DashboardController.php",
    "groupTitle": "Blog"
  },
  {
    "type": "GET",
    "url": "api/profile/dashboard/partner-wise-blog-count",
    "title": "Blog Count Partner wise",
    "name": "TotalCount_of_Partner_Wise_Blog",
    "group": "Blog",
    "description": "<p>Blog Count</p>",
    "success": {
      "examples": [
        {
          "title": "Success-Response-2000:",
          "content": "    HTTP/1.1 2000 OK\n{\n    \"status_code\": \"2000\",\n    \"message\": \"success\",\n    \"body\": [\n        {\n            \"total_count\": 3\n        }\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response-4001:",
          "content": "HTTP/1.1 4001 Unauthorized\n\n  {\n  \"status_code\": 4001,\n  \"message\": \"Unauthorized Accecss\",\n  \"body\": []\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response-5000:",
          "content": "HTTP/1.1 5000 Internal Server Error\n   {\n  \"status_code\": 5000,\n  \"message\": \"Internal Error, Try again later\",\n  \"body\": []\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/DashboardController.php",
    "groupTitle": "Blog"
  },
  {
    "type": "post",
    "url": "api/profile/dashboard/partner-wise-community-list",
    "title": "Community List Partner Wise",
    "name": "Community_List_Partner_Wise",
    "group": "Community",
    "description": "<p>Community List</p>",
    "success": {
      "examples": [
        {
          "title": "Success-Response-2000:",
          "content": "    HTTP/1.1 2000 OK\n{\n    \"status_code\": \"2000\",\n    \"message\": \"success\",\n    \"body\": {\n        \"current_page\": 1,\n        \"data\": [\n            {\n                \"community_count\": 2,\n                \"first_name\": \"Bharat\",\n                \"user_slug\": \"us-india\"\n            },\n            {\n                \"community_count\": 1,\n                \"first_name\": \"US India\",\n                \"user_slug\": \"us-india\"\n            }\n        ],\n        \"first_page_url\": \"http://wepprofile.choicetechlab.com/api/profile/dashboard/partner-wise-community-list?page=1\",\n        \"from\": 1,\n        \"last_page\": 1,\n        \"last_page_url\": \"http://wepprofile.choicetechlab.com/api/profile/dashboard/partner-wise-community-list?page=1\",\n        \"next_page_url\": null,\n        \"path\": \"http://wepprofile.choicetechlab.com/api/profile/dashboard/partner-wise-community-list\",\n        \"per_page\": 15,\n        \"prev_page_url\": null,\n        \"to\": 2,\n        \"total\": 2\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response-4001:",
          "content": "HTTP/1.1 4001 Unauthorized\n\n  {\n  \"status_code\": 4001,\n  \"message\": \"Unauthorized Accecss\",\n  \"body\": []\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response-5000:",
          "content": "HTTP/1.1 5000 Internal Server Error\n   {\n  \"status_code\": 5000,\n  \"message\": \"Internal Error, Try again later\",\n  \"body\": []\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/DashboardController.php",
    "groupTitle": "Community"
  },
  {
    "type": "get",
    "url": "api/profile/dashboard/partner-wise-community-count",
    "title": "Community Partner Count",
    "name": "TotalCount_of_Partner_Wise_Community",
    "group": "Community",
    "description": "<p>Community Partner Count</p>",
    "success": {
      "examples": [
        {
          "title": "Success-Response-2000:",
          "content": "    HTTP/1.1 2000 OK\n{\n    \"status_code\": \"2000\",\n    \"message\": \"success\",\n    \"body\": [\n        {\n            \"total_count\": 3\n        }\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response-4001:",
          "content": "HTTP/1.1 4001 Unauthorized\n\n  {\n  \"status_code\": 4001,\n  \"message\": \"Unauthorized Accecss\",\n  \"body\": []\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response-5000:",
          "content": "HTTP/1.1 5000 Internal Server Error\n   {\n  \"status_code\": 5000,\n  \"message\": \"Internal Error, Try again later\",\n  \"body\": []\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/DashboardController.php",
    "groupTitle": "Community"
  },
  {
    "type": "post",
    "url": "api/profile/dashboard/partner-wise-event-list",
    "title": "Event List Partner Wise",
    "name": "Event_List_Partner_Wise",
    "group": "Event",
    "description": "<p>Event List Partner Wise</p>",
    "success": {
      "examples": [
        {
          "title": "Success-Response-2000:",
          "content": "    HTTP/1.1 2000 OK\n{\n    \"status_code\": \"2000\",\n    \"message\": \"success\",\n    \"body\": {\n        \"current_page\": 1,\n        \"data\": [\n            {\n                \"event_count\": 2,\n                \"first_name\": \"Bharat\",\n                \"user_slug\": \"us-india\"\n            },\n            {\n                \"event_count\": 2,\n                \"first_name\": \"deAsra Foundation\",\n                \"user_slug\": \"deasra-foundation\"\n            },\n            {\n                \"event_count\": 2,\n                \"first_name\": \"Choice Techlab\",\n                \"user_slug\": \"choice-techlab\"\n            }\n        ],\n        \"first_page_url\": \"http://wepprofile.choicetechlab.com/api/profile/dashboard/partner-wise-event-list?page=1\",\n        \"from\": 1,\n        \"last_page\": 1,\n        \"last_page_url\": \"http://wepprofile.choicetechlab.com/api/profile/dashboard/partner-wise-event-list?page=1\",\n        \"next_page_url\": null,\n        \"path\": \"http://wepprofile.choicetechlab.com/api/profile/dashboard/partner-wise-event-list\",\n        \"per_page\": 15,\n        \"prev_page_url\": null,\n        \"to\": 3,\n        \"total\": 3\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response-4001:",
          "content": "HTTP/1.1 4001 Unauthorized\n\n  {\n  \"status_code\": 4001,\n  \"message\": \"Unauthorized Accecss\",\n  \"body\": []\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response-5000:",
          "content": "HTTP/1.1 5000 Internal Server Error\n   {\n  \"status_code\": 5000,\n  \"message\": \"Internal Error, Try again later\",\n  \"body\": []\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/DashboardController.php",
    "groupTitle": "Event"
  },
  {
    "type": "GET",
    "url": "api/profile/dashboard/partner-wise-event-count",
    "title": "Event Count Partner Wise",
    "name": "TotalCount_of_Partner_Wise_Event",
    "group": "Event",
    "description": "<p>Event Count Partner Wise</p>",
    "success": {
      "examples": [
        {
          "title": "Success-Response-2000:",
          "content": "    HTTP/1.1 2000 OK\n{\n    \"status_code\": \"2000\",\n    \"message\": \"success\",\n    \"body\": [\n        {\n            \"total_count\": 3\n        }\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response-4001:",
          "content": "HTTP/1.1 4001 Unauthorized\n\n  {\n  \"status_code\": 4001,\n  \"message\": \"Unauthorized Accecss\",\n  \"body\": []\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response-5000:",
          "content": "HTTP/1.1 5000 Internal Server Error\n   {\n  \"status_code\": 5000,\n  \"message\": \"Internal Error, Try again later\",\n  \"body\": []\n  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/DashboardController.php",
    "groupTitle": "Event"
  },
  {
    "type": "get",
    "url": "api/profile/connected-members",
    "title": "Connected Members",
    "name": "Connected_Members",
    "group": "Profile",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "user_slug",
            "description": "<p>Users unique user_slug.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response-2000:",
          "content": "   HTTP/1.1 2000 OK\n{\n    \"status_code\": \"2000\",\n    \"message\": \"success\",\n    \"body\": {\n        \"data\": {\n            \"connected_members\": [\n                {\n                    \"first_name\": \"Yogesh\",\n                    \"last_name\": \"Jadhav\",\n                    \"profile_pic\": null,\n                    \"slug\": \"yogesh-jadhav\"\n                },\n                {\n                    \"first_name\": \"Tester\",\n                    \"last_name\": \"Choice\",\n                    \"profile_pic\": \"1546861693fa841d1d24b136e619893f43ce11c2a3.jpg\",\n                    \"slug\": \"tester-choice\"\n                }\n            ]\n        }\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response-4001:",
          "content": "HTTP/1.1 4001 Unauthorized\n{\n \"status_code\": 4001,\n \"message\": \"Unauthorized Accecss\",\n \"body\": []\n }",
          "type": "json"
        },
        {
          "title": "Error-Response-5000:",
          "content": "HTTP/1.1 5000 Internal Server Error\n   {\n \"status_code\": 5000,\n \"message\": \"Internal Error, Try again later\",\n \"body\": []\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/MyProfileController.php",
    "groupTitle": "Profile"
  },
  {
    "type": "get",
    "url": "api/profile/connected-partners",
    "title": "Connected Partners",
    "name": "Connected_Partners",
    "group": "Profile",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "user_slug",
            "description": "<p>Users unique user_slug.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response-2000:",
          "content": "   HTTP/1.1 2000 OK\n{{\n    \"status_code\": \"2000\",\n    \"message\": \"success\",\n    \"body\": {\n        \"data\": {\n            \"connected_partners\": [\n                {\n                    \"first_name\": \"CRISIL Limited\",\n                    \"logo\": \"157674671116db163683672142f73bb4865145d842.JPG\",\n                    \"slug\": \"crisil-limited\"\n                },\n                {\n                    \"first_name\": \"Access Livelihoods Consulting India\",\n                    \"logo\": \"partner-alc-india.png\",\n                    \"slug\": \"alc-india\"\n                }\n            ]\n        }\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response-4001:",
          "content": "HTTP/1.1 4001 Unauthorized\n{\n \"status_code\": 4001,\n \"message\": \"Unauthorized Accecss\",\n \"body\": []\n }",
          "type": "json"
        },
        {
          "title": "Error-Response-5000:",
          "content": "HTTP/1.1 5000 Internal Server Error\n   {\n \"status_code\": 5000,\n \"message\": \"Internal Error, Try again later\",\n \"body\": []\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/MyProfileController.php",
    "groupTitle": "Profile"
  },
  {
    "type": "get",
    "url": "api/profile/my-profile",
    "title": "Profile Information",
    "name": "My_profile",
    "group": "Profile",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "user_slug",
            "description": "<p>Users unique user_slug.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response-2000:",
          "content": "   HTTP/1.1 2000 OK\n{\n    \"status_code\": \"2000\",\n    \"message\": \"success\",\n    \"body\": {\n        \"data\": {\n            \"connection_status\": [],\n            \"userProfile\": [\n                {\n                    \"first_name\": \"geeta\",\n                    \"last_name\": \"ahuja\",\n                    \"email\": \"Z2VldGFhaHVqYTg1QGdtYWlsLmNvbQ==\",\n                    \"gender\": \"female\",\n                    \"birth_year\": \"1919\",\n                    \"country\": \"India\",\n                    \"state\": \"West Bengal\\r\\n\",\n                    \"about\": \"\",\n                    \"profile_pic\": \"1586753296face9686a126c31e936870d7c956b970.png\",\n                    \"entreprise_name\": null,\n                    \"mentorship_status\": 0,\n                    \"other_city\": \"\",\n                    \"city\": \"Calcutta\"\n                }\n            ]\n        }\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response-4001:",
          "content": "HTTP/1.1 4001 Unauthorized\n{\n \"status_code\": 4001,\n \"message\": \"Unauthorized Accecss\",\n \"body\": []\n }",
          "type": "json"
        },
        {
          "title": "Error-Response-5000:",
          "content": "HTTP/1.1 5000 Internal Server Error\n   {\n \"status_code\": 5000,\n \"message\": \"Internal Error, Try again later\",\n \"body\": []\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/MyProfileController.php",
    "groupTitle": "Profile"
  },
  {
    "type": "get",
    "url": "api/profile/suggested-mentor",
    "title": "Suggested Mentor",
    "name": "Suggested_Mentor",
    "group": "Profile",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "user_slug",
            "description": "<p>Users unique user_slug.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response-2000:",
          "content": "   HTTP/1.1 2000 OK\n{\n    \"status_code\": \"2000\",\n    \"message\": \"success\",\n    \"body\": {\n        \"data\": {\n            \"suggested_mentors\": [\n                {\n                    \"slug\": \"ganesh-1\",\n                    \"first_name\": \"Ganesh\",\n                    \"last_name\": \"\",\n                    \"profile_pic\": \"1544691975fa841d1d24b136e619893f43ce11c2a3.jpg\"\n                },\n                {\n                    \"slug\": \"geeta-ahuja-2\",\n                    \"first_name\": \"Geeta\",\n                    \"last_name\": \"Ahuja\",\n                    \"profile_pic\": null\n                }\n            ]\n        }\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response-4001:",
          "content": "HTTP/1.1 4001 Unauthorized\n{\n \"status_code\": 4001,\n \"message\": \"Unauthorized Accecss\",\n \"body\": []\n }",
          "type": "json"
        },
        {
          "title": "Error-Response-5000:",
          "content": "HTTP/1.1 5000 Internal Server Error\n   {\n \"status_code\": 5000,\n \"message\": \"Internal Error, Try again later\",\n \"body\": []\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/MyProfileController.php",
    "groupTitle": "Profile"
  },
  {
    "type": "get",
    "url": "api/profile/suggested-partners",
    "title": "Suggested Partners",
    "name": "Suggested_Partners",
    "group": "Profile",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "user_slug",
            "description": "<p>Users unique user_slug.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response-2000:",
          "content": "   HTTP/1.1 2000 OK\n\t{\n    \"status_code\": \"2000\",\n    \"message\": \"success\",\n    \"body\": {\n        \"data\": {\n            \"suggested_partners\": [\n                {\n                    \"slug\": \"crisil-limited\",\n                    \"first_name\": \"CRISIL Limited\",\n                    \"last_name\": \"\",\n                    \"logo\": \"157674671116db163683672142f73bb4865145d842.JPG\"\n                },\n                {\n                    \"slug\": \"alc-india\",\n                    \"first_name\": \"Access Livelihoods Consulting India\",\n                    \"last_name\": \"\",\n                    \"logo\": \"partner-alc-india.png\"\n                }\n            ]\n        }\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response-4001:",
          "content": "HTTP/1.1 4001 Unauthorized\n{\n \"status_code\": 4001,\n \"message\": \"Unauthorized Accecss\",\n \"body\": []\n }",
          "type": "json"
        },
        {
          "title": "Error-Response-5000:",
          "content": "HTTP/1.1 5000 Internal Server Error\n   {\n \"status_code\": 5000,\n \"message\": \"Internal Error, Try again later\",\n \"body\": []\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/MyProfileController.php",
    "groupTitle": "Profile"
  },
  {
    "type": "get",
    "url": "api/profile/profile/entrepreneur/{id}",
    "title": "Request User information",
    "name": "getEntreprenuer",
    "group": "Profile",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Users unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "firstname",
            "description": "<p>Firstname of the User.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "lastname",
            "description": "<p>Lastname of the User.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/UsersController.php",
    "groupTitle": "Profile"
  },
  {
    "type": "get",
    "url": "api/profile/partner/view/{userId}",
    "title": "Request User information",
    "name": "showPartnerProfile",
    "group": "Profile",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "userId",
            "optional": false,
            "field": "userId",
            "description": "<p>Users unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "first_name",
            "description": "<p>Firstname of the User.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>of the User.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mobile",
            "description": "<p>of the User.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "partner_profile",
            "description": "<p>of User from partner_profile.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/UsersController.php",
    "groupTitle": "Profile"
  },
  {
    "type": "get",
    "url": "api/profile/users",
    "title": "list users for admin",
    "name": "deactivatelist",
    "group": "admin",
    "success": {
      "examples": [
        {
          "title": "Success-Response-2000:",
          "content": " HTTP/1.1 2000 OK\n   {\n     \"status_code\": \"2000\",\n    \"message\": \"success\",\n    \"body\": {\n    \"current_page\": 1,\n    \"data\": [\n        {\n            \"display_name\": null,\n            \"email\": \"abc@gmail.in\",\n            \"mobile\": \"212121212\",\n            \"activation_date\": null,\n            \"deactivation_date\": null,\n            \"created_at\": \"2018-12-03 07:26:08\",\n            \"updated_at\": \"2019-01-24 09:49:29\",\n            \"role\": \"Administer\",\n            \"username\": \"WEP RolE\",\n            \"created_by\": \"\"\n        }\n    ],\n    \"first_page_url\": \"http://localhost:8001/api/profile/users?page=1\",\n    \"from\": 1,\n    \"last_page\": 2,\n    \"last_page_url\": \"http://localhost:8001/api/profile/users?page=2\",\n    \"next_page_url\": \"http://localhost:8001/api/profile/users?page=2\",\n    \"path\": \"http://localhost:8001/api/profile/users\",\n    \"per_page\": 10,\n    \"prev_page_url\": null,\n    \"to\": 10,\n    \"total\": 11\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response-4001:",
          "content": "HTTP/1.1 4001 Unauthorized\n{\n   \"status_code\": 4001,\n   \"message\": \"Unauthorized Accecss\",\n   \"body\": []\n   }",
          "type": "json"
        },
        {
          "title": "Error-Response-5000:",
          "content": "HTTP/1.1 5000 Internal Server Error\n   {\n   \"status_code\": 5000,\n   \"message\": \"Internal Error, Try again later\",\n   \"body\": []\n   }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/UsersController.php",
    "groupTitle": "admin"
  },
  {
    "type": "delete",
    "url": "api/profile/user/{id}",
    "title": "delete user",
    "name": "destroy",
    "group": "admin",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>of user</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Success",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/UsersController.php",
    "groupTitle": "admin"
  },
  {
    "type": "delete",
    "url": "api/profile/area_of_specialization/{id}",
    "title": "delete  area of specialization",
    "name": "destroy_area_of_specialization",
    "group": "admin",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Numeric",
            "optional": false,
            "field": "id",
            "description": "<p>to delete the respective area_of_specialization</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "area_of_specialization",
            "description": "<p>of the area_of_specialization.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/AreaOfSpecializationsController.php",
    "groupTitle": "admin"
  },
  {
    "type": "get",
    "url": "api/profile/activity-log",
    "title": "Activity log user",
    "name": "index",
    "group": "admin",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>of user</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Success",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/ActivityController.php",
    "groupTitle": "admin"
  },
  {
    "type": "get",
    "url": "api/profile/area_of_specializations",
    "title": "list of area of specialization",
    "name": "index",
    "group": "admin",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "area_of_specialization",
            "description": "<p>of the area_of_specialization.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "updated_by",
            "description": "<p>of the area_of_specialization.</p>"
          },
          {
            "group": "Success 200",
            "type": "timestamp",
            "optional": false,
            "field": "updated_at",
            "description": "<p>of the area_of_specialization.</p>"
          },
          {
            "group": "Success 200",
            "type": "timestamp",
            "optional": false,
            "field": "created_at",
            "description": "<p>of the area_of_specialization.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/AreaOfSpecializationsController.php",
    "groupTitle": "admin"
  },
  {
    "type": "get",
    "url": "api/categories",
    "title": "list of categories",
    "name": "index",
    "group": "admin",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "area_of_specialization",
            "description": "<p>of the area_of_specialization.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "updated_by",
            "description": "<p>of the area_of_specialization.</p>"
          },
          {
            "group": "Success 200",
            "type": "timestamp",
            "optional": false,
            "field": "updated_at",
            "description": "<p>of the area_of_specialization.</p>"
          },
          {
            "group": "Success 200",
            "type": "timestamp",
            "optional": false,
            "field": "created_at",
            "description": "<p>of the area_of_specialization.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/CategoryController.php",
    "groupTitle": "admin"
  },
  {
    "type": "get",
    "url": "api/profile/users",
    "title": "list users for admin",
    "name": "index",
    "group": "admin",
    "success": {
      "examples": [
        {
          "title": "Success-Response-2000:",
          "content": " HTTP/1.1 2000 OK\n   {\n     \"status_code\": \"2000\",\n    \"message\": \"success\",\n    \"body\": {\n    \"current_page\": 1,\n    \"data\": [\n        {\n            \"display_name\": null,\n            \"email\": \"abc@gmail.in\",\n            \"mobile\": \"212121212\",\n            \"activation_date\": null,\n            \"deactivation_date\": null,\n            \"created_at\": \"2018-12-03 07:26:08\",\n            \"updated_at\": \"2019-01-24 09:49:29\",\n            \"role\": \"Administer\",\n            \"username\": \"WEP RolE\",\n            \"created_by\": \"\"\n        }\n    ],\n    \"first_page_url\": \"http://localhost:8001/api/profile/users?page=1\",\n    \"from\": 1,\n    \"last_page\": 2,\n    \"last_page_url\": \"http://localhost:8001/api/profile/users?page=2\",\n    \"next_page_url\": \"http://localhost:8001/api/profile/users?page=2\",\n    \"path\": \"http://localhost:8001/api/profile/users\",\n    \"per_page\": 10,\n    \"prev_page_url\": null,\n    \"to\": 10,\n    \"total\": 11\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response-4001:",
          "content": "HTTP/1.1 4001 Unauthorized\n{\n   \"status_code\": 4001,\n   \"message\": \"Unauthorized Accecss\",\n   \"body\": []\n   }",
          "type": "json"
        },
        {
          "title": "Error-Response-5000:",
          "content": "HTTP/1.1 5000 Internal Server Error\n   {\n   \"status_code\": 5000,\n   \"message\": \"Internal Error, Try again later\",\n   \"body\": []\n   }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/UsersController.php",
    "groupTitle": "admin"
  },
  {
    "type": "get",
    "url": "api/profile/user/search/{user}",
    "title": "Search user",
    "name": "search",
    "group": "admin",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "user",
            "description": "<p>parameter</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response-2000:",
          "content": " HTTP/1.1 2000 OK\n   {\n     \"status_code\": \"2000\",\n    \"message\": \"success\",\n    \"body\": {\n    \"current_page\": 1,\n    \"data\": [\n        {\n            \"display_name\": null,\n            \"email\": \"abc@gmail.in\",\n            \"mobile\": \"212121212\",\n            \"activation_date\": null,\n            \"deactivation_date\": null,\n            \"created_at\": \"2018-12-03 07:26:08\",\n            \"updated_at\": \"2019-01-24 09:49:29\",\n            \"role\": \"Administer\",\n            \"username\": \"WEP RolE\",\n            \"created_by\": \"\"\n        }\n    ],\n    \"first_page_url\": \"http://localhost:8001/api/profile/users?page=1\",\n    \"from\": 1,\n    \"last_page\": 2,\n    \"last_page_url\": \"http://localhost:8001/api/profile/users?page=2\",\n    \"next_page_url\": \"http://localhost:8001/api/profile/users?page=2\",\n    \"path\": \"http://localhost:8001/api/profile/users\",\n    \"per_page\": 10,\n    \"prev_page_url\": null,\n    \"to\": 10,\n    \"total\": 11\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response-4001:",
          "content": "HTTP/1.1 4001 Unauthorized\n{\n   \"status_code\": 4001,\n   \"message\": \"Unauthorized Accecss\",\n   \"body\": []\n   }",
          "type": "json"
        },
        {
          "title": "Error-Response-5000:",
          "content": "HTTP/1.1 5000 Internal Server Error\n   {\n   \"status_code\": 5000,\n   \"message\": \"Internal Error, Try again later\",\n   \"body\": []\n   }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/UsersController.php",
    "groupTitle": "admin"
  },
  {
    "type": "get",
    "url": "api/profile/activity-log/{slug}",
    "title": "Activity log user",
    "name": "show",
    "group": "admin",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "slug",
            "description": "<p>of user</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Success",
            "description": ""
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/ActivityController.php",
    "groupTitle": "admin"
  },
  {
    "type": "get",
    "url": "api/profile/user/{id}",
    "title": "detailed view of users for admin",
    "name": "show",
    "group": "admin",
    "success": {
      "examples": [
        {
          "title": "Success-Response-2000:",
          "content": " HTTP/1.1 2000 OK\n   {\n     \"status_code\": \"2000\",\n    \"message\": \"success\",\n    \"body\": {\n    \"data\": [\n        {\n            \"display_name\": null,\n            \"email\": \"abc@gmail.in\",\n            \"mobile\": \"212121212\",\n            \"activation_date\": null,\n            \"deactivation_date\": null,\n            \"created_at\": \"2018-12-03 07:26:08\",\n            \"updated_at\": \"2019-01-24 09:49:29\",\n            \"role\": \"Administer\",\n            \"username\": \"WEP RolE\",\n            \"created_by\": \"\"\n        }\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response-4001:",
          "content": "HTTP/1.1 4001 Unauthorized\n{\n   \"status_code\": 4001,\n   \"message\": \"Unauthorized Accecss\",\n   \"body\": []\n   }",
          "type": "json"
        },
        {
          "title": "Error-Response-5000:",
          "content": "HTTP/1.1 5000 Internal Server Error\n   {\n   \"status_code\": 5000,\n   \"message\": \"Internal Error, Try again later\",\n   \"body\": []\n   }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/UsersController.php",
    "groupTitle": "admin"
  },
  {
    "type": "get",
    "url": "api/profile/area_of_specializations/{id}",
    "title": "show speicific area of specialization",
    "name": "show_area_of_specialization",
    "group": "admin",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "area_of_specialization",
            "description": "<p>of the area_of_specialization.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "updated_by",
            "description": "<p>of the area_of_specialization.</p>"
          },
          {
            "group": "Success 200",
            "type": "timestamp",
            "optional": false,
            "field": "updated_at",
            "description": "<p>of the area_of_specialization.</p>"
          },
          {
            "group": "Success 200",
            "type": "timestamp",
            "optional": false,
            "field": "created_at",
            "description": "<p>of the area_of_specialization.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/AreaOfSpecializationsController.php",
    "groupTitle": "admin"
  },
  {
    "type": "post",
    "url": "api/profile/users",
    "title": "store user",
    "name": "store",
    "group": "admin",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "first_name",
            "description": "<p>to add in user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "display_name",
            "description": "<p>role of the user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mobile",
            "description": "<p>of the user.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>of the user.</p>"
          },
          {
            "group": "Parameter",
            "type": "date",
            "optional": false,
            "field": "activation_date",
            "description": "<p>of the user.</p>"
          },
          {
            "group": "Parameter",
            "type": "date",
            "optional": false,
            "field": "deactivation_date",
            "description": "<p>of the user.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "first_name",
            "description": "<p>to add in user</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "display_name",
            "description": "<p>role of the user</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "mobile",
            "description": "<p>of the user.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>of the user.</p>"
          },
          {
            "group": "Success 200",
            "type": "date",
            "optional": false,
            "field": "activation_date",
            "description": "<p>of the user.</p>"
          },
          {
            "group": "Success 200",
            "type": "date",
            "optional": false,
            "field": "deactivation_date",
            "description": "<p>of the user.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Updated_by",
            "description": "<p>of the user.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/UsersController.php",
    "groupTitle": "admin"
  },
  {
    "type": "post",
    "url": "api/profile/area_of_specializations",
    "title": "store  area of specialization",
    "name": "store_area_of_specialization",
    "group": "admin",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "area_of_specialization",
            "description": "<p>to add in area_of_specialization</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "updated_by",
            "description": "<p>role of the user who add</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "area_of_specialization",
            "description": "<p>of the area_of_specialization.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "updated_by",
            "description": "<p>of the area_of_specialization.</p>"
          },
          {
            "group": "Success 200",
            "type": "timestamp",
            "optional": false,
            "field": "updated_at",
            "description": "<p>of the area_of_specialization.</p>"
          },
          {
            "group": "Success 200",
            "type": "timestamp",
            "optional": false,
            "field": "created_at",
            "description": "<p>of the area_of_specialization.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/AreaOfSpecializationsController.php",
    "groupTitle": "admin"
  },
  {
    "type": "patch",
    "url": "api/profile/area_of_specialization/{id}",
    "title": "update  area of specialization",
    "name": "udpate_area_of_specialization",
    "group": "admin",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "area_of_specialization",
            "description": "<p>to add in area_of_specialization</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "updated_by",
            "description": "<p>role of the user who add</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "area_of_specialization",
            "description": "<p>of the area_of_specialization.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "updated_by",
            "description": "<p>of the area_of_specialization.</p>"
          },
          {
            "group": "Success 200",
            "type": "timestamp",
            "optional": false,
            "field": "updated_at",
            "description": "<p>of the area_of_specialization.</p>"
          },
          {
            "group": "Success 200",
            "type": "timestamp",
            "optional": false,
            "field": "created_at",
            "description": "<p>of the area_of_specialization.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/AreaOfSpecializationsController.php",
    "groupTitle": "admin"
  },
  {
    "type": "get",
    "url": "api/area_of_specializations/search/{name}",
    "title": "update  area of specialization",
    "name": "udpate_area_of_specialization",
    "group": "admin",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>to search in area_of_specialization</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "area_of_specialization",
            "description": "<p>of the area_of_specialization.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "updated_by",
            "description": "<p>of the area_of_specialization.</p>"
          },
          {
            "group": "Success 200",
            "type": "timestamp",
            "optional": false,
            "field": "updated_at",
            "description": "<p>of the area_of_specialization.</p>"
          },
          {
            "group": "Success 200",
            "type": "timestamp",
            "optional": false,
            "field": "created_at",
            "description": "<p>of the area_of_specialization.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/AreaOfSpecializationsController.php",
    "groupTitle": "admin"
  },
  {
    "type": "patch",
    "url": "api/profile/user/{id}",
    "title": "update user",
    "name": "updateUser",
    "group": "admin",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>of user</p>"
          },
          {
            "group": "Parameter",
            "type": "Array",
            "optional": false,
            "field": "parameter",
            "description": "<p>to be updated</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response-2000:",
          "content": "HTTP/1.1 2000 OK\n  {\n    \"status_code\": \"2000\",\n   \"message\": \"success\",\n   \"body\": {\n   \"current_page\": 1,\n   \"data\": [\n       {\n           \"display_name\": null,\n           \"email\": \"abc@gmail.in\",\n           \"mobile\": \"212121212\",\n           \"activation_date\": null,\n           \"deactivation_date\": null,\n           \"created_at\": \"2018-12-03 07:26:08\",\n           \"updated_at\": \"2019-01-24 09:49:29\",\n           \"role\": \"Administer\",\n           \"username\": \"WEP RolE\",\n           \"created_by\": \"\"\n       }\n   ],\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response-4001:",
          "content": "HTTP/1.1 4001 Unauthorized\n{\n   \"status_code\": 4001,\n   \"message\": \"Unauthorized Accecss\",\n   \"body\": []\n   }",
          "type": "json"
        },
        {
          "title": "Error-Response-5000:",
          "content": "HTTP/1.1 5000 Internal Server Error\n   {\n   \"status_code\": 5000,\n   \"message\": \"Internal Error, Try again later\",\n   \"body\": []\n   }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/Http/Controllers/UsersController.php",
    "groupTitle": "admin"
  }
] });
