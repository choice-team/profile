<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
    <title>Partner EOI Form Received
    </title>
    <style>
        body {
            color: #000;
            line-height: 20px;
        }
    </style>
</head>

<body style="background-color: #eee;">
    <table width="740" align="center" border="0" cellpadding="20" cellspacing="0" bgcolor="#f0f0f0">
        <tbody>
            <tr>
                <td>
                    <table width="640" align="center" border="0" cellpadding="0" cellspacing="0">
                        <tbody>

                            <tr>
                                <td align="left" valign="top" style="font-size: 12px; color: #000000; font-weight: normal;">
                                    <table width="640" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
                                        <tbody>
                                            <tr>
                                                <td height="20" colspan="4">&nbsp;</td>
                                            </tr>
                                            <tr align="center">
                                                <td align="center" valign="top">
                                                <img style="height: 84px;
    width: 60px;" src="{{env('FRONTEND_URL')}}/assets/images/color-logo.svg" alt="NITIAayog" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="20" colspan="4">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td align="center" valign="top" bgcolor="#FFFFFF">
                                                    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                                                        <tbody>

                                                            <tr>
                                                                <td height="20" colspan="4">&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                                <td width="40">&nbsp;</td>
                                                                <td align="left">
                                                                    <p style="font:600 18px 'Roboto', sans-serif; margin-top: 0; ">Dear Partner,
                                                                    </p>

                                                                    <p style="font:400 15px 'Roboto', sans-serif; line-height: 20px; margin: 22px 0; ">
                                                                    Thank you for registering on WEP portal. This email confirms our receipt of your Partner EOI Registration form. 

                                                                    <p style="font:400 15px 'Roboto', sans-serif; line-height: 20px; margin: 22px 0; ">NITI Aayog WEP Team will scrutinize your registration form and will revert back to you on your email id mentioned in the Partner EOI Registration form.</p>

                                                        

                                                                        <p style="font:400 15px 'Roboto', sans-serif; line-height: 20px; margin: 22px 0; ">Link to WEP Portal <a href="{{env('FRONTEND_URL')}}" target="blank" style="color:#ff4f81; text-decoration:none">
                                                            {{env('FRONTEND_URL')}} </a> </p>

                                                                            <p style="font:400 15px 'Roboto', sans-serif; line-height: 20px; margin: 22px 0; ">Follow us on <a href="http://www.facebook.com/WomenEntrepreneurshipPlatform/">Facebook</a></p>

                                    
                                                                    </p>
                                                                   
                                                                </td>
                                                                <td width="40">&nbsp;</td>
                                                            </tr>



                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top" bgcolor="#f0f0f0">
                                    <table width="640" bgcolor="#fff" border="0" align="center" cellpadding="0" cellspacing="0">
                                        <tbody>

                                            <tr>
                                                <td width="40">&nbsp;</td>
                                                <td align="left">
                                                    <p style="font:400 14px 'Roboto', sans-serif;line-height: 28px;  margin-bottom:0; color:#000; margin-top:0;">Regards,
                                                    </p>
                                                    <p style="font:600 14px 'Roboto', sans-serif;line-height: 28px;   margin-bottom:0; color:#000; margin-top:0;">Team – WEP
                                                    </p>
                                                    <p style="font:600 14px 'Roboto', sans-serif;line-height: 28px;   margin-bottom:0; color:#000; margin-top:0;">NITI Aayog
                                                    </p>
                                                </td>
                                                <td width="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td width="40">&nbsp;</td>
                                                <td height="20" style="border-bottom: 1px solid #eee;">&nbsp;</td>
                                                <td width="40">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td height="20" colspan="4">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td width="40">&nbsp;</td>
                                                <td align="center">
                                                    <p style="font:400 15px 'Roboto', sans-serif; line-height: 20px; color:#000; margin-top:0;">For any queries, write to us on
                                                        <span style="color:#ff4f81;">
                                                                <a href="mailto:wep-niti@gov.in" target="blank">wep-niti@gov.in </a>
                                                            </span>
                                                    </p>
                                                </td>
                                                <td width="40">&nbsp;</td>
                                            </tr>

                                            <tr>
                                                <td height="30" colspan="4" style="border-bottom: 2px solid #b84592;">&nbsp;</td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </td>

                        </tbody>
                    </table>
                </td>
                </tr>
                <tr>
                    <td align="left" valign="top" bgcolor="#f0f0f0">
                        <table width="640" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tbody>

                                <tr>
                                    <td width="40">&nbsp;</td>
                                    <td align="center">
                                        <p style="color:#000; font:400 16px 'Roboto', sans-serif; margin-top: 0;">Follow us on</p>
                                        <p>
                                        <span style="margin: 0 10px;">
                                                <a href="https://www.facebook.com/WomenEntrepreneurshipPlatform/" style="text-decoration:none" target="blank">
                                                    <img style="height: 33px;
    width: 60px;" src="{{env('FRONTEND_URL')}}/assets/images/fb.svg">
                                                </a>
                                            </span>
                                            <span style="margin: 0 10px;">
                                                <a href="https://twitter.com/NITIAayog" style="text-decoration:none" target="blank">
                                                    <img  style="height: 33px;
    width: 60px;" src="{{env('FRONTEND_URL')}}/assets/images/twitter.svg">
                                                </a>
                                            </span>
                                            <span style="margin: 0 10px;">
                                                <a href="https://www.linkedin.com/company/women-entrepreneurship-platform-niti-aayog/" style="text-decoration:none" target="blank">
                                                    <img  style="height: 33px;
    width: 60px;" src="{{env('FRONTEND_URL')}}/assets/images/linkedin.svg">
                                                </a>
                                            </span>
                                        </p>

                                    </td>
                                    <td width="40">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td height="20">&nbsp;</td>
                                </tr>
                            </tbody>

                        </table>
                    </td>
                </tr>
        </tbody>
    </table>


</body>

</html>

