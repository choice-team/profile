<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => '/api/profile/city/{id}'], function() use ($router) {
        $router->get('', [
            //'middleware' => 'can:city-view',
            'uses' => 'CityController@index',
            'as'   => 'city_show',
        ]);
    });

$router->group(['prefix' => '/api/profile','middleware' => 'auth:api'], function () use ($router) {

    // dashboard module
    $router->get('/dashboard/report-user-count','DashboardController@userCount');
    $router->get('/dashboard/membership-count','DashboardController@membershipCount');
    $router->get('/dashboard/scheme-blog','DashboardController@schemwiseBlog');
    $router->get('/dashboard/group-blog-count','DashboardController@groupwiseBlog');
    $router->get('/dashboard/forum','DashboardController@forum');
    $router->get('/dashboard/top-ans-count','DashboardController@topUserAnswerCount');
    $router->get('/dashboard/post-count','DashboardController@postCount');
    $router->get('/dashboard/post-ans-count','DashboardController@postAnsCount');
    $router->get('/dashboard/entreprenaur-partner-count','DashboardController@entreprenaurConnectingPartner');
    $router->get('/dashboard/user-wia-web','DashboardController@userRegistrationFromWeb');
    $router->get('/dashboard/user-via-facebook','DashboardController@userRegistrationFromFacebook');
    $router->get('/dashboard/user-via-google','DashboardController@userRegistrationFromGoogle');
    $router->get('/dashboard/user-via-contact','DashboardController@userFromContact');
    $router->get('/dashboard/user-via-city','DashboardController@userFromCity');
    $router->get('/dashboard/trending-blog','DashboardController@trendingBlog');
    $router->get('/dashboard/wti-count','DashboardController@countWtiApplicants');
    
    $router->get('/dashboard/state-wti-count','DashboardController@countWtiApplicantsStateWise'); 
    $router->get('/dashboard/partner-wise-blog-count','PartnerMonitoringController@partnerWiseBlogCount');
    $router->post('/dashboard/partner-wise-blog-list','PartnerMonitoringController@partnerWiseBlogList');

    $router->get('/dashboard/partner-wise-event-count','PartnerMonitoringController@partnerWiseEventCount');
    $router->post('/dashboard/partner-wise-event-list','PartnerMonitoringController@partnerWiseEventList'); 

    $router->get('/dashboard/partner-wise-community-count','PartnerMonitoringController@partnerWiseCommunityCount');  
    $router->post('/dashboard/partner-wise-community-list','PartnerMonitoringController@partnerWiseCommunityList');  

    $router->get('/entrepreneurs', 'UsersController@entrepreneurList');
    $router->get('/entrepreneurs-export', 'UsersController@exportEntreprenaurList');
    
    
    // $router->post('/entrepreneurs', 'UsersController@storeEntreprenuer');
    $router->post('/entrepreneurs', 'UsersController@storePersonalInfoApi');
    $router->get('/entrepreneur', 'UsersController@entrepreneurProfile');
    $router->get('/mentor/{id}', 'UsersController@mentorProfile');

    $router->put('/entrepreneur/{id}', 'UsersController@update');
    $router->get('/events/{id}','UsersController@entrepreneurEvents');
    $router->get('/entrepreneur/{id}/interest/','UsersController@entrepreneurInterestedArea');

    
    $router->get('/area-of-interest','UsersController@getAreaOfInterest');
    $router->post('/area-of-interest','UsersController@storeAreaOfInterest');
    $router->get('/area-of-interest/{user_slug}','UsersController@getUserAreaOfInterest');
    $router->get('/entrepreneur-qualification','UsersController@getQualification');
    // $router->post('/entrepreneur-qualification','UsersController@storeQualification');
    $router->patch('/entrepreneur-qualification','UsersController@updateQualification');
    $router->post('/business-info','UsersController@storeBusinessInfoApi');
    $router->post('/change-connection-status','UsersController@storeConnectionStatus');
    $router->put('/business-info/{id}','UsersController@updateBusinessInfoApi');
    $router->get('/business-info','UsersController@getBusinessInfoListApi');
    $router->post('/entrepreneur-qualification','UsersController@storeEducationalInfoApi');
    //$router->get('/qualification','UsersController@qualificationList');


    $router->get('/user-count','UsersDashboardController@getCount');
    
     
    $router->post('/update-profile-pic','UsersController@updateProfilePic');
    
    $router->patch('/partner/{slug}', 'PartnerServiceController@updateParterApi');
    $router->post('/partner-logo-update/{slug}', 'PartnerServiceController@updateParterApi');

    $router->post('/summary', 'UsersController@updateSummary');

    $router->post('/partner/view', 'UsersController@showPartnerProfile');

    $router->get('/module','ModuleController@index');
    $router->get('/operation','ModuleController@showOperation');
    $router->get('/get-user-permission', 'RoleController@rolesPermissionDashboard');
    $router->get('/get-user-module','PermissionController@showPermissionModule');

    $router->get('/partner','UsersController@partnerListLoginPage');   
    
    $router->get('/partner/{slug}','UsersController@singlePartnerProfile');

    $router->post('/partner-services', 'PartnerServiceController@storeApi');
    $router->patch('/partner-services/{id}', 'PartnerServiceController@updateApi');

    $router->get('/partner-pending','PartnerServiceController@partnerPendingList');
    $router->get('/partner-approve','PartnerServiceController@partnerApproveList');

    //notifications
    $router->get('/notifications','NotificationController@index');
    $router->patch('/notification/{id}','NotificationController@update');

    //$router->get('/suggested-mentor','UsersController@suggestedMentor');
    $router->get('/suggested-mentor/search/{search}','UsersController@suggestedMentor');
    $router->get('/suggested-partner','UsersController@suggestedPartner');
    $router->get('/suggested-partner/search/{search}','UsersController@suggestedPartner');
    //mapper module and operation
    $router->get('/map','RoleController@mapper');
   
    // content management api
    $router->get('/content/menu-item','HomePageContentController@getMenuItem');
    $router->post('/content/update-main-title','HomePageContentController@updateContent');
    $router->post('/content/update-description','HomePageContentController@updateDescription');
    $router->post('/content/update-subtitle','HomePageContentController@updateSubTitle');
    $router->post('/content/content-detail','HomePageContentController@getDetailLanguage');
    $router->get('/content/main-title-id','HomePageContentController@getMainTitleId');

    $router->post('/content/translate','HomePageContentController@showLanguageData');
    //route for roles and permissions
    $router->group(['prefix' => '/roles'], function() use ($router) {
        $router->get('', [
            //'middleware' => 'can:role-list',
            'uses' => 'RoleController@index',
            'as'   => 'role_index',
        ]);
        $router->get('/search/{role}', [
            //'middleware' => 'can:role-list',
            'uses' => 'RoleController@search',
            'as'   => 'role_search',
        ]);
        $router->post('', [
            'middleware' => 'can:role-create',
            'uses' => 'RoleController@store',
            'as'   => 'role_store',
        ]);
    });

    $router->group(['prefix' => '/role/{id}'], function() use ($router) {
        $router->get('', [
            'middleware' => 'can:role-view',
            'uses' => 'RoleController@show',
            'as'   => 'role_show',
        ]);

        $router->patch('', [
             'middleware' => 'can:role-update',
            'uses' => 'RoleController@update',
            'as'   => 'role_update',
        ]);
        
        $router->delete('', [
            'middleware' => 'can:role-deactivate',
            'uses' => 'RoleController@destroy',
            'as'   => 'role_destroy',
        ]);

        $router->get('/activate', [
            'middleware' => 'can:role-activate',
            'uses' => 'RoleController@restore',
            'as'   => 'role_restore',
        ]);
	});
	

	$router->group(['prefix' => '/permissions'], function() use ($router) {
        $router->get('', [
            'middleware' => 'can:permission-list',
            'uses' => 'PermissionController@index',
            'as'   => 'permission_index',
        ]);
        $router->get('/search/{role}', [
            'middleware' => 'can:permission-list',
            'uses' => 'PermissionController@search',
            'as'   => 'permission_search',
        ]);
        $router->post('', [
            'middleware' => 'can:permission-create',
            'uses' => 'PermissionController@store',
            'as'   => 'permission_store',
        ]);
    });

    $router->group(['prefix' => '/permission/{role_id}'], function() use ($router) {
        $router->get('', [
            'middleware' => 'can:permission-view',
            'uses' => 'PermissionController@show',
            'as'   => 'permission_show',
        ]);

        $router->patch('', [
            'middleware' => 'can:permission-update',
            'uses' => 'PermissionController@update',
            'as'   => 'permission_update',
        ]);
        
        $router->delete('', [
            'middleware' => 'can:permission-deactivate',
            'uses' => 'PermissionController@destroy',
            'as'   => 'permission_destroy',
        ]);
	});
	
	$router->group(['prefix' => '/users'], function() use ($router) {
        $router->get('', [
            'middleware' => 'can:user-list',
            'uses' => 'UsersController@index',
            'as'   => 'users_index',
        ]);

        $router->get('/deactivate','UsersController@deactivateUserList');

        $router->get('/search/{user}', [
            'middleware' => 'can:user-list',
            'uses' => 'UsersController@search',
            'as'   => 'users_search',
        ]);
        $router->post('', [
            'middleware' => 'can:user-create',
            'uses' => 'UsersController@store',
            'as'   => 'users_store',
        ]);
    });

    $router->group(['prefix' => '/user/{slug}'], function() use ($router) {
        $router->get('', [
            'middleware' => 'can:user-view',
            'uses' => 'UsersController@show',
            'as'   => 'users_show',
        ]);

        $router->patch('', [
            'middleware' => 'can:user-update',
            'uses' => 'UsersController@updateUser',
            'as'   => 'users_update',
        ]);
        
        $router->delete('', [
            'middleware' => 'can:user-deactivate',
            'uses' => 'UsersController@destroy',
            'as'   => 'users_destroy',
        ]);
        $router->get('/activate', [
            'middleware' => 'can:user-activate',
            'uses' => 'UsersController@restore',
            'as'   => 'users_restore',
        ]);
	});


    //master tables routes
    $router->group(['prefix' => '/categories'], function() use ($router) {
        $router->get('', [
            'middleware' => 'can:area-of-interest-list',
            'uses' => 'CategoryController@index',
            'as'   => 'categories_index',
        ]);
        $router->get('/search/{category}', [
            'middleware' => 'can:area-of-interest-list',
            'uses' => 'CategoryController@search',
            'as'   => 'categories_search',
        ]);
        $router->post('', [
            //'middleware' => 'can:area-of-interest-create',
            'uses' => 'CategoryController@store',
            'as'   => 'categories_store',
        ]);
    });

    $router->group(['prefix' => '/category/{id}'], function() use ($router) {
        $router->get('', [
            'middleware' => 'can:area-of-interest-view',
            'uses' => 'CategoryController@show',
            'as'   => 'categories_show',
        ]);

        $router->patch('', [
            'middleware' => 'can:area-of-interest-update',
            'uses' => 'CategoryController@update',
            'as'   => 'categories_update',
        ]);
        
        $router->delete('', [
            'middleware' => 'can:area-of-interest-deactivate',
            'uses' => 'CategoryController@destroy',
            'as'   => 'categories_destroy',
        ]);

        $router->get('/activate', [
            // 'middleware' => 'can:area-of-interest-activate',
            'uses' => 'CategoryController@restore',
            'as'   => 'categories_restore',
        ]);
    });

    $router->group(['prefix' => '/countries'], function() use ($router) {

        $router->get('/search/{country}', [
            //'middleware' => 'can:country-list',
            'uses' => 'CountriesController@search',
            'as'   => 'countries_search',
        ]);

        $router->post('', [
           // 'middleware' => 'can:country-create',
            'uses' => 'CountriesController@store',
            'as'   => 'countries_store',
        ]);
    });

    $router->group(['prefix' => '/country/{id}'], function() use ($router) {
        $router->get('', [
            'middleware' => 'can:country-view',
            'uses' => 'CountriesController@show',
            'as'   => 'country_show',
        ]);

        $router->patch('', [
            'middleware' => 'can:country-update',
            'uses' => 'CountriesController@update',
            'as'   => 'country_update',
        ]);
        
        $router->delete('', [
            'middleware' => 'can:country-deactivate',
            'uses' => 'CountriesController@destroy',
            'as'   => 'country_destroy',
        ]);

        $router->get('/activate', [
            'middleware' => 'can:country-activate',
            'uses' => 'CountriesController@restore',
            'as'   => 'country_restore',
        ]);
    });


    $router->group(['prefix' => '/states'], function() use ($router) {
        $router->get('', [
            //'middleware' => 'can:state-list',
            'uses' => 'StatesController@index',
            'as'   => 'states_search',
        ]);

        $router->get('/search/{state}', [
            //'middleware' => 'can:state-list',
            'uses' => 'StatesController@search',
            'as'   => 'states_search',
        ]);

        $router->post('', [
            'middleware' => 'can:state-create',
            'uses' => 'StatesController@store',
            'as'   => 'states_store',
        ]);
    });

    $router->group(['prefix' => '/state/{id}'], function() use ($router) {
        $router->get('', [
            'middleware' => 'can:state-view',
            'uses' => 'StatesController@show',
            'as'   => 'states_show',
        ]);

        $router->patch('', [
            'middleware' => 'can:state-update',
            'uses' => 'StatesController@update',
            'as'   => 'states_update',
        ]);
        
        $router->delete('', [
            'middleware' => 'can:state-deactivate',
            'uses' => 'StatesController@destroy',
            'as'   => 'states_destroy',
        ]);

        $router->get('/activate', [
            'middleware' => 'can:state-activate',
            'uses' => 'StatesController@restore',
            'as'   => 'state_restore',
        ]);
    });

    $router->group(['prefix' => '/city'], function() use ($router) {
        $router->get('', [
            //'middleware' => 'can:city-list',
            'uses' => 'CityController@list',
            'as'   => 'city_list',
        ]);

        $router->post('', [
            //'middleware' => 'can:city-list',
            'uses' => 'CityController@store',
            'as'   => 'city_store',
        ]);

           


    });

    $router->group(['prefix' => '/city/{id}'], function() use ($router) {
       

        $router->patch('', [
            //'middleware' => 'can:state-update',
            'uses' => 'CityController@update',
            'as'   => 'city_update',
        ]);
        
        $router->delete('', [
           // 'middleware' => 'can:state-deactivate',
            'uses' => 'CityController@destroy',
            'as'   => 'city_destroy',
        ]);

         $router->get('/activate', [
            'middleware' => 'can:state-activate',
            'uses' => 'CityController@restore',
            'as'   => 'city_restore',
        ]);
    });



    $router->group(['prefix' => '/qualifications'], function() use ($router) {
        
        $router->get('/search/{qualification}', [
            'middleware' => 'can:qualification-list',
            'uses' => 'QualificationsController@search',
            'as'   => 'qualifications_search',
        ]);

        $router->post('', [
            'middleware' => 'can:qualification-create',
            'uses' => 'QualificationsController@store',
            'as'   => 'qualifications_store',
        ]);
    });

    $router->group(['prefix' => '/qualification/{id}'], function() use ($router) {
        $router->get('', [
            'middleware' => 'can:qualification-view',
            'uses' => 'QualificationsController@show',
            'as'   => 'qualification_show',
        ]);

        $router->patch('', [
            'middleware' => 'can:qualification-update',
            'uses' => 'QualificationsController@update',
            'as'   => 'qualification_update',
        ]);
        
        $router->delete('', [
            'middleware' => 'can:qualification-deactivate',
            'uses' => 'QualificationsController@destroy',
            'as'   => 'qualification_destroy',
        ]);

        $router->get('/activate', [
            'middleware' => 'can:qualification-activate',
            'uses' => 'QualificationsController@restore',
            'as'   => 'qualification_restore',
        ]);
    });


    $router->group(['prefix' => '/sector-of-enterprises'], function() use ($router) {
        $router->get('', [
            //'middleware' => 'can:sector-of-enterprises-list',
            'uses' => 'SectorOfEnterprisesController@index',
            'as'   => 'sector_of_enterprises_index',
        ]);
        
        $router->get('/search/{enterprise}', [
            'middleware' => 'can:sector-of-enterprises-list',
            'uses' => 'SectorOfEnterprisesController@search',
            'as'   => 'sector_of_enterprises_search',
        ]);

        $router->post('', [
            'middleware' => 'can:sector-of-enterprises-create',
            'uses' => 'SectorOfEnterprisesController@store',
            'as'   => 'sector_of_enterprises_store',
        ]);
    });

    $router->group(['prefix' => '/sector-of-enterprise/{id}'], function() use ($router) {
        $router->get('', [
            'middleware' => 'can:sector-of-enterprises-view',
            'uses' => 'SectorOfEnterprisesController@show',
            'as'   => 'sector_of_enterprise_show',
        ]);

        $router->patch('', [
            'middleware' => 'can:sector-of-enterprises-update',
            'uses' => 'SectorOfEnterprisesController@update',
            'as'   => 'sector_of_enterprise_update',
        ]);
        
        $router->delete('', [
            'middleware' => 'can:sector-of-enterprises-deactivate',
            'uses' => 'SectorOfEnterprisesController@destroy',
            'as'   => 'sector_of_enterprise_destroy',
        ]);

        $router->get('/activate', [
            'middleware' => 'can:sector-of-enterprises-activate',
            'uses' => 'SectorOfEnterprisesController@restore',
            'as'   => 'sector_of_enterprises_restore',
        ]);
    });


    $router->group(['prefix' => '/type-of-enterprises'], function() use ($router) {
        $router->get('', [
          //  'middleware' => 'can:type-of-enterprises-list',
            'uses' => 'TypeOfEnterprisesController@index',
            'as'   => 'type_of_enterprises_index',
        ]);

        $router->get('/search/{enterprise}', [
            'middleware' => 'can:type-of-enterprises-list',
            'uses' => 'TypeOfEnterprisesController@search',
            'as'   => 'type_of_enterprises_search',
        ]);

        $router->post('', [
            'middleware' => 'can:type-of-enterprises-create',
            'uses' => 'TypeOfEnterprisesController@store',
            'as'   => 'type_of_enterprises_store',
        ]);
    });

    $router->group(['prefix' => '/type-of-enterprise/{id}'], function() use ($router) {
        $router->get('', [
            'middleware' => 'can:type-of-enterprises-view',
            'uses' => 'TypeOfEnterprisesController@show',
            'as'   => 'type_of_enterprise_show',
        ]);

        $router->patch('', [
            'middleware' => 'can:type-of-enterprises-update',
            'uses' => 'TypeOfEnterprisesController@update',
            'as'   => 'type_of_enterprise_update',
        ]);
        
        $router->delete('', [
            'middleware' => 'can:type-of-enterprises-deactivate',
            'uses' => 'TypeOfEnterprisesController@destroy',
            'as'   => 'type_of_enterprise_destroy',
        ]);

        $router->get('/activate', [
            'middleware' => 'can:type-of-enterprises-activate',
            'uses' => 'TypeOfEnterprisesController@restore',
            'as'   => 'type_of_enterprise_restore',
        ]);
    });


    $router->group(['prefix' => '/stage-of-businesses'], function() use ($router) {
        $router->get('', [
            //'middleware' => 'can:stage-of-business-list',
            'uses' => 'StageOfBusinessesController@index',
            'as'   => 'stage_of_businesses_index',
        ]);

        $router->get('/search/{business}', [
            'middleware' => 'can:stage-of-business-list',
            'uses' => 'StageOfBusinessesController@search',
            'as'   => 'stage_of_businesses_search',
        ]);

        $router->post('', [
            'middleware' => 'can:stage-of-business-create',
            'uses' => 'StageOfBusinessesController@store',
            'as'   => 'stage_of_businesses_store',
        ]);
    });

    $router->group(['prefix' => '/stage-of-business/{id}'], function() use ($router) {
        $router->get('', [
            'middleware' => 'can:stage-of-business-view',
            'uses' => 'StageOfBusinessesController@show',
            'as'   => 'stage_of_business_show',
        ]);

        $router->patch('', [
            'middleware' => 'can:stage-of-business-update',
            'uses' => 'StageOfBusinessesController@update',
            'as'   => 'stage_of_business_update',
        ]);
        
        $router->delete('', [
            'middleware' => 'can:stage-of-business-deactivate',
            'uses' => 'StageOfBusinessesController@destroy',
            'as'   => 'stage_of_business_destroy',
        ]);

        $router->get('/activate', [
            'middleware' => 'can:stage-of-business-activate',
            'uses' => 'StageOfBusinessesController@restore',
            'as'   => 'stage_of_business_restore',
        ]);
    });


    $router->group(['prefix' => '/area-of-specializations'], function() use ($router) {
        $router->get('', [
            //'middleware' => 'can:area-of-specialization-list',
            'uses' => 'AreaOfSpecializationsController@index',
            'as'   => 'area_of_specializations_index',
        ]);

        $router->get('/search/{name}', [
            'middleware' => 'can:area-of-specialization-list',
            'uses' => 'AreaOfSpecializationsController@search',
            'as'   => 'area_of_specializations_search',
        ]);

        $router->post('', [
            'middleware' => 'can:area-of-specialization-create',
            'uses' => 'AreaOfSpecializationsController@store',
            'as'   => 'area_of_specializations_store',
        ]);
    });

    $router->group(['prefix' => '/area-of-specialization/{id}'], function() use ($router) {
        $router->get('', [
            'middleware' => 'can:area-of-specialization-view',
            'uses' => 'AreaOfSpecializationsController@show',
            'as'   => 'area_of_specialization_show',
        ]);

        $router->patch('', [
            'middleware' => 'can:area-of-specialization-update',
            'uses' => 'AreaOfSpecializationsController@update',
            'as'   => 'area_of_specialization_update',
        ]);
        
        $router->delete('', [
            'middleware' => 'can:area-of-specialization-deactivate',
            'uses' => 'AreaOfSpecializationsController@destroy',
            'as'   => 'area_of_specialization_destroy',
        ]);

        $router->get('/activate', [
            'middleware' => 'can:area-of-specialization-activate',
            'uses' => 'AreaOfSpecializationsController@restore',
            'as'   => 'area_of_specialization_restore',
        ]);
    });

    $router->group(['prefix' => '/partner/{slug}'], function() use ($router) {
        $router->delete('/', [
            'middleware' => 'can:partner-deactivate',
            'uses' => 'PartnerServiceController@delete',
            'as'   => 'partner_destroy',
        ]);

        $router->get('/activate', [
            'middleware' => 'can:partner-activate',
            'uses' => 'PartnerServiceController@restore',
            'as'   => 'partner_restore',
        ]);

        $router->patch('/approve', [
            'middleware' => 'can:partner-approve',
            'uses' => 'PartnerServiceController@approvePartner',
            'as'   => 'partner_approve',
        ]);

        $router->patch('/reject', [
            'middleware' => 'can:partner-reject',
            'uses' => 'PartnerServiceController@rejectPartner',
            'as'   => 'partner_reject',
        ]);
       
    });
    $router->get('/activity-log','ActivityController@index');
    $router->get('/activity-log/{id}','ActivityController@show');

    $router->get('/getuser-id','MyProfileController@getUserId');

    $router->get('/my-profile','MyProfileController@myProfile');
    $router->get('/suggested-mentor','MyProfileController@suggestedMentor');
    $router->get('/connected-partners','MyProfileController@connectedPartners');
    $router->get('/connected-members','MyProfileController@connectedMembers');
    $router->get('/suggested-partners','MyProfileController@suggestedPartners');

    //Dynamic forms
$router->post('/form-builder','FormBuilderController@getFormList');
$router->post('/save-form-structure','FormBuilderController@saveNewlyCreatedForm');
$router->post('/save-form-data','FormBuilderController@saveFormData');
$router->get('/get-form-fields/{slug}','FormBuilderController@getFormFields');
$router->get('/get-all-forms/{slug}','FormBuilderController@getAllForms');
$router->post('/update-form-fields/{slug}','FormBuilderController@updateFormFields');
$router->delete('/delete-form/{slug}','FormBuilderController@deleteForm');
$router->post('/upload-file','FormBuilderController@uploadFile');
});

$router->post('/api/profile/partner', 'PartnerServiceController@storeParterApi');
$router->get('/api/profile/partner-export', 'PartnerServiceController@partnerExport');
$router->get('/api/profile/get-google-api-key','LanguageController@returnGoogleAPIKey');


/*Api for Language Conversion*/
$router->get('/api/profile/menus','LanguageController@getMenus');
$router->get('/api/profile/events/{id}','UsersController@entrepreneurEvents');
$router->get('/api/profile/partner-list','PartnerServiceController@partnerList');
$router->get('/api/profile/entrepreneur-count','PartnerServiceController@entrepreneurCount');
$router->get('/api/profile/pre-login-partner/{slug}','PartnerServiceController@singlePreLoginPartnerProfile');
$router->get('/api/profile/languages','LanguageController@getLanguages');
$router->get('/api/profile/contents','LanguageController@getContents');
$router->get('/api/profile/contents/{menu}','LanguageController@getContentsByMenu');
$router->get('/api/profile/contents/{menu}/{language}','LanguageController@getContentsByMenuAndLanguage');
$router->get('/api/profile/states','StatesController@index');
$router->get('/api/profile/countries','CountriesController@index');
$router->get('/api/profile/qualifications','QualificationsController@index');
















